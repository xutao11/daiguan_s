<?php
namespace app\common\service;

use think\Image;
use app\common\model\System;

class ImageService
{
    static public function getThumb($src,$width=0,$height=0,$type=Image::THUMB_CENTER)
    {
        if(empty($src)){
            return '';
        }
        $root = ROOT_PATH . DS .'wwwroot';
        $realPath = $root . $src;
		if(!file_exists($realPath)){
			return '';
		}
		if($width != 0 && $height != 0){
		    $w = $width;
            $h = $height;
        }else{
            $w = intval(System::getKey('thumb_w'));
            $h = intval(System::getKey('thumb_h'));
            if($w <= 0){
                $w = $width;
            }
            if($h <= 0){
                $h = $height;
            }
        }
		$info = pathinfo($src);
		$thumbName = $info['dirname'].'/'.$info['filename'].'_'.$w.'_'.$h.'.'.$info['extension'];
		$realThumbName = $root . $thumbName;
		if(!file_exists($realThumbName)){
			$image = Image::open($realPath);
			$image->thumb($w, $h,$type)->save($realThumbName);
		}
		var_dump($realThumbName);die;
		return $thumbName;
    }
    static public function getcopy($src,$width="960",$height="600"){

        if(empty($src)){
            return '';
        }
        $root = ROOT_PATH . DS .'wwwroot';
        $realPath = $root . $src;
        if(!file_exists($realPath)){
            return '';
        }

        $info = pathinfo($src);
        $copyName = $info['dirname'].'/'.$info['filename'].'_'.$width.'_'.$height.'.'.$info['extension'];
        $realThumbName = $root . $copyName;
            if(!file_exists($realThumbName)){
                $image = Image::open($realPath);
                $image->crop($width, $height,320,106)->save($realThumbName);
            }

        return $copyName;


    }
}
