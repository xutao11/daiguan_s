//创建和初始化地图函数：
function initMap(id,markers){
    createMap(id,102.250703,26.665352);//创建地图
    setMapEvent();//设置地图事
    addMapControl();//向地图添加控件
    addMapOverlay(markers);//向地图添加覆盖物
}
/**
id:容器ID
lng:地理经度
lat:地理纬度
*/
function createMap(id,lng,lat,zoom=9){
    map = new BMap.Map(id);
    //Point 此类表示一个地理坐标点; lng：地理经度； lat:地理纬度
    map.centerAndZoom(new BMap.Point(lng,lat),zoom);
    //map.centerAndZoom("成都",14);
}
function setMapEvent(){
    map.enableDragging();           //开启标注拖拽功能
    map.enableDoubleClickZoom()     //启用双击放大，默认启用
}

function getLocation(){
    var geo = new BMap.Geolocation();
    return geo.getCurrentPosition
}
function walkTo(lng,lat){
    var point = new BMap.Point(lng,lat);
    var geo = new BMap.Geolocation();
    geo.getCurrentPosition(function(result){
        var render = {
            map   : map,
            panel : "results",
            autoViewport: true
        }
        var walk = new BMap.WalkingRoute(map,{
            renderOptions:render
        });
        map.clearOverlays();
        walk.search(result.point,point);
    });
}
function drivingTo(lng,lat){
    var point = new BMap.Point(lng,lat);
    var geo = new BMap.Geolocation();
    geo.getCurrentPosition(function(result){
        var render = {
            map   : map,
            panel : "results",
            autoViewport: true
        }
        var driving = new BMap.DrivingRoute(map,{
            renderOptions:render
        });
        map.clearOverlays();
        driving.search(result.point,point);
    });
}
function addClickHandler(target,title,content){
    var opts = {
        width: 0,
        height:0,
        title: title,        //信息窗标题文字，支持HTML内容
        enableMessage: false                //是否在信息窗里显示短信发送按钮（默认开启）
    };
    //InfoWindow 此类表示地图上包含信息的窗口。
    //var info = var BMapLib.infoBox(map,contont,opts);
    var currentPoint = target.getPosition();
    var html = "<div>"+content+"<div>";
    html += "<hr /><span>到此景点方式：</span>";
    html += "<a href='javascript:walkTo("+currentPoint.lng+","+currentPoint.lat+");'>步行</a>";
    html += "<a href='javascript:drivingTo("+currentPoint.lng+","+currentPoint.lat+");'>驾车</a></div></div>";
    var infoWindow = new BMap.InfoWindow(html,opts);
    //addEventListener 添加事件监听函数
    target.addEventListener("click",function(){
        //openInfoWindow 在地图上打开信息窗口
        target.openInfoWindow(infoWindow);
    });
}
function addMapOverlay(markers){
    for(var index = 0; index < markers.length; index++ ){
        var point = new BMap.Point(markers[index].position.lng,markers[index].position.lat);
        //Marker 此类表示地图上一个图像标注。param.icon:标注所用的图标对象
        var marker = new BMap.Marker(point,{
            /**
            Icon 此类表示标注覆盖物所使用的图标。
            param.url:图像地址;
            param.size:图标可视区域大小；
            param.imageOffset:图标所用的图片相对于可视区域的偏移值，此功能的作用等同于CSS中的background-position属性
            */
            /**
            Size 此类以像素表示一个矩形区域的大小,
            param.width:水平方向的数值；
            param.height:竖直方向的数值；
            */
            icon:new BMap.Icon("http://api.map.baidu.com/lbsapi/createmap/images/icon.png",new BMap.Size(20,25),{
                imageOffset: new BMap.Size(-45,-20)
            })
        });
        //Label 此类表示地图上的文本标注。param.
        var label = new BMap.Label(markers[index].title,{offset: new BMap.Size(30,5)});
        //setLabel 为标注添加文本标注
        marker.setLabel(label);
        marker.disableMassClear();
        addClickHandler(marker,markers[index].title,markers[index].content);
        //addClickHandler(marker,infoWindow);
        //addOverlay 将覆盖物添加到地图中，一个覆盖物实例只能向地图中添加一次
        map.addOverlay(marker);
    };
}
    //向地图添加控件
function addMapControl(){
    //ScaleControll比例尺控件
    //anchor 控件停靠位置
    var scaleControl = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
    //sietUnit 设置比例尺单位制; BMAP_UNIT_IMPERIAL:英制单位; BMAP_UNIT_METRIC:公制单位
    scaleControl.setUnit(BMAP_UNIT_METRIC);
    //将控件添加到地图，一个控件实例只能向地图中添加一次
    map.addControl(scaleControl);
    //NavigationControl  此类表示地图的平移缩放控件，可以对地图进行上下左右四个方向的平移和缩放操作。
    //type 平移缩放控件的类型
    var navControl = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
    map.addControl(navControl);

    //定位控件
    var locationControll = new BMap.GeolocationControl();
    map.addControl(locationControll);
}
