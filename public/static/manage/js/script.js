if($('#ueditor').length){
    var ue = UE.getEditor('ueditor');
}
if($('.ueditor-voice').length){
    $('.ueditor-voice').each(function () {
        var id = $(this).attr('id');
        var ue = UE.getEditor(id);
    });
}

//删除
if($('.del').length){
    $('.del').click(function(){
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        $.post(url,{'id':id},function(data){
            console.log(data);
            if(data.code == 1){
                alert('删除成功');
                location.reload();
            }else{
                alert(data.msg);
            }
        });
    });
}

//排序
if($('.sort').length){
    $('.sort').click(function(){
        var id = $(this).attr('data-id');
        var sort = $(this).attr('data-sort');
        var url = $(this).attr('data-url');
        $.post(url,{'id':id,'sort':sort},function(data){
            if(data.code == 0){
                alert(data.msg);
            }else{
                location.reload();
            }
        });
    });
}

//推荐
if($('.recommend').length){
    $('.recommend').click(function(){
        var that = $(this);
        var id = that.attr('data-id');
        var param = that.attr('data-param');
        var url = that.attr('data-url');
        $.post(url,{'id':id,'param':param},function(data){
            if(data.code == '1'){
                if(param === "top"){
                    if(data.param == '1'){
                        that.text('取消置顶');
                    }else{
                        that.text('置顶');
                    }
                }else if(param === "hot"){
                    if(data.param == '1'){
                        that.text('取消热门');
                    }else{
                        that.text('热门');
                    }
                }else if(param === "recommend"){
                    if(data.param == '1'){
                        that.text('取消推荐');
                    }else{
                        that.text('推荐');
                    }
                }
            }else{
                alert(data.msg);
            }
        });
    });
}
