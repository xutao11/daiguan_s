var h = $(window).height();
$(document).ready(function(){
	$(".section").height(h);
});
$(window).resize(function() {
	var h = $(window).height();
	$(".section").height(h);
});

$(document).ready(function() {
	$('#fullpage').fullpage({
	    anchors: ['firstPage', 'secondPage', '3rdPage', '4rdPage','lastPage'],
	    css3:true,
	    menu: '#menu',
	    verticalCentered:true,
	    responsiveWidth: 991,
	    responsiveHeight:800
	});
});
$(function() {
    $('div#dx_menu').mmenu({
        offCanvas: {
            position: "right"//打开方式，right，left,bottom
        }, //每个方法结束后进行下一个方法时用“,”分割
        navbar: {
            position: "bottom",
            title: "菜单"//加“[{...}]”去除menu名称
         }
    });
});
