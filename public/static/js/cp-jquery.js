
$(function() {
    $('div#Dx_menu').mmenu({
        offCanvas: {
            position: "right"//打开方式，right，left,bottom
        }, //每个方法结束后进行下一个方法时用“,”分割
        navbar: {
            position: "bottom",
            title: "菜单"//加“[{...}]”去除menu名称
         }
    });
    $('.banner').slick({
        dots: true,
        fade:false,
        arrows:true,
        pauseOnHover:true,
        autoplay:true,
        autoplaySpeed:4000,
        useCSS:true,


    });


    $(window).scroll(function () {  
        var scroll_top;  
        if (document.documentElement && document.documentElement.scrollTop) {  
            scroll_top = document.documentElement.scrollTop;  
        }  
        else if (document.body) {  
            scroll_top = document.body.scrollTop;
        }         
        if(scroll_top >= 100){  
            $('.navbar').addClass("navbar_fixed");
            $(".center").css("margin-top",80);
        }  
        else{  
            $('.navbar').removeClass("navbar_fixed");
            $(".center").css("margin-top",0);
        };
    });
});
