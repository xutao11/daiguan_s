<?php
namespace app\admin\controller;

use app\common\service\ImageService;

class Upload
{
    //语音上传
    /*
    public function voice()
    {
        $file = request()->file('upvoice');
        $fileInfo = $file->getInfo();
        if($fileInfo['type'] == 'audio/mpeg'){
            $info = $file->move(ROOT_PATH.'public'.DS.'uploads');
            $return = [];
            if(!$info){
                $return['state'] = $file->getError();
            }else{
                $return['state'] = 'SUCCESS';
                $voice = '/uploads/'.$info->getSaveName();
                $return['voice'] = $voice;
            }
            return json($return);
        }else{
            $return = [
                'state' => '音频文件必须为MP3格式'
            ];
            return json($return);
        }

    }*/
    //图片上传
    public function image()
    {
        $image = request()->file('upfile');
        $info = $image->move(ROOT_PATH.'public'.DS.'uploads');
        echo "1111";
        var_dump($info);die;

        $return = [];
        if(!$info){
            $return['state'] = $image->getError();
        }else{
            $return['state'] = 'SUCCESS';
            $src = '/uploads/'.$info->getSaveName();
            $return['thumb_src'] = ImageService::getThumb($src);
            $return['src'] = $src;

        }


        return json($return);

    }

    //编辑器中图片上传
    public function ueditor()
    {
		$configPath = ROOT_PATH.'public/static/manage/js/ueditor/config.json';
		$CONFIG = json_decode(preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents($configPath)), true);

		$action = input('get.action','');
		switch($action){
			case 'config':
				$result =  $CONFIG;
				break;
			case 'uploadimage':
				//$result = $this->uploadImage();
                $image = request()->file('upfile');
                $info = $image->move(ROOT_PATH.'public'.DS.'uploads');
                $result = array();
        		if(!$info){
        			$result['state'] = $info->getExtension();
        		}else{
        			$result['state'] = 'SUCCESS';
        			//$return['url'] = '/Uploads/'.$info['savepath'].$info['savename'];
                    $result['url'] = '/uploads/' . $info->getSaveName();
        			$result['title'] = $info->getSaveName();
        			$result['original'] = $info->getFileName();
        			$result['type'] = $info->getType();
        			$result['size'] = $info->getSize();
        		}
				break;
			//图片列表
			//case 'listimage':
				//$result = include('action_list.php');
				//$result = $this->getListImage($size,$start);
				//break;
			default:
				$result = ['state'=> '请求地址出错'];
				break;
		}
		/* 输出结果 */
		if (isset($_GET["callback"])) {
			if (preg_match("/^[\w_]+$/", $_GET["callback"])) {
				return htmlspecialchars($_GET["callback"]) . '(' . $result . ')';
			} else {
                return json(['state' => 'callback参数不合法']);
			}
		} else {
			return json($result);
		}
    }
}
