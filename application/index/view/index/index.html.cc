<?php
use app\common\service\ImageService;
?>
{layout name="layout/index" /}
<link href="/static/css/bos_bootstrap.css" type="text/css" rel="stylesheet" />
<link href="/static/css/bos_animsition.css" type="text/css" rel="stylesheet" />
<link href="/static/css/jquery.fullPage.css" type="text/css" rel="stylesheet" />
<link href="/static/css/jquery.mmenu.all.css" type="text/css" rel="stylesheet" />


<link href="/static/css/slick.css" rel="stylesheet" type="text/css" />
<link href="/static/css/style.css" rel="stylesheet" type="text/css" />
<style>
	.slick-next slick-arrow{
		width: 100px;
	}

</style>
    <!-- header end -->
    <div id="fullpage">
	         <!--1-->
	         <div class="section  fp-section fp-table" id="section0">

				 <div class="banner">

					 {foreach $img as $vo}
					 <div>
						 <?php if(is_mobile()):?>
						 <img src="<?php echo ImageService::getcopy($vo['src'],960,600); ?>" class="img-responsive phone">
						 <?php else:?>
						 <img src="{$vo.src}" class="img-responsive pc">
						 <?php endif;?>
					 </div>
					{/foreach}
				 </div>



	         </div>
	         <!--2-->
	         <div class="section fp-section fp-table" id="section1">
	             <div class="container">
	                 <div class="row  top30">
	                     <div class="col-xs-12 col-sm-6 col-md-6">
	                         <div class="index_name">
	                             <h3>我们是谁？</h3>
	                             <span>您贴心的贷款管家</span>
								 <p>贷管网，是中国本土领先的高度聚焦个人<br>
									 和企业的贷款管理、信用管理、理财规划 <br>
									 解决方案的互联网金融科技平台。<br>
									 贷管网，通过与银行、金融机构、PE/VC的深度合作，<br>
									 为国内个人金融需求者和企业金融需求者提供更专业、<br>
									 更贴心的金融解决方案以及私人管家服务。
								 </p>
							 </div>
	                     </div>

	                     <div class="col-xs-12 col-sm-6 col-md-6">
	                         <img src="__STATIC__/image/img_02.png" class="img-responsive">
	                     </div>
	                 </div>
	             </div>
	             <div class="container top30">
	                 <div class="row">
	                     <div class="col-xs-12 col-sm-12 col-md-12">
	                         <div class="index_name">
	                             <h3>我们能做什么？</h3>
	                             <span>我们提供的服务></span>
	                         </div>
	                     </div>
</div>
	                 <div class="row  doing">
	                     <div class="col-xs-6 col-sm-2 col-md-2 top30">
	                         <a href="#">
	                             <img src="__STATIC__/image/icon_01.png" class="img-responsive">
	                             <h5>贷款管理</h5>
	                         </a>
	                     </div>
	                     <div class="col-xs-6 col-sm-2 col-md-2 top30">
	                         <a href="#">
	                             <img src="__STATIC__/image/icon_02.png" class="img-responsive">
	                             <h5>理财规划</h5>
	                         </a>
	                     </div>
	                     <div class="col-xs-6 col-sm-2 col-md-2 top30">
	                         <a href="#">
	                             <img src="__STATIC__/image/icon_03.png" class="img-responsive">
	                             <h5>信用管理</h5>
	                         </a>
	                     </div>
	                     <div class="col-xs-6 col-sm-2 col-md-2 top30">
	                         <a href="#">
	                             <img src="__STATIC__/image/icon_04.png" class="img-responsive">
	                             <h5>信用贷款</h5>
	                         </a>
	                     </div>
	                     <div class="col-xs-6 col-sm-2 col-md-2 top30">
	                         <a href="#">
	                             <img src="__STATIC__/image/icon_05.png" class="img-responsive">
	                             <h5>抵押贷款</h5>
	                         </a>
	                     </div>
	                     <div class="col-xs-6 col-sm-2 col-md-2 top30">
	                         <a href="#">
	                             <img src="__STATIC__/image/icon_06.png" class="img-responsive">
	                             <h5>金融策划</h5>
	                         </a>
	                     </div>
	                 </div>
	             </div>
	         </div>
	         <!--3-->
	         <div class="section  fp-section fp-table top3" id="section2">
	             <div class="container top30">
	                 <div class="index_data">
	                     <span class="text_blud">大数据</span>&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;&nbsp;<span class="text_blud">人工智能</span>&nbsp;&nbsp; 的数据驱动模式<br/>完美融合于<span class="text_org">金融行业</span>场景应用
	                 </div>
	                 <div class="index_dataImg">
	                     <img src="__STATIC__/image/img_03.png" class="img-responsive">
	                 </div>
	             </div>
	             <div class="container top30">
	                 <div class="row data_list">
						 {foreach name="recommend" item="vo"}
	                         <div class="col-xs-12 col-sm-4 top30">

	                             <a href="/news/detail/{$vo.id}" class="list">
									 <img class="index_img" src="{$vo['src']}">
	                                 <h3><?php echo mb_substr($vo['title'],0,9,'utf8'); ?></h3>
	                                 <p>{$vo.description}</p>
	                             </a>
	                         </div>
						 {/foreach}
						 <div class="col-xs-12 col-sm-4 col-md-4 top30" style="display: none">
							 <a href="#" class="list">
								 <h3>贷款更方便</h3>
								 <p>无需到达门店，在线三步轻松申请，快捷方便轻松省心！</p>
							 </a>
						 </div>
	                     </div>
	             </div>
	         </div>
	         <!--4-->
	         <div class="section  fp-section fp-table top4" id="section3">
	             <div class="container">
	                 <div class="link">
	                     <h3>合作伙伴</h3>
	                     <p>超过100项贷款方案，给你更多选择</p>
	                     <div class="row top30">
	                         {volist name="links" id="item"}
	                         <div class="col_20 top30">
								 <a style=" outline: none;" href="{$item.url}">
	                             	<img src="{$item['src']}" class="img-responsive">
								 </a>
                         </div>
                         {/volist}
                     </div>
                     <div class="more top30">更多信贷合作洽谈中，敬请期待...</div>
                 </div>
             </div>
         </div>
         <!--5-->
         <div class="section  fp-section fp-table" id="section4">
             <div class="container">
                 <div class="cantact">
                     <h3>联系我们 </h3>

                     <h4>中国成都武侯区天府新谷7号楼5楼</h4>
                     <h4>kefu@daiguanyun.com</h4>
                     <img src="__STATIC__/image/img_06.png" class="img-responsive">
                     <h4>扫码添加微信公众号</h4>
                     <h4>微信搜索公众号《贷管网》</h4>
                 </div>
             </div>
         </div>
     </div>

     <div class="index_footer navbar-fixed-bottom">
	         <div class="container">
	             