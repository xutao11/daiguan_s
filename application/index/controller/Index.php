<?php
namespace app\index\controller;

use think\Controller;
use app\common\model\Link;
use think\Db;

class Index extends Controller
{
    public function index()
    {
        cookie('title','贷管网');
        $links = Link::select();
        $img = Db::table('dx_slide')->select();

        //查询推荐的产品
        $recommend = Db::name('article')->where('recommend','1')->limit(6)->select();
        /*echo "<pre/>";
        print_r($recommend);die;*/

        $this->assign('recommend',$recommend);
        $this->assign('img',$img);
        $this->assign('links',$links);

        return view();
    }
}
