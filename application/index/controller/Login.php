<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017/8/17
 * Time: 14:09
 */
namespace app\index\controller;

use function foo\func;
use think\Controller;
use app\common\model\Data;
use think\Db;
use think\Session;
class Login extends Controller{

    public function index(){


        return view('login');
    }

    //登录表单提交
    public function login_from(){



        //对密码进行md5加密
        $pwd = md5($_POST['p']);
        //判断登录
        $re = Db::table('dx_user')->where('user_tel', $_POST['username'])->find();
        if ($re and $re['user_pwd'] == $pwd) {
            //登录状态写入session
            Session::set('is_login', '1');
            Session::set('user_name', $re['user_name']);
            Session::set('user_id', $re['user_id']);
            Session::set('user_tel',$re['user_tel']);
            //登录成功跳转
             $this->redirect(Url('index/personal/index'));
           // $this->success('登录成功。。',Url('index/personal/index'),0);
        } else {
            //登录失败
           // errors('账号或密码错误');


           $this->error('账号或密码错误');
        }

    }
    //注册表单
    public function register(){


        if($_POST['qq']==Session::get('tel_code')){
            echo "手机验证成功";
            $date =[
                'user_tel'=>$_POST['user'],
                'user_pwd'=>md5($_POST['passwd']),
            ];

            //判断该手机号是否注册
            $tel = Db::table('dx_user')->where('user_tel',$date['user_tel'])->select();

            if($tel){
                errors('该手机以注册，请登录');
                //$this->error('该手机以注册，请登录');
            }else{
                $re = Db::table('dx_user')->insert($date);
                if($re){
                    $this->redirect('/query');
                    /*$this->success('注册成功请登录','/query',2);*/
                }else{
                    //errors('注册失败请重试');
                    $this->error('注册失败请重试');
                }
            }
        }else{
           // errors("手机验证失败");
            $this->error('手机验证失败');
        }

    }
    //退出登录
    public function outlogin(){

        Session::clear();
        $this->redirect(Url('index/query/index'));
        //$this->success('安全退出',Url('index/query/index'),2);
    }

}