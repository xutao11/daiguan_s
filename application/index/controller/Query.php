<?php
namespace app\index\controller;

use function foo\func;
use think\Controller;
use app\common\model\Data;
use think\Db;
use think\Session;

class Query extends Controller
{
    //贷款申请
    public function index()
    {
        if(request()->isPost()){

            $item = [
                'name' => input('post.name'),
                'phone' => input('post.phone'),
                'birthday' => input('post.birthday_phone'),
                'province' => input('post.province'),
                'city' => input('post.city'),
                'county' => input('post.county'),
                'sex' => input('post.sex')
            ];
            if($item['birthday'] == ''){
                $item['birthday'] = input('post.jHsDateInput');
            }

            $data = Data::create($item);
            session('data_id',$data->id);
            $this->redirect('/query/second');
        }else{
            cookie('title','贷款查询');
            $is_login =Session::get('is_login');
            if($is_login){
                return view();
            }else{
                $this->redirect('index/login/index');
            }
        }
    }


    public function second()
    {
        if(request()->isPost()){
            $item = [
                'work' => input('post.work/d'),
                'wages' => input('post.wages/d'),
                'way' => input('post.way/d'),
                'credit' => input('post.credit/d')
            ];
            $id = input('post.id/d');
            if(empty($id)){
                $this->error('错误请求',Url('index/indx/index'));
            }else{
                Data::where(['id' => $id])->update($item);
                $this->redirect('/query/third');
            }
        }else{
            $id = session('data_id');
            if(empty($id)){
                $this->error('错误请求',Url('index/indx/index'));
            }
            cookie('title','贷款查询');
            $this->assign('id',$id);
            return view();
        }
    }

    public function third()
    {
        if(request()->isPost()){
            $id = input('post.id/d');
            if(empty($id)){
                $this->error('错误请求',Url('index/indx/index'));
            }
            $item = [
                'house' => input('post.house/d'),
                'car' => input('post.car/d'),
                'card' => input('post.card/d'),
                'life' => input('post.life/d')
            ];
            //房产信息
            if($item['house'] == 2){
                $item['house_value'] = input('post.house_value/d');
            }elseif($item['house'] == 3){
                $item['house_loan'] = input('post.house_loan/d');
                $item['house_months'] = input('post.house_months/d');
            }
            //车产信息
            if($item['car'] == 2){
                $item['car_value'] = input('post.car_value');
            }elseif($item['car'] == 3){
                $item['car_loan'] = input('post.car_loan/d');
                $item['car_months'] = input('post.car_months/d');
            }
            //信用卡信息
            if($item['card'] == 2){
                $item['card_number'] = input('post.card_number/d');
                $item['card_max'] = input('post.card_max/d');
            }
            //寿险信息
            if($item['life'] == 2){
                $item['life_type'] = input('post.life_type/d');
                $item['life_company'] = input('post.life_company');
                $item['life_money'] = input('post.life_money/d');
                $item['life_months'] = input('post.life_months');
            }
            $item['user_id']=Session::get('user_id');
            $item['application_date']=date('Y-m-d',time());
           
            Data::where(['id' => $id])->update($item);
           // success_msg('申请成功，请等待客服人员联系！');
            $this->success('申请成功，请等待客服人员联系！',Url('index/query/index'),2);
        }else{
            $id = session('data_id');
            if(empty($id)){
                //errors('错误请求');
                $this->error('错误请求');
            }
            cookie('title','贷款查询');
            $this->assign('id',$id);
            return view();
        }
    }
    //微信登录
    public function wechatlogin(){

       $openid =  Session::get('openid');
       $re = Db::name('user')->where('openid',$openid)->find();
       if($re){
           //登录状态写入session
           Session::set('is_login', '1');
           Session::set('user_name', $re['user_name']);
           Session::set('user_id', $re['user_id']);
           //登录成功跳转
           $this->redirect(Url('index/query/index'));
           //$this->success('登录成功。。', Url('index/query/index'), 2);
       }else{
            //获取用户资料
          $access_token = $this->get_access_token();
              $arr = $this->get_user($openid,$access_token);

              $data= [
                  'user_name'=>$arr['nickname'],
                  'user_sex'=>$arr['sex'],
                  'user_dizhi'=>$arr['province'].$arr['city'],
                  'user_pwd'=>md5('123456'),
                  'openid'=>$openid,
                  'register_time'=>date('Y-m-d H:i:s'),
                  'edit_time'=>time()
              ];

              $re = Db::name('user')->data($data)->insert();
           Session::set('is_login', '1');
           Session::set('user_name', $re['user_name']);
           Session::set('user_id', $re['user_id']);
           //登录成功跳转

           $this->success('注册成功，默认密码为123456，请及时修改', Url('index/query/index'), 2);
       }
    }
    //获取用户信息
    protected function get_user($openid,$access_token){
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={$access_token}&openid={$openid}&lang=zh_CN";
        $re = file_get_contents($url);
        $re = json_decode($re,true);
        return $re;
    }



    public function get_access_token(){
        $appid = 'wx902deaa1c9227d1f';
        $secret = '81d1d5e2121941e6c0b035fcf7ce5666';
        $get_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$secret}";

        $date = file_get_contents($get_url);


        $date = json_decode($date,true);

        return $date['access_token'];

    }
}
