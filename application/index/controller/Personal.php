<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017/8/15
 * Time: 17:39
 */
namespace app\index\controller;

use think\Controller;
use app\common\model\Article;
use app\common\service\ImageService;
use app\common\model\Category;
use think\Db;
use think\response\Redirect;
use think\Session;

//个人中心
class Personal extends Controller{


    public function index(){
        //判断是否登录
        $islogin = Session::get('is_login');
        if($islogin == '1'){

            //获取登录用户id
            $user_id = Session::get('user_id');
            //获取用户的信息
            $user = Db::name('user')
                ->where('user_id',$user_id)
                ->find();
            //获取用户私人管家的信息
            $service = Db::name('service')
                ->where('ser_id',$user['ser_id'])
                ->find();

            $this->assign('user',$user);
            $this->assign('service',$service);
            return view();

        }else{
            $this->redirect('index/login/index');
        }




    }
    //密码修改
    public function editpwd(){

        $user_id = Session::get('user_id');
        //查询用户信息
        $user = Db::name('user')
            ->where('user_id',$user_id)
            ->find();
        if(request()->isPost()){
            //判断是是否记得密码
            if($_POST['type'] == "pwd"){
                if(md5($_POST['old_pwd']) == $user['user_pwd']){
                    //密码验证成功
                    $re = Db::name('user')->where('user_id',$user_id)->update(['user_pwd'=>md5($_POST['new_pwd'])]);
                    if($re){

                        $this->success('密码修改成功');
                    }else{

                        $this->error('密码修改失败');
                    }

                }else {

                    $this->error('密码错误');
                }
            }elseif($_POST['type'] == "tel"){

                //判断验证码
                $code = Session::get('edit_code');
                if($code == $_POST['imgcode']){
                    $re = Db::name('user')->where("user_id",$user_id)
                        ->update(['user_pwd'=>md5($_POST['password'])]);

                    $this->success('密码修改成功');
                }else{
                    $this->error('验证码错误');
                }

            }

        }else{
             $this->error('非法操作');
        }
    }


    //手机号修改
    public function edittel(){
        $user_id = Session::get('user_id');
        $user = Db::name('user')->where('user_id',$user_id)->find();
        //ajax验证密码
        if(request()->isAjax()){
            if(md5($_POST['pwd']) != $user['user_pwd']){
                echo '0';
            }
        }elseif(request()->isPost()){
           //验证码
            $code = Session::get('edit_code');
            if($code == $_POST['code']){
                $re = Db::name('user')
                    ->where('user_id',$user_id)
                    ->update(['user_tel'=>$_POST['new_tel']]);
               $this->success('修改成功');
            }else{
                $this->error('验证码错误');
            }
        }
    }
}