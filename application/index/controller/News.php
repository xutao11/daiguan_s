<?php
namespace app\index\controller;

use think\Controller;
use app\common\model\Article;
use app\common\service\ImageService;
use app\common\model\Category;
use think\Db;

class News extends Controller
{
    public function detail()
    {
        $id = input('param.id/d');
        $item = Article::get($id);
        $item['category_title'] = Category::get($item['category_id'])['title'];
        
        $this->assign('item',$item);
        cookie('title',$item['title']);

        return view();
    }
    public function index()
    {

        $categoryid = input('param.category_id');
        $category_id = input('param.category/d');
        if($categoryid){
            //点击具体的分类
            $re = Db::name('article')
                ->where('category_id',$categoryid)
                ->paginate(6);
            $type = $categoryid;
        }else{
            //点击贷款超市就显示全部

            //echo $category_id;die;
            //查询子分类
            $categorys = Db::name('category')->where('parent_id',$category_id)->select();

            $category_ids = "";
            foreach ($categorys as $v){
                $category_ids .= $v['id'].",";
            }
            $re = Db::name('article')
                ->where(array('category_id'=>array('IN',$category_ids)))
                ->paginate(6);



            $type = '0';
        }




        $page = $re->render();

        $this->assign('type',$type);
        $this->assign('items',$re);
        $this->assign('page', $page);
        $this->assign('category_id',$category_id);

        cookie('title',Category::get($category_id)['title']);

      return view();
    }


    /*底部贷款超市*/
    public function dibu(){
        $categoryid = input('param.id');
        $category_id = "12";

        $re = Db::name('article')
            ->where('category_id',$categoryid)
            ->paginate(6);
        $type = $categoryid;
        $page = $re->render();

        $this->assign('type',$type);
        $this->assign('items',$re);
        $this->assign('page', $page);
       $this->assign('category_id',$category_id);

        cookie('title',Category::get($category_id)['title']);

        return view('index');


    }
    public function img(){
        $src = '/uploads/20170808/f4361ea72c88b631c0ac067581d79c2f.jpg';
        ImageService::get_roportion($src);
    }
}
