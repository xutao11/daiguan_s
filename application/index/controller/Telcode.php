<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/18
 * Time: 16:44
 */
namespace app\index\controller;
use think\Controller;
use think\Session;
use function wechat\createCode;

require EXTEND_PATH.'SendTemplateSMS.php';

class Telcode extends Controller{

    public function sendcode(){

        $code_img = $_SESSION['imgcode'];
        //注册手机号
        $tel = $_POST['tel'];
        if($_POST['imgcode'] ==strtolower($code_img)){
          //  echo "注意接收手机验证码";
            $charset = '123456789';//随机因子
           $code = $this->createCode($charset,4);
            //向手机发送验证码
           $this->totel($tel,$code);
            Session::set('tel_code',$code);
            if($code == "验证码发送成功"){
                echo "1";
            }
        }else{
            echo '0';
        }
    }
    //生成随机验证码
     function createCode($charset,$codelen) {
        $_len = strlen($charset)-1;
         $code='';
        for ($i=0;$i<$codelen;$i++) {
            $code .= $charset[mt_rand(0,$_len)];
        }
         return $code;
    }
    //发送手机验证码
    public function totel($tel,$code){
        sendTemplateSMS($tel ,array($code,'5'),"194612");

    }
    //微信修改手机号
    public function wechat(){
        $phone = $_POST['tel'];
        $chatset = "123456789";
        $code = $this->createCode($chatset,4);
        $e = $this->totel($phone,$code);

        Session::set('wechat_code',$code);
        echo $e;

    }
    //个人中心修改守家密码是短信验证
    public function edit(){
        if(request()->isAjax()){
            $code = $this->createCode('1234567890',4);
            Session::set('edit_code',$code);
           $re = $this->totel($_POST['tel'],$code);


        /* if($re == '1'){
             Session::set('edit_code',$code);
             echo "1";
         }else{
             echo "0";
         }*/

        }else{
             $this->error('非法操作!!');
        }




    }


}