<?php
namespace app\index\Controller;

use app\common\model\Area as AreaModel;

class Area
{
	function index($parent_id = 0)
	{
		$items = AreaModel::all(['parent_id'=>$parent_id]);
		return $items;
	}
}