<?php
namespace app\index\service;

use app\common\model\Category;
use app\common\service\UriService as UriBase;

class UriService extends UriBase
{
    static public function getCategoryUri($id){
		$uri = '';
		$cate = Category::get($id);
		if(!empty($cate)){
            $param = ['category_id'=>$cate['id']];
			switch($cate['type']){
				case self::ARTICLE:
					$uri = "/news/category/{$id}.html";
					break;
				case self::PAGE:
					$uri = "/page/{$id}.html";
					break;
				case self::IMAGE:
					$uri = Url('Content/image',$param);
					break;
				case self::DOWNLOAD:
					$uri = Url('Content/download',$param);
					break;
				case self::PHOTOS:
					$uri = Url('Content/photos',$param);
					break;
				case self::VIDEO:
					$uri = Url('Content/video',$param);
					break;
				case self::URL:
					$uri = Url('Category/edit',['id'=>$cate['id']]);
					break;
                case self::VOICE:
                    $uri = Url('voice/list',['id'=>$cate['id']]);
                    break;
				default:
					$uri = '';
					break;
			}
		}
		return $uri;
    }
}
