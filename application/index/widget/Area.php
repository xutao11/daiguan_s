<?php
namespace app\index\widget;

//use app\common\model\Area;
use think\View;

class Area
{
    public function index($province=0,$city=0,$count=0)
    {

        $view = new View();
        $view->assign('province', $province);
		$view->assign('city', $city);
		$view->assign('count',$count);
		return $view->fetch('widget/area');
    }
}
