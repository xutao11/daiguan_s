<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
//递归函数
function tree($date,$pid = "0",$level = '0'){

 static $arr = array();
    foreach($date as $k=>$v){
        if($v['parent_id'] == $pid){
            $arr[$k]=$v;
            $arr["$k"]["level"] = $level;



            tree($date,$v['id'],$level+1);

        }

    }
   return $arr;
}

//地址
function area($date,$pid){
    static $dizhi = [];
    foreach ($date as $K=>$v){
        if($v['area_id'] == $pid){
            $dizhi[] = $v['area_name'];
            area($date,$v['parent_id']);
        }

    }

    return $dizhi;


}
//loan（贷款列表）查询已还期数
function repayment_yihuan($loan_id){
        $re = \think\Db::name('repayment')->where('loan_id',$loan_id)
            ->where('repayment_zhuangtai','1')->count();
        echo $re;

}
//获取缩略图
function thunm($src){
    $re = explode('.',$src);
    $t = $re[0].'_960_300.'.$re['1'];
    echo $t;


}
function is_mobile()
{
// 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
    {
        return true;
    }
// 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
    {
// 找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    }
// 脑残法，判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT']))
    {
        $clientkeywords = array (
            'nokia',
            'oppo',
            'xiaomi',
            'miui',
            'huawei',
            'coolpad',
            'sony',
            'ericsson',
            'mot',
            'samsung',
            'htc',
            'sgh',
            'lg',
            'sharp',
            'sie-',
            'philips',
            'panasonic',
            'alcatel',
            'lenovo',
            'iphone',
            'ipod',
            'blackberry',
            'meizu',
            'android',
            'netfront',
            'symbian',
            'ucweb',
            'windowsce',
            'palm',
            'operamini',
            'operamobi',
            'openwave',
            'nexusone',
            'cldc',
            'midp',
            'wap',
            'mobile'
        );
// 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
        {
            return true;
        }
    }
// 协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT']))
    {
// 如果只支持wml并且不支持html那一定是移动设备
// 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
        {
            return true;
        }
    }
    return false;
}

function return_msg($code,$msg,$data=''){
    $arr = [
        'code'=>$code,
        'msg'=>$msg,
        'data'=>$data
    ];
    return json_encode($arr);

}
