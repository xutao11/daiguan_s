<?php
namespace app\admin\service;

use app\common\service\ImageService;

class ToolService
{
    //删除文件
    public static function delFile($path,$isImg = 0)
    {
        if(empty($path)){
            return null;
        }
        $root = ROOT_PATH . 'public';
        if($isImg){
            $thumb = ImageService::getThumb($path);
            if(!empty($thumb)){
                $thumbPath = $root . $thumb;
                if(file_exists($thumbPath)){
                    unlink($thumbPath);
                }
            }
        }
        $filePath = $root.$path;
        if(file_exists($filePath)){
            unlink($filePath);
        }
    }
}
