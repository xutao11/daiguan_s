<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/29
 * Time: 14:21
 */
namespace app\wechat\controller;
use think\Controller;
use think\Db;
use extend\wechat;
use think\Session;
use think\Validate;

class Wechatweb extends Controller{
    //微信请求地址
    public function wechat_get(){


        //用传来的$_GET['state']来判断用户的请求

        if($_GET['state'] == "diyadaikuan"){

            $re = $this->diyadaikuan();
        }elseif ($_GET['state'] == "xinyongdaikuan"){

            $re = $this->xinyongdaikuan();
        }elseif ($_GET['state'] == 'p2p'){

            $re = $this->p2p();
        }elseif ($_GET['state'] == 'xinyongka'){

            $re = $this->xinyongka();
        }elseif ($_GET['state']=='dianziguoqiao'){

            $re = $this->dianziguoqiao();
        }


        $this->assign('items',$re);
        return view('list');


    }
    //我抵押贷款
    public function diyadaikuan(){
        $re = Db::name('article')->where('category_id','14')->select();




        return $re;
    }
    //信用贷款
    public function xinyongdaikuan(){
        $re = Db::name('article')->where('category_id','15')->select();



        return $re;
    }
        //P2P网贷
    public function p2p(){
        $re = Db::name('article')->where('category_id','17')->select();



        return $re;
    }
    //大额信用卡
    public function xinyongka(){
        $re = Db::name('article')->where('category_id','18')->select();



        return $re;
    }
    //垫资过桥
    public function dianziguoqiao(){
        $re = Db::name('article')->where('category_id','19')->select();

        return $re;
    }

    //我的贷款
    public function myloan(){

        $code = $_GET['code'];
        $arr = $this->get_openid_code($code);
        $openid = $arr['openid'];
        $user =Db::name('user')->where('openid',$openid)->find();
        $user_name = $user['user_name'];


       $data = Db::name('user') ->where("openid",$openid)
           ->alias('u')
           ->join('dx_loan l','l.user_id = u.user_id')
           ->where('l.loan_state',"10")
           ->select();
       foreach ($data as $k=>$v){
                $data[$k]['yihuan'] = Db::name('repayment')
                    ->where('repayment_zhuangtai',"1")
                    ->where('loan_id',"{$v['loan_id']}")->count();
       }
        Session::set('is_login', '1');
        Session::set('user_name', $user['user_name']);
        Session::set('user_id', $user['user_id']);
        Session::set('user_tel',$user['user_tel']);

            $this->assign("user_name",$user_name);

        $this->assign('data',$data);
        return view('table');

    }

    //获取openid
    public function get_dates($code){

        $appid = 'wx902deaa1c9227d1f';
        $secret = '81d1d5e2121941e6c0b035fcf7ce5666';
        $url="https://api.weixin.qq.com/sns/oauth2/access_token?appid={$appid}&secret={$secret}&code={$code}&grant_type=authorization_code";

        //请求
        $re = file_get_contents("$url");

        $re = json_decode($re,true);
        return $re;

    }

    //详情
    public function xiangqing(){
        $id = input('param.id');
        $re = Db::name('article')->where('id',$id)->find();
        if($re['isskip'] == "1"){
            $data = file_get_contents("{$re['url']}");

         return $data;
        }else{
            $this->assign('items',$re);
            return view();
        }



    }

    //信用管理
    public function credit(){
        $code = $_GET['code'];
        $arr  =$this->get_openid_code($code);
        $openid = $arr['openid'];
        $user =Db::name('user')->where('openid',$openid)->find();
        $user_name = $user['user_name'];
       //链表查询
        $re = Db::name('user')->where('openid',$openid)
            ->join('dx_credit','dx_credit.user_id = dx_user.user_id')
            ->field('dx_credit.*')
            ->find();
        Session::set('is_login', '1');
        Session::set('user_name', $user['user_name']);
        Session::set('user_id', $user['user_id']);
        Session::set('user_tel',$user['user_tel']);



        $this->assign("user_name",$user_name);
       $this->assign('data',$re);

        return view('credit');
    }

    //存储openid 与code
    protected function get_openid_code($code){
        /* 判断数据库表 wechat_code 中是否存在code==$_GET['code']
          *如果存在就调用同一条数据中的 openid
         * 如果不存在则调用get_dates方法获取openid并存入数据库
         * 同时删除之前的数据
        */
        $re = Db::name('wechat_code')->where('code',"{$_GET['code']}")->find();
        if($re){
            $date = $re;
        }else{
            $arr = $this->get_dates($code);
            $openid = $arr['openid'];
            $access_token = $arr['access_token'];
            $date = [
                'code'=>"$code",
                'openid'=>"$openid",
                'time' =>time(),
                'access_token'=>"$access_token"
            ];
            //插入新数据
            Db::name('wechat_code')->data($date)->insert();
            //删除数据
            Db::name('wechat_code')->where('openid',"$openid")
                ->where('time',"<","{$date['time']}")
                ->delete();
        }
        return $date;

    }


    //贷款评测
    public function daikuanpingce(){
        $code = $_GET['code'];
       $arr = $this->get_openid_code($code);
       $openid = $arr['openid'];
       $access_token = $arr['access_token'];
        Session::set('openid',$openid);
        Session::set('access_token',$access_token);
        $this->redirect('index/query/wechatlogin');
    }


    //我的查询
    public function chaxun(){
        $code = $_GET['code'];
        //获取openid
        $arr = $this->get_openid_code($code);
        $openid = $arr['openid'];
        //进度查询
        $jindu = Db::name('loan')
            ->alias('l')
            ->join('dx_user u','l.user_id = u.user_id')
            ->where('u.openid',"$openid")
            ->field('l.*')
            ->select();
        //借款查询
        $jiekuan = Db::name('data')
            ->alias('d')
            ->join('dx_user u','u.user_id = d.user_id')
            ->where('u.openid',"$openid")
            ->select();
        $user =Db::name('user')->where('openid',$openid)->find();

           Session::set('is_login', '1');
           Session::set('user_name', $user['user_name']);
           Session::set('user_id', $user['user_id']);
           Session::set('user_tel',$user['user_tel']);


        $user_name = $user['user_name'];
        $this->assign("user_name",$user_name);
        $this->assign('jiekuan',$jiekuan);
        $this->assign('jindu',$jindu);
        return view('chaxun');
    }


    //个人中心
    public function myabout(){
        //接受code
        $code = $_GET['code'];
        //获取openid
        $arr = $this->get_openid_code($code);
        $openid = $arr['openid'];
        $access_token = $arr['access_token'];
        $data = $this->user($openid,$access_token);
        $this->assign('data',$data);
        $this->assign('access_token',$access_token);



        return view('myabout');
    }


    //获取用户信息
    public function user($openid,$access_token){
        $url = "https://api.weixin.qq.com/sns/userinfo?access_token={$access_token}&openid={$openid}&lang=zh_CN";
        $re  =file_get_contents($url);
        $re = json_decode($re,true);
        return $re;

    }
    //手机号修改
    public function edittel(){
        if(request()->isPost()){
            $tel = input('param.tel');
            $code = input('param.code');
            $openid = input('param.openid');
            $access_token = input("param.access_token");
           $wechat_code = Session::get('wechat_code');

           if($wechat_code ==$code){
               if(Db::name('user')->where('openid',$openid)->find()){
                   Db::name('user')->where('openid',$openid)->update(['user_tel'=>$tel]);
                   $this->success("修改成功");
               }else{
                    $data = $this->user($openid,$access_token);
                    $date = [
                        'user_name'=>"{$data['nickname']}",
                        'user_sex'=>"{$data['sex']}",
                        'user_dizhi'=>"{$data['province']}"."{$data['city']}",
                        'user_pwd'=>md5('123456'),
                        'openid'=>"{$data['openid']}",
                        'register_time'=>date('Y-m-d H:i:s'),
                        'edit_time'=>time()
                    ];
                    $re = Db::name('user')->data($date)->insert();
                    if($re){
                        $a= <<<EOF
                        <script>
                        alert('注册成功，默认密码为123456，请尽快修改');
                        window.history.back(-1); 
                        
</script>
EOF;
                        echo $a;

                    }
               }
           }else{
               $a= <<<EOF
                        <script>
                        alert('验证码错误');
                        window.history.back(-1); 
                        
</script>
EOF;
               echo $a;
           }
        }else{
            $openid = input('param.openid');
            $access_token = input('param.access_token');
            $this->assign('access_token',$access_token);
           $this->assign('openid',$openid);
            return view('edittel');
        }



    }


  //密码修改
    public function editpwd(){
        if(request()->isPost()){
            $openid = input('param.openid');
            $pwd = input('param.pwd');
            $pwd =md5($pwd);
            $re = Db::name('user')->where('openid',$openid)->update(['user_pwd'=>$pwd]);
            $this->success('密码修改成功');
          //  return view('success');

        }else{
            $openid = input('param.openid');
            $re = Db::name('user')->where('openid',$openid)->find();
            if($re){
                $this->assign('openid',$openid);
                return view('editpwd');
            }else{
                return view('zhuce');
            }

        }


    }

    //私人管家
    public function service(){
        $openid = input('param.openid');
            //查询所属客户的客服
        $date = Db::name('user')->where('openid',$openid)
            ->alias('u')
            ->join('dx_service s','s.ser_id = u.ser_id')
            ->field('s.*')
            ->find();
        $user =Db::name('user')->where('openid',$openid)->find();
        $user_name = $user['user_name'];
        Session::set('is_login', '1');
        Session::set('user_name', $user['user_name']);
        Session::set('user_id', $user['user_id']);
        Session::set('user_tel',$user['user_tel']);

        $this->assign("user_name",$user_name);
        $this->assign('data',$date);
        return view('service');


    }


    //征信查询
    public function zhengxin(){
        return view();
    }
    //自助贷款管理上传
    public function self_help(){
        $openid = input('param.openid');
        $user =Db::name('user')->where('openid',$openid)->find();
        $user_name = $user['user_name'];
        Session::set('is_login', '1');
        Session::set('user_name', $user['user_name']);
        Session::set('user_id', $user['user_id']);
        Session::set('user_tel',$user['user_tel']);
        $this->assign("user_name",$user_name);
        return view();
    }


    //管家服务
    //贷款管理
    public function management(){
        if($_GET['state'] == "loan_management"){

            $category_id = '30';

        }elseif ($_GET['state'] == "credit_management"){
            $category_id = '31';

        }elseif ($_GET['state'] == 'investment_financing'){

            $category_id = '32';
        }
        $re = $this->management_type($category_id);
        $this->assign('item',$re);
        return view('show');
    }
    public function management_type($category_id){
      $data = Db::name("article")
            ->where('category_id',"$category_id")
            //->where('recommend','1')
            ->order("sort asc")
            ->select();
        if($data){
            $re = [
                'title'=>$data['0']['title'],
                'description'=>$data['0']['description'],
                'content'=>$data['0']['content']
            ];
        }else{
            $re = false;
        }


       return $re;

    }






}