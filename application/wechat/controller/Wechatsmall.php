<?php


namespace app\wechat\controller;

use app\admin\model\DataModel;
use app\admin\model\LoanModel;

use app\common\model\UserModel;
use think\Cache;
use think\Controller;
use think\Db;
use extend\wechat;
use think\Session;
use wechat\wechatCallbackapiTest;
use wechat\wxBizDataCrypt;

//require EXTEND_PATH.'CCPRestSmsSDK.php';


//微信接口
class Wechatsmall extends Controller
{
    public static $code;

    public function test()
    {
        $wechat_api = new \wechat\wechatCallbackapiTest();
        $wechat_api->sendTemplateSMS('13688143070', array('123455', '5'), "194612");
    }
    //登录接口
    //（openid）
    public function login_api()
    {
        //唯一标识
        $unionid = input('post.unionid');
        if (!$unionid) return return_msg(1, 'unionid为空');
        $user = $this->get_user($unionid);
        if (!$user) return return_msg(2, '没有该用户');
        return return_msg(0, '用户存在', $user);
    }

    //获取验证码
    public function get_code()
    {


        $tel = input('post.tel');
        if (!$tel) return return_msg(3, '手机号为空');
        //查询该手机号是否绑定
        $tel_user = UserModel::tel_get($tel);
        if ($tel_user) return return_msg(5, '此手机号已绑定用户');
        //发送验证码
        $this->send_code($tel);
        return return_msg(0, '发送成功', Cache::get($tel));
//        return return_msg(0,'发送成功',$_SESSION['wechat_code']);

    }

    //手机修改(绑定)提交
    public function edit_tel()
    {

        $unionid = input('post.unionid');
        $tel = input('post.tel');
        $code = input('post.code');
        Cache::get($tel);
        if (!$code) return return_msg(1, '验证码为空');
        if (Cache::get($tel) == false) return return_msg(10, '验证码不存在');

//            if ($code != $_SESSION['wechat_code']) return return_msg(1, '验证码不正确');
        if ($code != Cache::get($tel)) return return_msg(1, '验证码不正确');

//        if(!isset($_SESSION['wechat_code'])) return return_msg(10,'$_SESSION["wechat_code"]不存在');

//        if(!file_exists('./code/'.$tel.'.txt')) return return_msg(3,'请获取验证码');
//        $code_file = fopen('./code/'.$tel.'.txt','r');
//        $code_num = fread($code_file,filesize('./code/'.$tel.'.txt'));
//        fclose($code_file);
//
//                if ($code !=$code_num) return return_msg(1, '验证码不正确',WECHAT_CODE);
//        if(is_file('./code/'.$tel.'.txt')){
//            unlink('./code/'.$tel.'.txt');
//        }

        if (!$unionid) return return_msg(2, 'unionid为空');
        //查询该电话号码存在否
        $user = $this->get_user($unionid);
        if ($user) {
            //该用户存在 更新
            $re = UserModel::where('unionid', $unionid)->update([
                'user_tel' => $tel,
                'edit_time' => time()
            ]);
            if ($re) {
                return return_msg(0, '更新成功');
            } else {
                return return_msg(4, '更新失败');
            }
        } else {
            //不存在就添加
            $re = UserModel::create([
                'user_tel' => $tel,
                'unionid' => $unionid,
                'edit_time' => time(),
                'user_pwd' => md5('123456')
            ]);
            if ($re) {
                return return_msg(0, '添加成功，初始密码为123456，请及时修改');
            } else {
                return return_msg(5, '添加失败');
            }
        }


        //获取unionid
        $get_data = $this->get_data($js_code);
        if (!$get_data) return return_msg(11, '没有获取到unionid');
        if (!$tel) return return_msg(3, '手机号为空');
        $is_tel = UserModel::where('user_tel', $tel)
            ->where('unionid', '!=', $get_data['unionid'])
            ->count();
        if ($is_tel) return return_msg(7, '手机已存在');
        //查询该openid是否存在
//            $user = $this->get_user($openid);
        $user = UserModel::where('unionid', $get_data['unionid'])
            ->count();
//            if(!$user) return return_msg(5,'没有此用户');
        if ($user) {
            $re = UserModel::where('unionid', $get_data['unionid'])->update([
                'user_tel' => $tel
            ]);
            if ($re) {
                return return_msg(0, '修改成功', $re);
            } else {
                return return_msg(4, '修改失败');
            }
        } else {
            $re = UserModel::create([
//                'user_name'=>$get_user_data['nickname'],
                'user_tel' => $tel,
                'user_pwd' => md5('123456'),
                'edit_time' => time()
//                'user_dizhi'=>$get_user_data['province'].$get_user_data['city'],
//                'openid' =>$openid,
//                'user_sex'=>$get_user_data['sex'] == 1?1:0

            ]);
            if ($re) {
                return return_msg(0, '成功初始密码为123456请尽快修改');
            } else {
                return return_msg(8, '错误！请稍后重试');
            }

//            $wechat_api = new wechatCallbackapiTest();
//            $get_user_data = $wechat_api->user($openid);
//            if(isset($get_user_data['errcode'])) return return_msg(9,'openid错误',$get_user_data);
//            $re = UserModel::create([
//                'user_name'=>$get_user_data['nickname'],
//                'user_tel'=>$tel,
//                'user_dizhi'=>$get_user_data['province'].$get_user_data['city'],
//                'openid' =>$openid,
//                'user_sex'=>$get_user_data['sex'] == 1?1:0
//            ]);
//            if($re){
//                return return_msg(0,'成功初始密码为123456请尽快修改');
//            }else{
//                return return_msg(8,'错误！请稍后重试');
//            }
        }
//        } else {
//            return return_msg(1, '非法请求');
//        }

    }

    //验证码发送
    public function send_code($tel)
    {
        //生成随即验证码
        $code = rand(0, 9999);
        $code = str_pad($code, 4, "0", STR_PAD_LEFT);
        Cache::set($tel, $code, 60);
        return;
//        $_SESSION['wechat_code'] = $code;
//       $code_file = fopen('./code/'.$tel.'.txt','w')or die("Unable to open file!");
//        fwrite($code_file,$code);
//        fclose($code_file);
        //发送验证码
//        $wechat_api = new \wechat\wechatCallbackapiTest();
//        $wechat_api->sendTemplateSMS($tel, array($code, '5'), "194612");


    }

    //进度 查询接口
    public function loan_evaluation()
    {
        $unionid = input('post.unionid');
        if (!$unionid) return return_msg(1, 'unionid为空');
        $user = $this->get_user($unionid);
        if (!$user) return return_msg(2, '用户不存在');
        //查询贷款
        $loans = LoanModel::where('user_id', $user->user_id)->select();
        $loan_state = config('data.loan_state');
        if ($loans) {
            foreach ($loans as $k => $v) {
                $loans[$k]->loan_state = $loan_state[$v->loan_state];
            }
        } else {
            $loans = null;
        }
        return return_msg(0, '查询成功', $loans);


        //状态
        //询此用户的贷款评测
//        if (request()->isAjax()) {
        $js_code = input('post.js_code');
//        $openid = 'oUVWfv3OPlBkzbRORQSCsGsBz4Lg';
        if (!$js_code) return return_msg(2, 'js_code为空');
        //查询此openid是否存在数据库
        $user = $this->get_user($js_code);
        if (!$user) return return_msg(3, '此用户不存在');
        //询此用户的贷款评测
        $loans = LoanModel::where('user_id', $user->user_id)->select();
        $loan_state = config('data.loan_state');
        if ($loans) {
            foreach ($loans as $k => $v) {
                $loans[$k]->loan_state = $loan_state[$v->loan_state];
            }
        } else {
            $loans = null;
        }
        return return_msg(0, '查询成功', $loans);
//
//
//        } else {
//            return return_msg(1, '非法请求');
//        }
    }

    //进度查询
    public function progress_query()
    {

        $unionid = input('post.unionid');
        if ($unionid) return return_msg(1, 'unionid为空');
        $user = $this->get_user($unionid);
        if (!$user) return return_msg(2, '用户不存在');


        $data = DataModel::where('user_id', $user->user_id)->select();
//        $data = DataModel::where('user_id',47)->select();
        if ($data) {
            foreach ($data as $k => $v) {
                if ($v->sex == 'm') {
                    $data[$k]->sex = '男';
                } else {
                    $data[$k]->sex = '女';
                }
                //地址
                $areas = Db::name('areas')->select();
                $dizhi = area($areas, $v->county);
                $data[$k]->dizhi = $dizhi[0] . $dizhi[1] . $dizhi[2];
                //职位
                $works = config('data.work');
                $data[$k]->work = $works[$v->work];
                //月收入
                $wages = config('data.wages');
                $data[$k]->wages = $wages[$v->wages];
                //发薪方式
                $ways = config('data.way');
                $data[$k]->way = $ways[$v->way];
                //征信状况
                $credit = config('data.credit');
                $data[$k]->credit = $credit[$v->credit];
                //信用卡
                $card = config('data.card');
                $data[$k]->card = $card[$v->card];
                //房产
                $house = config('data.house');
                $data[$k]->house = $house[$v->house];
                //还贷月数
                $months = config('data.months');

                $data[$k]->house_months = $v->house_months == '' ? '' : $months[$v->house_months];
                //车产
                $car = config('data.car');
                $data[$k]->car = $car[$v->car];
                //车 还贷时间
                $data[$k]->car_months = $v->car_months == '' ? '' : $months[$v->car_months];
                //寿险保单信息
                $life = config('data.life');
                $data[$k]->life = $life[$v->life];
                //保险险种
                $life_type = config('data.life_type');
                $data[$k]->life_type = $v->life_type == '' ? '' : $life_type[$v->life_type];
//                //实缴期限
                $life_months = config('data.life_months');
                $data[$k]->life_months = $v->life_months == "" ? "" : $life_months[$v->life_months];
            }
        }
        return return_msg(0, 'ok', $data);
    }

    //根据openid 获取用户
    public function get_user($unionid)
    {
        $user = UserModel::where('unionid', $unionid)->find();
        if ($user) {
            return $user;
        } else {
            return false;
        }

    }

    //密码修改
    public function edit_pwd()
    {
        $unionid = input('post.unionid');
        $pwd = input('psot.pwd');
        if (!$unionid) return return_msg(1, 'unionid为空');
        $user = $this->get_user($unionid);
        if (!$user) return return_msg(2, '用户不存在');
        $re = UserModel::where('user_id', $user->user_id)->update([
            'user_pwd' => md5($pwd)
        ]);
        if (!$re) return return_msg(3, '密码修改失败,请稍后重试');
        return return_msg(0, '密码修改成功');


//
//        ============================================================================================================================================
//
//
//
//
//
//
//        $js_code = input('post.js_code');
//        if(!$js_code) return return_msg(1,'js_code为空');
//
//        $user = $this->get_user($js_code);
//        if(!$user) return return_msg(2,'没有查询到该用户');
//        $pwd = input('post.pwd');
//        if(!$pwd) return return_msg(3,'密码为空');
//        //获取unionid
//        $get_data = $this->get_data($js_code);
//        if(!$get_data) return return_msg(5,'unionid没有获取到');
//        $re = UserModel::where('user_id',$user->user_id)->update([
//            'user_pwd'=>md5($pwd)
//        ]);
//        if($re){
//            return return_msg(0,'修改成功',$user);
//        }else{
//            return return_msg(4,'修改失败，请重试');
//        }
    }

    //贷款评测
    public function loan_shengqing()
    {
        $unionid = input('post.unionid');
        if (!$unionid) return return_msg(1, 'unionid为空');
        //查询用户
        $user = $this->get_user($unionid);
        if (!$user) return return_msg(2, '用户不存在');
        //姓名
        $re = DataModel::create([
            'user_id' => $user->user_id,
            'name' => input('post.name')??'',
            'phone' => input('post.phone')??'',
            'birthday' => input('post.birthday')??'',
            'province' => input('post.province')??'',
            'city' => input('post.city')??'',
            'county' => input('post.county')??'',
            'sex' => input('post.sex')??'m',
            'work' => input('post.work')??null,
            'wages' => input('post.wages')??null,
            'way' => input('post.way')??null,
            'credit' => input('post.credit')??null,
            'house' => input('post.house')??null,
            'house_value' => input('post.house_value')??null,
            'house_loan' => input('post.house_loan')??null,
            'house_months' => input('post.house_months')??null,
            'car' => input('post.car')??null,
            'car_value' => input('post.car_value')??null,
            'car_loan' => input('post.car_loan')??null,
            'car_months' => input('post.car_months')??null,
            'card' => input('post.card')??null,
            'card_number' => input('post.card_number')??null,
            'card_max' => input('post.card_max')??null,
            'life' => input('post.life')??null,
            'life_type' => input('post.life_type')??null,
            'life_company' => input('post.life_company')??null,
            'life_money' => input('post.life_money')??null,
            'life_months' => input('post.life_months')??null,
            'application_date' => date('Y-m-d', time()),
            'reply' => input('post.reply')??null,
        ]);
        if ($re) {
            return return_msg(0, '提交成功', $re);
        } else {
            return return_msg(1, '提交失败');
        }


    }


    //微信小程序解密
    public function decrypt($sessionKey, $encryptedData, $iv)
    {

        $appid = 'wxa02e44170bc722cd';

//        $encryptedData="bfmmvj/4A0Hh/mnxTQho0eK8CB0V0a+3d6zNEFrkQLOoGzDWItq5cEdtnR2WGZ0LM6/YS0vdhuhODCxE0p7gvRwQvnMR7rSbiLUfKKvSOlNwicYrKWpFIvAo2NjNMm0eVqRy72ZGZGFwTQD72MDKzHtgKg9ZQh/WwlTaj1lotlZ94ab0XTx7ZqKsBlqoWU0kRf8ilZ63rRtHWJFQedIigc0UQZ7HdE36wxXcqQnNjt4sIUCKebUnNSHua85hzjeYoVxi8lO/q5ZTA5vSHpXNNhSAmhTyIT7T9h0G/rIcZXTi4UIjWl8E/csbWE0Az5dFj0a+mxvy3it24QdFGY8KOh9AaQBXCY/ljU+kimkiOADxS9wrg65Gsf5ukxK2Hgmz9V9vouJHcyNU9muY0RAUPJsrLb15GSGvxOhWkPEMc3pJPkwTr1ISfP4PMumjCdhBCZDTzzDkR0TC+DTkiNrwRA==";
//        $iv = 'XWfbAVbEjZewhdn3Z5AkGQ==';
        $pc = new \wechat\wxBizDataCrypt($appid, $sessionKey);
        $errCode = $pc->decryptData($encryptedData, $iv, $data);
        if ($errCode == 0) {
//            print($data . "\n");
            return return_msg(0, 'ok', $data);
        } else {
//            print($errCode . "\n");
            return return_msg(1, '', $errCode);
        }
    }

    //获取sessionKey unionid
    public function get_data($js_code)
    {
        $appid = 'wxa02e44170bc722cd';
        $secret = 'c6830e265995ebc3a3ad593018c78a3b';
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid=' . $appid . '&secret=' . $secret . '&js_code=' . $js_code . '&grant_type=authorization_code';
        $re = file_get_contents($url);

        $re = json_decode($re, true);
//        return $re;

        if (isset($re['errcode'])) {
            return return_msg(1, $js_code, $re);
        } else {
            return $re;
        }
//        if(isset($re['errcode'])){
//            return false;
//        }else{
//            var_dump($re);
//        }
    }

    //curl
    protected function curl_date($url, $date)
    {
        //初始化
        $ch = curl_init();
        //设置请求地址
        curl_setopt($ch, CURLOPT_URL, $url);
        //捕获内容但不输出
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //模拟post发送
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $date);

        //执行
        $re = curl_exec($ch);
        //判断是否发送成功
        if ($re == false) {
            echo curl_error($ch);
        } else {
            echo $re;
        }
        //关闭
        curl_close($ch);
    }
}


?>