<?php


namespace app\wechat\controller;

use think\Controller;
use think\Db;
use extend\wechat;
use think\Session;
use wechat\wechatCallbackapiTest;

//require EXTEND_PATH.'CCPRestSmsSDK.php';


//微信接口
class Index extends Controller
{


    //curl
    protected function curl_date($url, $date)
    {
        //初始化
        $ch = curl_init();
        //设置请求地址
        curl_setopt($ch, CURLOPT_URL, $url);
        //捕获内容但不输出
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //模拟post发送
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $date);

        //执行
        $re = curl_exec($ch);
        //判断是否发送成功
        if ($re == false) {
            echo curl_error($ch);
        } else {
            echo $re;
        }
        //关闭
        curl_close($ch);
    }

    //自定义菜
    public function menu()
    {
        $assess_token = $this->get_access_token();
        //curl初始化
        $ch = curl_init();
        //请求地址
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token={$assess_token}";

        curl_setopt($ch, CURLOPT_URL, $url);


        //捕获内容但不输出
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //模拟post发送
        curl_setopt($ch, CURLOPT_POST, 1);

        //数据

        //贷管云线上
        $date = '{
             "button":[
              {
                   "name":"贷款超市",
                   "sub_button":[
                   {	
                       "type":"view", 
                       "name":"抵押贷款",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fwechat_get&response_type=code&scope=snsapi_userinfo&state=diyadaikuan#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"信用贷款",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fwechat_get&response_type=code&scope=snsapi_userinfo&state=xinyongdaikuan#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"P2P网贷",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fwechat_get&response_type=code&scope=snsapi_userinfo&state=p2p#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"大额信用卡",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fwechat_get&response_type=code&scope=snsapi_userinfo&state=xinyongka#wechat_redirect"
                    },
                   
                    {
                       "type":"view",
                       "name":"垫资过桥",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fwechat_get&response_type=code&scope=snsapi_userinfo&state=dianziguoqiao#wechat_redirect"
                    }]
               },
                {
                   "name":"管家服务",
                   "sub_button":[
                   {	
                       "type":"view",
                       "name":"贷款评测",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fdaikuanpingce&response_type=code&scope=snsapi_userinfo&state=diyadaikuan#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"贷款管理",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fmanagement&response_type=code&scope=snsapi_userinfo&state=loan_management#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"信用管理",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fmanagement&response_type=code&scope=snsapi_userinfo&state=credit_management#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"投资理财",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fmanagement&response_type=code&scope=snsapi_userinfo&state=investment_financing#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"征信查询",
                       "url":"http://www.daiguanyun.com/wechat/wechatweb/zhengxin"
                    }]
               },
                {
                   "name":"我的管家",
                   "sub_button":[
                   {	
                       "type":"view",
                       "name":"贷款管理",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fmyloan&response_type=code&scope=snsapi_userinfo&state=diyadaikuan#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"信用管理",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fcredit&response_type=code&scope=snsapi_userinfo&state=diyadaikuan#wechat_redirect"
                    },
                    {	
                       "type":"view",
                       "name":"我要查询",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fchaxun&response_type=code&scope=snsapi_userinfo&state=diyadaikuan#wechat_redirect"
                    },
                   
                    {
                       "type":"view",
                       "name":"个人中心",
                       "url":"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx902deaa1c9227d1f&redirect_uri=http%3a%2f%2fwww.daiguanyun.com%2fwechat%2fwechatweb%2fmyabout&response_type=code&scope=snsapi_userinfo&state=diyadaikuan#wechat_redirect"
                    }]
               }
               ]
         }';
        curl_setopt($ch, CURLOPT_POSTFIELDS, $date);

        //执行
        $re = curl_exec($ch);
        //判断是否发送成功
        if ($re == false) {
            echo curl_error($ch);
        } else {
            echo $re;
        }
        //关闭
        curl_close($ch);
    }

    //设置消息推送对的模板
    public function tmplate()
    {
        //调用获取assess_token的方法
        $assess_token = $this->get_access_token();
        //请求地址
        $url = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token={$assess_token}";
        $date = '    {
              "industry_id1":"8",
              "industry_id2":"9"
           }';
        //调用curl方法
        $this->curl_date($url, $date);

    }

    //获取设置的行业信息
    public function get_template()
    {
        //获取assess_token
        $assess_token = $this->get_access_token();
        //请求地址
        $url = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token={$assess_token}";
        //模拟get请求
        $re = file_get_contents($url);
        var_dump($re);
    }

    //获取模板id
    public function get_tplid()
    {
        //获取assess_token
        $assess_token = $this->get_access_token();
        //请求地址
        $url = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token={$assess_token}";
        //请求参数
        $date = '   {
           "template_id_short":"TM00015"
       }';
        //调用curl方法模拟post请求
        $this->curl_date($url, $date);
    }

    //获取模板列表
    public function get_tpllist()
    {
        //获取assess_token
        $assess_token = $this->get_access_token();
        //请求地址
        $url = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token={$assess_token}";
        //模拟get请求
        $re = file_get_contents($url);
        var_dump($re);

    }

    //还款消息发送
    protected function to_msguser($dates)
    {


        //获取assess_token
        $assess_token = $this->get_access_token();
        //请求地址
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$assess_token}";
        //请求数据
        $date = [
            "touser" => "{$dates['openid']}",
            "template_id" => "yAG2V_CBrcsXfunHErrYs82QyVUHJZgJPvz0Pj4cduM",

            "data" => [
                "first" => [
                    "value" => "尊敬的{$dates['turename']}{$dates['user_sex']}:\n您好，贷管网温馨提醒：您的{$dates['pingtai']}于{$dates['loan_date']}到期，请于{$dates['repayment_date']}日前存入{$dates['repayment_money']}元到您尾号为{$dates['bank_card']}的{$dates['bank']}银行卡，剩余还款期数还有{$dates['loan_all_qishu']}期",
                    "color" => "#173177"
                ],
                "keyword1" => [
                    "value" => "{$dates['pingtai']}",
                    "color" => "#173177"
                ],
                "keyword2" => [
                    "value" => "{$dates['repayment_money']}元",
                    "color" => "#173177"
                ],
                "keyword3" => [
                    "value" => "第{$dates['loan_qishu']}期",
                    "color" => "#173177"
                ],
                "keyword4" => [
                    "value" => "{$dates['repayment_date']}日",
                    "color" => "#173177"
                ],
                "remark" => [
                    "value" => "祝您工作顺利，生活愉快^_^",
                    "color" => "#173177"
                ],

            ]
        ];
        $t = json_encode($date, true);

        //模拟post发送
        $this->curl_date($url, $t);

    }

    //判断是否有符合发送的客户
    public function getuser()
    {


        //生日提醒
//        $this->user_birthday();
        //还款提醒
        $this->loan();
        //信用卡tixing
//        $this->bank_msg();
//        $this->is_loan();

    }


    //到了还款日 默认还款成功
    public function is_loan()
    {
        $date = date("Y-m-d", time());

        $data = Db::name('repayment')->where('moneths_date', $date)->select();
        if ($data) {
            foreach ($data as $k => $v) {
                Db::name('repayment')->where('repayment_id', $v['repayment_id'])->update(['repayment_zhuangtai' => '1']);
            }
        }
    }


    //获取assess_token
    public function get_access_token()
    {
        $appid = 'wx902deaa1c9227d1f';
        $secret = '81d1d5e2121941e6c0b035fcf7ce5666';
        $get_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$secret}";

        $date = file_get_contents($get_url);


        $date = json_decode($date, true);

        return $date['access_token'];

    }


//token 验证
    public function ver_token()
    {
        $wechat_api = new \wechat\wechatCallbackapiTest();
$wechat_api->valid();
      //  $wechat_api->responseMsg();
    }
    public function test(){
       var_dump($_SESSION['wechat_code']);
        $wechat_api = new \wechat\wechatCallbackapiTest();
        $wechat_api->sendTemplateSMS('18582596534' ,array($_SESSION['wechat_code'],'5'),"194612");
    }


    protected function birthday_msg($dates)
    {

        //获取assess_token
        $assess_token = $this->get_access_token();
        //请求地址
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$assess_token}";
        //请求数据
        $date = [
            "touser" => "{$dates['openid']}",
            "template_id" => "gj2l_DpJlDZHQV0duVDt0SMUOMvcG7Nq0q3dGAPsTQg",

            "data" => [
                "first" => [
                    "value" => "",
                    "color" => "#173177"
                ],
                "keyword1" => [
                    "value" => "qqqq",
                    "color" => "#173177"
                ],
                "keyword2" => [
                    "value" => "2222",
                    "color" => "#173177"
                ],
                "keyword3" => [
                    "value" => "44444",
                    "color" => "#173177"
                ],
                "remark" => [
                    "value" => "祝您生活愉快！！",
                    "color" => "#173177"
                ],

            ]
        ];
        $t = json_encode($date, true);

        //模拟post发送
        $this->curl_date($url, $t);

    }

//还款判断
    protected function loan()
    {
        //获取今天的日期
        $today1 = date('Y-m-d', time());

        //获取今天该发送的日期

        for ($i = 0; $i <= 2; $i++) {
            $today_3 = date("Y-m-d", strtotime("+{$i} days", strtotime($today1)));


            $dates = Db::name('repayment')->where('moneths_date', $today_3)
                ->alias('r')
                ->join('dx_loan l', 'l.loan_id = r.loan_id')
                ->join('dx_user u', 'l.user_id = u.user_id')
                ->select();
            //银行卡后四位
            foreach ($dates as $v) {
                $re = [
                    'user_tel' => $v['user_tel'],
                    'user_name' => $v['user_name'],
                    'turename' => "{$v['turename']}",
                    'repayment_money' => "{$v['repayment_money']}",
                    'loan_date' => "{$v['moneths_date']}",
                    'loan_all_qishu' => "{$v['loan_all']}" . '/' . "{$v['loan_all']}" - "{$v['repayment_qishu']}",
                    'openid' => $v['openid'],
                    'pingtai' => $v['loan_type'] . '/' . $v['loan_pingtai'],
                    'repayment_date' => $v['repayment_date'],
                    'bank_card' => substr($v['bank_card'], -4),
                    'loan_qishu' => "{$v['repayment_qishu']}",
                    'bank' => "{$v['bank']}"

                ];
                $re['user_sex'] = "{$v['user_sex']}==1" ? "先生" : "女士";

                //调用发送
                $this->to_msguser($re);

            }
        }


    }

    //信用卡还款提醒
    public function bank_msg()
    {
        //获取今天的日期
        $day = date('d');
        //获取本月天数
        $days = date('t');
        $today = date("Y-m-d", time());

        $re = Db::name('loan')->where('loan_type', '信用卡')->field('loan_id,repayment_date')->select();
        foreach ($re as $k => $v) {

            if ($days < $re[$k]['repayment_date']) {
                $BeginDate = date('Y-m-01', strtotime(date("Y-m-d")));
                //本月最后一天
                $re[$k]['repayment_date'] = date('Y-m-d', strtotime("$BeginDate +1 month -1 day"));
            } else {
                $re[$k]['repayment_date'] = date('Y-m-') . str_pad($v['repayment_date'], 2, "0", STR_PAD_LEFT);
            }
        }
        //符合发送的日期；
        for ($i = 0; $i <= 2; $i++) {
            $date = date('Y-m-d', strtotime("+{$i} days", strtotime($today)));
            foreach ($re as $k => $v) {
                if ($v['repayment_date'] == $date) {
                    $data = Db::name('loan')->where('loan_id', $v['loan_id'])->find();
                    $user = Db::name('user')->where('user_id', $data['user_id'])->find();
                    $data['repayment_date'] = $date;
                    $user['user_sex'] = "{$user['user_sex']}==1" ? "先生" : "女士";
                    $this->bank_card($user, $data);

                }
            }
        }

    }

    //信用卡模板
    public function bank_card($user, $data)
    {

        //获取assess_token
        $assess_token = $this->get_access_token();
        //请求地址
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$assess_token}";
        //请求数据
        $date = [
            "touser" => "{$user['openid']}",
            "template_id" => "gj2l_DpJlDZHQV0duVDt0SMUOMvcG7Nq0q3dGAPsTQg",

            "data" => [
                "first" => [
                    "value" => "尊敬的{$user['turename']}{$user['user_sex']}:\n您的{$data['loan_pingtai']}{$data['loan_type']}还款日{$data['repayment_date']}马上要到了，请及时处理",
                    "color" => "#173177"
                ],
                "keyword1" => [
                    "value" => "{$user['user_tel']}",
                    "color" => "#173177"
                ],
                "keyword2" => [
                    "value" => "{$user['turename']}",
                    "color" => "#173177"
                ],
                "keyword3" => [
                    "value" => "{$data['repayment_date']}",
                    "color" => "#173177"
                ],
                "remark" => [
                    "value" => "祝您工作顺利，生活愉快^_^",
                    "color" => "#173177"
                ],

            ]
        ];
        $t = json_encode($date, true);

        //模拟post发送
        $this->curl_date($url, $t);

    }


    //查看自动回复配置
    public function peizhi()
    {
        $access_token = $this->get_access_token();

        $url = "https://api.weixin.qq.com/cgi-bin/get_current_autoreply_info?access_token=$access_token";
        $re = file_get_contents($url);
        $re = json_decode($re, true);
        echo "<pre/>";
        print_r($re);
    }

    //生日提醒
    public function user_birthday()
    {
        $access_token = $this->get_access_token();

        $date = date('m-d', time());
        $re = Db::name('user')->select();

        foreach ($re as $k => $v) {
            // echo $v['user_birthday'];

            if ($date == substr($v['user_birthday'], 5, 10)) {
                $url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$access_token}";
                $text = '尊敬的' . $v['user_name'] . ':\n贷管网祝您生日快乐，身体健康！';
                $date = '{
                            "touser":"' . $v['openid'] . '",
                            "msgtype":"text",
                            "text":
                            {
                                 "content":"' . $text . '"
                            }
                        }';

                //$date = json_encode($date,true);
                $this->curl_date($url, $date);
            }
        }

    }


}


?>