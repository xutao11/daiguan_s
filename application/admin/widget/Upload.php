<?php
namespace app\admin\widget;

use think\View;

class Upload
{
    //语音
    public function voice($voices=[],$append='')
    {
        $view = new View();
        $view->assign('voices',$voices);
        $view->assign('append',$append);
        return $view->fetch('widget/voice');
    }

    //图片
    public function image($src='',$append='')
    {
        $view = new View();
        $view->assign('src',$src);
        $view->assign('append',$append);
        return $view->fetch('widget/image');
    }
}
