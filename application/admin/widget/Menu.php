<?php
namespace app\admin\widget;

use app\common\model\Category;
use think\View;

class Menu
{
    public function index($category_id)
    {

        $items = Category::where(['parent_id'=>0])->where("type <> ".Category::URL)->select();


        $view = new View();
        $view->assign('category_id', $category_id);
		$view->assign('menus', $items);
		return $view->fetch('widget/menu');
    }

    public function left()
    {
        $view = new View();
        $view->assign('controller_name',request()->controller());
        return $view->fetch('widget/left');
    }
}
