<?php
namespace app\admin\controller;

use app\common\model\Slide as SlideModel;
use app\admin\service\ToolService;

class Slide extends Base
{
    public function del()
    {
        if(request()->isAjax()){
            $id = input('post.id/d');
            $item = SlideModel::get($id);   //图片
            $msg = ['code'=>0,'msg'=>'fail'];
            if(!empty($item)){
                if(!empty($item['src'])){
                    ToolService::delFile($item['src'],1);
                }
                SlideModel::destroy($id);
                $msg = ['code'=>1,'msg'=>'ok'];
            }
            return json($msg);
        }
        $this->error('无此操作');
    }

    //排序
    public function sort()
    {
        if(request()->isAjax()){
            $id = input('post.id/d');
            $sort = input('post.sort');
            $slide = SlideModel::get($id);
            $msg = ['code' => 0,'msg' => 'fail'];
            if(!empty($slide) && in_array($sort,['up','down'])){
                if($sort == 'up'){
                    $prev = SlideModel::where("sort < {$slide['sort']}")
                    ->order("sort desc")
                    ->limit(1)
                    ->find();
                    if(!empty($prev)){
                        SlideModel::where(['id' => $id])->update(['sort' => $prev['sort']]);
                        SlideModel::where(['id' => $prev['id']])->update(['sort' => $slide['sort']]);
                        $msg = ['code' => 1,'msg' => 'ok'];
                    }else{
                        $msg = ['code' => 0,'msg' => '无需调整排序'];
                    }
                }else{
                    $next = SlideModel::where("sort > {$slide['sort']}")
                    ->order("sort asc")
                    ->limit(1)
                    ->find();
                    if(!empty($next)){
                        SlideModel::where(['id' => $id])->update(['sort' => $next['sort']]);
                        SlideModel::where(['id' => $next['id']])->update(['sort' => $slide['sort']]);
                        $msg = ['code' => 1,'msg' => 'ok'];
                    }else{
                        $msg = ['code' => 0,'msg' => '无需调整排序'];
                    }
                }
            }
            return json($msg);
        }
        $this->error('无此操作');
    }

    //编辑
    public function edit()
    {
		if(request()->isPost()){
            $item = [];
			$item['title'] = input('post.title');
            $item['url'] = input('post.url');
            $item['description'] = input('post.description');
			$img = input('post.img');
            $id = input('post.id/d');

			if(!empty($img)){
				$oldItem = SlideModel::get($id);
				if(!empty($oldItem['src'])){
				    ToolService::delFile($oldItem['src'],1);
				}
				$item['src'] = $img;
			}
			SlideModel::where(['id' => $id])->update($item);
			$this->redirect('Slide/index');
		}else{
			$id = input('param.id/d');

			$item = SlideModel::get($id);
			$this->assign('item',$item);
			return view();
		}
    }

    //添加
    public function add()
    {
        if(request()->isPost()){
            $item = [];
			$item['title'] = input('post.title');
            $item['url'] = input('post.url');
            $item['description'] = input('post.description');
			$img = input('post.img');
			if(!empty($img)){
				$item['src'] = $img;
			}
            SlideModel::create($item);

			$this->redirect('Slide/index');
		}else{
			return view();
		}
    }

    //列表
    public function index()
    {
        $items = SlideModel::order("sort asc")->select();
        $this->assign('items',$items);
        return view();
    }
}
