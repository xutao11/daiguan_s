<?php

namespace app\admin\controller;
use app\admin\model\LoanModel;
use app\admin\model\RepaymentModel;
use app\common\service\ImageService;
use think\Controller;
use think\Db;
use think\Url;
use think\Validate;



//贷款控制器
class Loan extends Controller{




	public function add(){
        $user_id = input('param.user_id');
        //更新用户修改时间
        Db::name('user')->where('user_id',$user_id)->update(['edit_time'=>time()]);
        $user_name=input('param.user_name');
        $re = Db::table('dx_user')->where('user_id',$user_id)->find();
        $this->assign('user_name',$re['turename']);
        $this->assign('user_id',$user_id);
		return view();

	}


	public function type(){
	    if(!empty($_POST)){
            if($_POST['type_id'] == '信用卡'){
                $_POST['repayment_type'] = '';
            }
            //loan表单提交
            $date_loan = [
                'loan_state'=>$_POST['loan_state'],
                'loan_type'=>$_POST['type_id'],
                'loan_pingtai' => $_POST['loan_pingtai'],
                'loan_all' => $_POST['loan_all'],
                'repayment_date'=>$_POST['repayment_date'],

                'start_date' => $_POST['jHsDateInput'],
                'loan_money' => $_POST['loan_money'],
                'user_id'=>$_POST['user_id'],
                'repayment_type'=>$_POST['repayment_type'],
                'bank' => $_POST['bank'],
                'bank_card'=>$_POST['bank_card'],
                'edit_time'=>time(),
                "add_time"=>time()

            ];
            //数据验证
            $yanzheng = [
                'loan_type'=>'require',
                'loan_pingtai' => 'require',
                'loan_all' => 'number|between:1,120',
                'bank_card'=>'number'
            ];
            $msg = [
                'loan_type.require'=>'贷款类型必选',
                'loan_pingtai.require' => '贷款平台不能为空',
                'loan_all.number' => '贷款分期必须为数字',
                'loan_all.between' => '贷款分期数最多120个月',
                'loan_money.number' => '贷款金额必须为数字',
                'bank_card.number' => '还款卡号位数字'

            ];
            $validate = new Validate($yanzheng, $msg);
            $re = $validate->check($date_loan);

            if (!$re) {

                $this->error($validate->getError());
            } else {
                //写入贷款表
                $re = Db::table('dx_loan')->insert($date_loan);
                $loan_id =Db::table('dx_loan')->getLastInsID();
                $e = Db::table('dx_loan')->where('loan_id',$loan_id)->find();
                if($_POST['type_id'] == '信用卡'){
                    if($re){
                        $r =<<<R
                    <script>alert('添加成功');</script>
R;

                        echo $r;
                        $this->assign('id',$e['user_id']);
                        return view('content/tiaozhuan');
                    }else{
                        $this->error('添加失败，请稍后重试', Url('admin/loan/loan_list'), 2);
                    }

                }
                    if (isset($re)) {
                        //判断贷款状态
                        //如果放款完成则进入每月的详情
                        if($_POST['loan_state'] == '10'){

                            $this->success('添加成功,请添加还款详情', Url('admin/repayment/addshow',['user_id'=>$date_loan['user_id'],'loan_id'=>$loan_id]), 2);
                        }else{
                            $r =<<<R
                    <script>alert('添加成功');</script>
R;

                            echo $r;
                            $this->assign('id',$e['user_id']);
                            return view('content/tiaozhuan');
                            //如果没有放款完成则进入贷款列表

                            //$this->success('添加成功',Url('admin/loan/loan_list'),2);
                        }

                    } else {
                        $this->error('添加失败，请稍后重试', Url('admin/loan/loan_list'), 2);
                    }


            }
        }else{
            $this->error('非法操作....');
        }

    }

    //客户贷款列表
    public function loan_list(){
      
        //接受user_id
        $user_id  =input('param.user_id');
        if($user_id){
            //更新用户修改时间
            Db::name('user')->where('user_id',$user_id)->update(['edit_time'=>time()]);
            //贷款表与客户表联表查询    指定用户
            $model = Db::table('dx_loan')
                ->alias('l')
                ->join('dx_user u','l.user_id = u.user_id')
                ->where('l.user_id',$user_id)
                ->order('l.edit_time desc')
                ->paginate(8);

            $this->assign('page',$model->render());

        }else {
            //贷款表与客户表联表查询    全部用户
            $model = Db::table('dx_loan')
                ->alias('l')
                ->join('dx_user u','l.user_id = u.user_id',"LEFT")
                ->order('l.edit_time desc')
                ->paginate(8);
            $this->assign('page',$model->render());
        }
        $arr = [];
      foreach($model as $v){
            if($v['loan_state'] == "10"){
                $count = Db::name("repayment")
                    ->where('loan_id',$v['loan_id'])
                    ->where("repayment_zhuangtai","1")
                    ->count();
                $v['yihuan']=$count;
                $arr[] = $v;

            }else{
                $arr[]=$v;
            }
        }
        $this->assign('items',$arr);
        return view('list');
    }

    public function del(){
        $re = Db::table("dx_loan")->where('loan_id',$_POST['loan_id'])->delete();
        $re1 =Db::table('dx_repayment')->where('loan_id',$_POST['loan_id'])->delete();
        if($re){
            echo '删除成功';
        }else{
            echo '删除失败';
        }
    }
    //修改贷款类型
    public function edit(){
        if (!empty($_POST)){
            //修改时间
            Db::name('loan')->where('loan_id',$_POST['loan_id'])->update(['edit_time'=>time()]);
            if($_POST['type_id'] == '信用卡'){
                $_POST['repayment_type'] = '';
                $_POST['loan_all'] = "";
            }
            //loan表单修改提交
            $date_loan = [
                'loan_type'=>$_POST['type_id'],
                'loan_pingtai' => $_POST['loan_pingtai'],
                'loan_all' => $_POST['loan_all'],
                'start_date' => $_POST['jHsDateInput'],
                'loan_money' => $_POST['loan_money'],
                'user_id'=>$_POST['user_id'],
                'repayment_type'=>$_POST['repayment_type'],
                'repayment_date'=>$_POST['repayment_date'],
                'loan_state'=>$_POST['loan_state'],
                'bank' => $_POST['bank'],
                'bank_card'=>$_POST['bank_card'],
                'edit_time'=>time()
            ];
            //数据验证
            $yanzheng = [
                'loan_type'=>'require',
                'loan_pingtai' => 'require',
                'loan_all' => 'number|between:1,120',
                'loan_alr' => 'number|between:0,120',
                'bank_card'=>'number'
            ];
            $msg = [
                'loan_type.require'=>'贷款类型必选',
                'loan_pingtai.require' => '贷款平台不能为空',
                'loan_all.number' => '贷款分期必须为数字',
                'loan_all.between' => '贷款分期数最多120个月',
                'loan_alr.number' => '贷款分期必须为数字',
                'loan_money.number' => '贷款金额必须为数字',
                'bank_card.number' => '还款卡号位数字'
            ];
            $validate = new Validate($yanzheng, $msg);
        
            $re = $validate->check($date_loan);
            if (!$re) {

                $this->error($validate->getError());
            } else {

               $re = Db::table('dx_loan')->where('loan_id', $_POST['loan_id'])
                    ->update($date_loan);
                if($_POST['type_id'] == '信用卡'){
                    $this->success('添加成功',Url('admin/loan/loan_list'),2);
                }

                if (isset($re)) {
                    if($_POST['loan_state'] == '10'){
                        $this->success('修改成功', Url('admin/repayment/edit_form',['user_id'=>$date_loan['user_id'],'loan_id'=>$_POST['loan_id']]), 2);
                    }else{
                        $this->success('修改成功',Url('admin/loan/loan_list'),2);
                    }

                } else {
                    $this->error('修改失败，请稍后重试',Url('admin/loan/loan_list'), 2);
                }
            }
               Db::table('dx_loan')->where('loan_id',$_POST['loan_id'])->update([
                  'loan_type'=>$_POST['type_id'],
               ]);
               $re = Db::table('dx_repayment')->where('repayment_id',$_POST['repayment_id'])->find();

               $this->assign('user_name',$_POST['turename']);
               $this->assign('loan_id',$_POST['loan_id']);
               $this->assign('loan_type',$_POST['type_id']);
               $this->assign('error',null);
               $this->assign('re',$re);
               $this->assign('repayment_id',$_POST['repayment_id']);
               return view('tianjiaedit');
        }else{
            //修改页面
            $loan_id = input('param.loan_id');
            
            $user_id = input('param.user_id');
            $re = Db::table('dx_loan')->where('loan_id',$loan_id)->find();
            $usery = Db::table('dx_user')->where('user_id',$user_id)->find();

            $this->assign('re',$re);
            $this->assign('loan_id',$loan_id);
            $this->assign('user_name',$usery['turename']);
            $this->assign('user_id',$user_id);
            return view('addedit');
        }

    }
    //修改贷款具体
    public function edittianjia()
    {

        //数据验证
        $yanzheng = [
            'loan_pingtai' => 'require',
            'repayment_all' => 'number|between:1,120',
            'repayment_alr' => 'number|between:1,120',
            'start_date' => 'require',

        ];
        $msg = [
            'loan_pingtai.require' => '贷款不能为空',
            'repayment_all.number' => '贷款分期必须为数字',
            'repayment_all.between' => '贷款分期数必须小于120',
            'repayment_alr.number' => '贷款分期必须为数字',
            'user_dizhi.require' => '客户所在不能为空',
            'start_date.require' => '贷款起始时间不能为空',
            'loan_money.number' => '贷款金额必须为数字',

        ];
        $date = [
            'loan_id' => $_POST['loan_id'],
            'loan_pingtai' => $_POST['loan_pingtai'],
            'repayment_all' => $_POST['repayment_all'],
            'repayment_alr' => $_POST['repayment_alr'],
            'start_date' => $_POST['jHsDateInput'],
            'loan_money' => $_POST['loan_money']

        ];
        $validate = new Validate($yanzheng, $msg);
        $re = $validate->check($date);
        if (!$re) {

            $this->error($validate->getError(),Url('admin/loan/loan_list'));
        } else {

            $re = Db::table('dx_repayment')->where('repayment_id', $_POST['repayment_id'])
                ->update($date);
            if (isset($re)) {
                $this->success('修改成功', Url('admin/loan/loan_list'), 2);
            } else {
                $this->error('修改失败，请稍后重试',Url('admin/loan/loan_list'), 2);
            }
        }


    }




    public function qingchu(){
        $date_loan = Db::table('dx_loan')->select();
        foreach ($date_loan as $v){
            $data =Db::table('dx_repayment')->where('loan_id',$v['loan_id'])->select();
            $count= count($data);
            if($count==0){

                $re = Db::table('dx_loan')->where('loan_id',$v['loan_id'])->delete();

            }
        }
    }


}























?>