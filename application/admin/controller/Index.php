<?php
namespace app\admin\controller;

use \think\Controller;

class Index extends Controller
{
    //首页，做页面跳转
    public function index()
    {
        $this->redirect('Content/index');
    }
}
