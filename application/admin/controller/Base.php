<?php
namespace app\admin\controller;

use think\Controller;
use daxiang\Auth;

class Base extends Controller
{
    protected function _initialize(){
		$sess_auth = session('auth');
		if(!$sess_auth){
			$this->redirect('Login/index');
		}
		//如果是超级管理员，不用验证权限，给予所有权限

		if($sess_auth['uid']==1){
			return true;
		}
		if($sess_auth['uid'] > 1){
		    return true;
        }
        $module = request()->module();
        $controller = request()->controller();
        $action = request()->action();
		$name = $module.'/'.$controller.'/'.$action;
		$auth = new Auth();
        $id = $auth->check($name,$sess_auth['uid']);
		if($id = $auth->check($name,$sess_auth['uid'])){
			return true;
		}else{
            if(request()->isAjax()){
                return json(['code' => 0,'msg' => '没有权限']);
            }else{
                $this->error("没有权限");
            }
		}
	}
}
