<?php
namespace app\admin\controller;

use app\common\model\Category;
use app\common\model\Article;
use app\admin\service\CategoryService;
use app\admin\service\UriService;
use app\common\model\Video;
use app\common\model\Voice;
use app\common\model\WechatArticle;

class Content extends Base
{
	//微信文章
    public function wechat()
    {
        $cate_id = input('param.category_id');
        $keyword = input('param.keyword');
        $where = ['category_id' => $cate_id];
        if($keyword!=''){
            $where['title'] = array('LIKE',"%$keyword%");
            $this->assign('keyword',$keyword);
        }
        $list = WechatArticle::where($where)
        ->order("sort desc,create_time desc")
        ->paginate(20);

        $this->assign('list',$list);
        $this->assign('page',$list->render());
        $this->assign('category_id',$cate_id);
        return view();
    }
	//视频
    public function video()
    {
        $cate_id = input('param.category_id/d');
        $where = ['category_id' => $cate_id] ;
        $list = Video::where($where)
        ->order("sort desc,create_time desc")
        ->paginate();

        $this->assign('list',$list);
        $this->assign('page',$list->render());
        $this->assign('category_id',$cate_id);
        return view();
    }
    //语音
    public function voice()
    {
        $cate_id = input('param.category_id');
		$keyword = input('param.keyword');
		$where = ['category_id' => $cate_id];
		if($keyword!=''){
			$where['title'] = array('LIKE',"%$keyword%");
            $this->assign('keyword',$keyword);
		}
        $list = Voice::where($where)
        ->order("sort desc,create_time desc")
        ->paginate();

		$this->assign('list',$list);
		$this->assign('page',$list->render());
		$this->assign('category_id',$cate_id);
		return view();
    }
    //文章
    public function article()
    {
        $cate_id = input('param.category_id');
		$keyword = input('param.keyword');
		$where = ['category_id' => $cate_id];
		if($keyword!=''){
			$where['title'] = array('LIKE',"%$keyword%");
            $this->assign('keyword',$keyword);
		}
        $list = Article::where($where)
        ->order("top desc,recommend desc,hot desc,sort desc,create_time desc")
        ->paginate(20);


		$this->assign('list',$list);
		$this->assign('page',$list->render());
		$this->assign('category_id',$cate_id);
		return view();
    }

    //做页面跳转
    public function index()
    {
        $items = CategoryService::getTopCate();
        $items = Category::where(['parent_id'=>0])->where("type <> ".Category::URL)->order(['sort'=>'asc','id'=>'asc'])->select();

		if(!empty($items)){

			$first = $items[0];
            $children = Category::where(['parent_id'=>$first['id']])->where('type','<>',Category::URL)->order(['sort'=>'asc','id'=>'asc'])->select();

            if(count($children)){
				$url = UriService::getCategoryUri($children[0]['id']);
                $children_son = Category::where(['parent_id'=>$children[0]['id']])->where('type','<>',Category::URL)->order(['sort'=>'asc','id'=>'asc'])->select();

			}else{
				$url = UriService::getCategoryUri($first['id']);
			}
			if(!empty($url)){
				$this->redirect($url);
			}
		}else{
			$this->redirect('Category/add');
		}
    }

    //单页
    public function page()
    {
		if(request()->isPost()){
            $item = [];
			$item['title'] = input('post.title');
            $item['category_id'] = input('post.category_id/d');
            $item['description'] = input('post.description');
            $item['content'] = input('post.content');
			$img = input('post.img');
			if(!empty($img)){
				$item['src'] = $img;
			}
            $count = Article::where(['category_id' => $item['category_id']])->count();
			//判断是添加还是编辑
			if($count){
				//编辑
                Article::where("category_id= {$item['category_id']}")->update($item);
			}else{
				//添加记录
                Article::create($item);
			}
			$this->redirect('Content/page?category_id='.$item['category_id']);
		}else{
			$category_id = input('param.category_id');
			$category = Category::get($category_id);
			if(empty($category)){
				$this->redirect('Content/index');
			}
			$item = Article::where(['category_id'=> $category_id])->find();
			$this->assign('item',$item);
			$this->assign('category_id',$category_id);
            return view();
		}
    }
}
