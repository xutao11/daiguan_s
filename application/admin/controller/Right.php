<?php
namespace app\admin\controller;

use app\admin\model\AuthGroup;
use app\admin\model\AuthRule;

class Right extends Base
{
    public function delRule()
    {
        if (request()->isAjax()) {
			$id = input('post.id/d');
            $msg = ['code'=>0,'msg'=>'fail'];
            $item = AuthRule::get($id);
			if(!empty($item)){
                AuthRule::destroy($id);
                $msg = ['code'=>1,'msg'=>'ok'];
            }
            return json($msg);
		}
        $this->error('无此操作');
    }
    //编辑权限
    public function editRule()
    {
        if(request()->isPost()){
            $item = [
                'title' => input('post.title'),
                'name' => strtolower(input('post.name')),
                'status' => input('post.status',1)
            ];
            $id = input('post.id/d');
            $where = "name='{$item['name']}' and id <> $id";
            $rule = AuthRule::where($where)->find();
            $rule2 = AuthRule::get($id);
            if(!empty($rule)){
                $this->error('已存在相同权限');
            }elseif(empty($rule2)){
                $this->error('无此权限');
            }else{
                AuthRule::where(['id' => $id])->update($item);
                $this->redirect('Right/rule');
            }
        }else{
            $id = input('param.id/d');
            $item = AuthRule::get($id);
            if(empty($item)){
                $this->error('无此权限');
            }else{
                $this->assign('item',$item);
                return view();
            }
        }
    }
    public function addRule()
    {
        if(request()->isPost()){
            $item = [
                'title' => input('post.title'),
                'name' => strtolower(input('post.name')),
                'status' => input('post.status',1),
                'parent_id' => input('post.parent_id/d')
            ];
            $id = input('post.id/d');
            $rule = AuthRule::where(['name' => $item['name']])->find();
            if(!empty($rule)){
                $this->error('已存在相同权限');
            }else{
                AuthRule::create($item);
                $this->redirect('Right/rule');
            }
        }else{
            $parent_id = input('param.parent_id/d',0);
            $this->assign('parent_id',$parent_id);
            return view();
        }
    }
    //权限列表
    public function rule()
    {
        $list = AuthRule::where(['parent_id' => 0])->select();
        $this->assign('items',$list);
        return view();
    }
    //角色权限设置
    public function groupRule()
    {
        if(request()->isPost()){
            $rules = input('post.rules/a',[]);
            $group_id = input('post.group_id/d');
            $group = AuthGroup::get($group_id);;
            if(empty($group)){
                $this->error('无此角色');
            }else{
                AuthGroup::where(['id' => $group_id])->update(['rules' => implode(',',$rules??[])]);
                $this->redirect('Right/group');
            }
        }else{
            $group_id = input('param.group_id/d');
            $group = AuthGroup::get($group_id);
            if(!empty($group)){
                $rules = AuthRule::where(['parent_id' => 0])->select();
                $this->assign('group_id',$group_id);
                $this->assign('rules',$rules);
                $this->assign('group',$group);
                return view();
            }else{
                $this->error('无此角色');
            }
        }
    }
    //删除角色
    public function delGroup()
    {
        if (request()->isAjax()) {
			$id = input('post.id/d');
            $msg = ['code'=>0,'msg'=>'fail'];
            $item = AuthGroup::get($id);
			if(!empty($item)){
                AuthGroup::destroy($id);
                $msg = ['code'=>1,'msg'=>'ok'];
            }
            return json($msg);
		}
        $this->error('无此操作');
    }
    //编辑角色
    public function editGroup()
    {
        if(request()->isPost()){
            $item = [
                'title' => input('post.title'),
                'status' => input('post.status')
            ];
            $id = input('post.id/d');
            AuthGroup::where(['id' => $id])->update($item);
            $this->redirect('Right/group');
        }else{
            $id = input('param.id/d');
            $item = AuthGroup::get($id);
            $this->assign('item',$item);
            return view();
        }
    }
    //添加角色
    public function addGroup()
    {
        if(request()->isPost()){
            $item = [
                'title' => input('post.title'),
                'status' => input('post.status')
            ];
            AuthGroup::create($item);
            $this->redirect('Right/group');
        }else{
            return view();
        }
    }
    //角色列表
    public function group()
    {
        $list = AuthGroup::select();
        $this->assign('list',$list);
        return view();
    }
}
