<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/8/2
 * Time: 10:33
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;


//信用管理控制器
class Credit extends Controller{

    //表单页面显示
    public function showfrom(){
        $user_id = input('param.user_id');
        //更新用户修改时间
        Db::name('user')->where('user_id',$user_id)->update(['edit_time'=>time()]);


        
        $re = Db::name('credit')->where('user_id',$user_id)->find();
        if($re){
            $this->assign('data',$re);
        }else{
            $this->assign('data',null);
        }
        $user = Db::name('user')->where('user_id',$user_id)->find();
        $this->assign('user_id',$user_id);
        $this->assign('user_name',$user['user_name']);
        return view('credit');

    }
    //表单提交
    public function getfrom(){

        //查询该客户是否有记录
        $_POST['time']=time();
        $re=Db::name('credit')->where('user_id',$_POST['user_id'])->count();
        if($re){
            $return = Db::name('credit')->where('user_id',$_POST['user_id'])->data($_POST)->update();
        }else{
            $return = Db::name('credit')->data($_POST)->insert();
        }
        if($return){

            $this->success("保存成功",Url('admin/user/userlist'),2);

        }else{
            $this->error("保存失败，请稍后重试");
        }
    }


}