<?php
namespace app\admin\controller;

use app\common\model\Video as VideoModel;
use app\admin\service\ToolService;

class Video extends Base
{
    public function del()
    {
        if (request()->isAjax()) {
			$id = input('post.id/d');
            $msg = ['code'=>0,'msg'=>'fail'];
            $item = VideoModel::get($id);
			if(!empty($item)){
                if(!empty($item['img'])){
                    ToolService::delFile($item['img'],1);
                }

                VideoModel::destroy($id);
                $msg = ['code'=>1,'msg'=>'ok'];
            }
            return json($msg);
		}
        $this->error('无此操作');
    }
    //排序
    public function sort()
    {
        if(request()->isPost()){
			$id = input('post.id/d');
			$sort = input('post.sort');
            $msg = ['code'=>0,'msg'=>'fail'];
            if($id > 0 && in_array($sort,['up','down'])){
                $item = VideoModel::get($id);
                if(empty($item)){
                    $msg = ['code'=>0,'msg'=>'无此视频'];
                }else{
                    if($sort == 'up'){
                        $prev = VideoModel::where("category_id='{$item['category_id']}' and sort  > {$item['sort']}")
                            ->order("sort asc")
                            ->limit(1)
                            ->find();
                        if(!empty($prev)){
                            VideoModel::where(['id' => $id])->update(['sort' => $prev['sort']]);
                            VideoModel::where(['id' => $prev['id']])->update(['sort' => $item['sort']]);
                            $msg = ['code' => 1,'msg' => 'ok'];
                        }else{
                            $msg = ['code'=>0,'msg'=>'无须调整排序'];
                        }
                    }elseif($sort == 'down'){
                        $next = VideoModel::where("category_id='{$item['category_id']}' and sort < {$item['sort']}")
                            ->order("sort desc")
                            ->limit(1)
                            ->find();
                        if(!empty($next)){
                            VideoModel::where(['id' => $id])->update(['sort' => $next['sort']]);
                            VideoModel::where(['id' => $next['id']])->update(['sort' => $item['sort']]);
                            $msg = ['code' => 1,'msg' => 'ok'];
                        }else{
                            $msg = ['code'=>0,'msg'=>'无须调整排序'];
                        }
                    }
                }
			}
            return json($msg);
		}
        $this->error('无此操作');
    }
    //编辑
    public function edit()
    {
        if(request()->isPost()){
            $item = [
                'title' => input('post.title'),
                'description' => input('post.description'),
                'src' => input('post.src')
            ];
            $id = input('post.id/d');
            $img = input('post.img');

            $video = VideoModel::get($id);
            if(!empty($img)){
                //删除之前图片
                if(!empty($video['img'])){
                    ToolService::delFile($video['img'],1);
                }
                $item['img'] = $img;
            }
            VideoModel::where(['id' => $id])->update($item);
            $this->redirect('Content/video?category_id='.$video['category_id']);
        }else{
            $id = input('param.id');
            $item = VideoModel::get($id);
            $this->assign('item',$item);
            $this->assign('category_id',$item['category_id']);
            return view();
        }
    }
    //添加
    public function add()
    {
        if(request()->isPost()){
            $item = [
                'title' => input('post.title'),
                'description' => input('post.description'),
                'src' => input('post.src'),
                'type' => input('post.type'),
                'category_id' => input('post.category_id')
            ];
            $img = input('post.img');

            if(!empty($img)){
                $item['img'] = $img;
            }
            VideoModel::create($item);
            $this->redirect('Content/video?category_id='.$item['category_id']);
        }else{
            $category_id = input('param.category_id/d');
            $this->assign('category_id',$category_id);
            return view();
        }
    }
}
