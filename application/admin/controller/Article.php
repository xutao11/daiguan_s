<?php
namespace app\admin\controller;

use app\common\model\Article as ArticleModel;
use app\admin\service\ToolService;

class Article extends Base
{
    //删除
    public function del()
    {
        if (request()->isAjax()) {
			$id = input('post.id/d');
            $msg = ['code'=>0,'msg'=>'fail'];
            $item = ArticleModel::get($id);
			if(!empty($item)){
                if(!empty($item['src'])){
                    ToolService::delFile($item['src'],1);
                }
                ArticleModel::destroy($id);
                $msg = ['code'=>1,'msg'=>'ok'];
            }
            return json($msg);
		}
        $this->error('无此操作');
    }

    //排序
    public function sort()
    {
        if(request()->isPost()){
			$id = input('post.id/d');
			$sort = input('post.sort');
            $msg = ['code'=>0,'msg'=>'fail'];
            if($id > 0 && in_array($sort,['up','down'])){
                $item = ArticleModel::get($id);
                if(empty($item)){
                    $msg = ['code'=>0,'msg'=>'无此文章'];
                }else{
                    if($sort == 'up'){
                        $prev = ArticleModel::where("category_id='{$item['category_id']}' and sort  > {$item['sort']}")
                            ->order("sort asc")
                            ->limit(1)
                            ->find();
                        if(!empty($prev)){
                            ArticleModel::where(['id' => $id])->update(['sort' => $prev['sort']]);
                            ArticleModel::where(['id' => $prev['id']])->update(['sort' => $item['sort']]);
                            $msg = ['code' => 1,'msg' => 'ok'];
                        }else{
                            $msg = ['code'=>0,'msg'=>'无须调整排序'];
                        }
                    }elseif($sort == 'down'){
                        $next = ArticleModel::where("category_id='{$item['category_id']}' and sort < {$item['sort']}")
                            ->order("sort desc")
                            ->limit(1)
                            ->find();
                        if(!empty($next)){
                            ArticleModel::where(['id' => $id])->update(['sort' => $next['sort']]);
                            ArticleModel::where(['id' => $next['id']])->update(['sort' => $item['sort']]);
                            $msg = ['code' => 1,'msg' => 'ok'];
                        }else{
                            $msg = ['code'=>0,'msg'=>'无须调整排序'];
                        }
                    }
                }
			}
            return json($msg);
		}
        $this->error('无此操作');
    }

    //推荐
    public function recommend()
    {
        if(request()->isAjax()){
	        $id = input('post.id/d');
            $param = input('post.param');
            $msg = ['code'=>0,'msg'=>'fail'];
            if(in_array($param,['top','hot','recommend'])){
                $item = ArticleModel::get($id);
                if($item[$param] == '1'){
                    $val = '0';
                }else{
                    $val = '1';
                }
                ArticleModel::where(['id' => $id])->update(["{$param}"=>$val]);
                $msg = ['code'=>1,'msg'=>'ok','param'=>$val];
            }else{
                $msg = ['code'=>0,'msg'=>'操作失败'];
            }
            return json($msg);
        }
        $this->error('无此操作');
    }

    //编辑
    public function edit()
    {
        if(request()->isPost()){
            $item = [];
            $item['title'] = input('post.title');
            $item['description'] = input('post.description');
            $item['recommend'] = input('post.recommend',0);
            $item['top'] = input('post.top',0);
            $item['hot'] = input('post.hot',0);
            $item['content'] = input('post.content');
            $item['seo_title'] = input('post.title');
            $item['seo_keyword'] = input('post.seo_keyword');
            $item['seo_description'] = input('post.seo_description');
            $item['isskip'] = input('post.isskip');

        	$img = input('post.img');
            $id = input('post.id/d');

            $article = ArticleModel::get($id);
			if(!empty($img)){
			    //删除之前图片
				if(!empty($article['src'])){
				    ToolService::delFile($article['src'],1);
				}
				$item['src'] = $img;
			}
			ArticleModel::where(['id' => $id])->update($item);
			$this->redirect('Content/article?category_id='.$article['category_id']);
		}else{
			$id = input('param.id');
			$item = ArticleModel::get($id);
			$this->assign('item',$item);
            $this->assign('category_id',$item['category_id']);
            $this->assign('tags', ArticleModel::$tags);
            
            return view();
		}
    }
    /*public function editwechat()
    {
        $id = input('param.id');
        $item = WechatModel::get($id);
        $this->assign('item',$item);
        $this->assign('category_id',$item['category_id']);
        $this->assign('tags', ArticleModel::$tags);
        return view();
    }*/

    //添加
    public function add()
    {
        if(request()->isPost()){
            $item = [];
            $item['title'] = input('post.title');
            $item['description'] = input('post.description');
            $item['recommend'] = input('post.recommend',0);
            $item['top'] = input('post.top',0);
            $item['hot'] = input('post.hot',0);
            $item['content'] = input('post.content');
            $item['category_id'] = input('post.category_id/d');
            $item['seo_title'] = input('post.title');
            $item['seo_keyword'] = input('post.seo_keyword');
            $item['seo_description'] = input('post.seo_description');
            $item['url'] = input('post.url');
            $item['isskip'] = input('post.isskip');

        	$img = input('post.img');
        	if(!empty($img)){
        		$item['src'] = $img;
        	}
            ArticleModel::create($item);
        	$this->redirect('Content/article?category_id='.$item['category_id']);
        }else{
        	$category_id = input('param.category_id');
        	$this->assign('category_id',$category_id);
        	return view();
    	}
    }
    public function addwechat()
    {
        $category_id = input('param.category_id');
        $this->assign('category_id',$category_id);
        $this->assign('tags', ArticleModel::$tags);
        return view();
    }
}
