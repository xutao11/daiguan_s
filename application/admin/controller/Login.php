<?php
namespace app\admin\controller;

use think\Controller;
use app\common\model\Member;

class Login extends Controller
{
    public function index()
    {
		if(request()->isPost()){
			$username = input('post.username');
			$password = input('post.password');
			if(empty($username) || empty($password)){
				$this->error('用户名或密码不可为空！');
			}

			$where = ['username'=>$username,'password'=>md5($password)];
			$item = Member::where($where)->find();
            if(!empty($item)){
                $auth = ['uid'=>$item['id'],'uname'=>$item['username']];
                session('auth',$auth);
                $this->redirect('Index/index');
            }
			$this->error('请重新登录!',Url('Login/index'));
		}else{
			return view();
		}
    }



}
