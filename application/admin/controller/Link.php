<?php
namespace app\admin\controller;

use app\common\model\Link as LinkModel;
use app\admin\service\ToolService;

class Link extends Base
{
    //删除
    public function del()
    {
        if(request()->isAjax()){
            $id = input('post.id/d');
            $item = LinkModel::get($id);   //图片
            $msg = ['code' => 0,'msg' => 'fail'];
            if(!empty($item)){
                if(!empty($item['src'])){
                    ToolService::delFile($item['src'],1);
                }
                LinkModel::destroy($id);
                $msg = ['code' => 1,'msg' => 'ok'];
            }
            return json($msg);
        }
        $this->error('无此操作');
    }

    //排序
    public function sort()
    {
        if(request()->isAjax()){
            $id = input('post.id/d');
            $sort = input('post.sort');
            $item = LinkModel::get($id);
            $msg = ['code' => 0,'msg' => 'fail'];
            if(!empty($item) && in_array($sort,['up','down'])){
                if($sort == 'up'){
                    $prev = LinkModel::where("sort < {$item['sort']}")
                    ->order("sort desc")
                    ->limit(1)
                    ->find();
                    if(!empty($prev)){
                        LinkModel::where(['id' => $id])->update([ 'sort' => $prev['sort']]);
                        LinkModel::where(['id' => $prev['id']])->update(['sort' => $item['sort']]);
                        $msg = ['code' => 1,'msg' => 'ok'];
                    }
                }else{
                    $next = LinkModel::where("sort > {$item['sort']}")
                    ->order("sort asc")
                    ->limit(1)
                    ->find();
                    if(!empty($next)){
                        LinkModel::where(['id' => $id])->update(['sort' => $next['sort']]);
                        LinkModel::where(['id' => $next['id']])->update(['sort' => $item['sort']]);
                        $msg = ['code' => 1,'msg' => 'ok'];
                    }
                }
            }
            return json($msg);
        }
        $this->error('无此操作');
    }

    //编辑
    public function edit()
    {
        if(request()->isPost()){
            $item = [];
            $item['title'] = input('post.title');
            $item['url'] = input('post.url');
            $id = input('post.id/d');
            $img = input('post.img');
            if(!empty($img)){
                $oldItem = LinkModel::get($id);
                if(!empty($oldItem['src'])){
                    ToolService::delFile($oldItem['src'],1);
                }
                $item['src'] = $img;
            }
            LinkModel::where(['id' => $id])->update($item);
            $this->redirect('Link/index');
        }else{
            $id = input('param.id/d');
            $item = LinkModel::get($id);
            $this->assign('item',$item);
            return view();
        }
    }

    //添加
    public function add()
    {
        if(request()->isPost()){
            $item = [];
            $item['title'] = input('post.title');
            $item['url'] = input('post.url');
            $img = input('post.img');
            if(!empty($img)){
                $item['src'] = $img;
            }
            LinkModel::create($item);
            $this->redirect('Link/index');
        }else{
            return view();
        }
    }

    public function index()
    {
        $items = LinkModel::order("sort asc")->select();
        $this->assign('items',$items);
        return view();
    }
}
