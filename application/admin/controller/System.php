<?php
namespace app\admin\controller;

use app\common\model\System as SystemModel;

class System extends Base
{
    public function index()
    {
        if(request()->isPost()){
            $item = I('item');
            $img_size = I('img_size');
            $file_size = I('file_size');
            $banner = I('post.img');
            if($banner){
                $item['banner'] = $banner;
            }
            /*$mark_img = I('post.img');
            if($mark_img){
                $item['mark_img'] = $mark_img;
            }*/
            if($img_size){
                $item['img_size'] = I('img_size')*1024*1024;
            }
            if($file_size){
                $item['file_size'] = I('file_size')*1024*1024;
            }
            $model = M('system');
            $system = System::get();
            if(empty($system)){
                $model->add($item);
            }else{
                $model->where("id='{$system['id']}'")->save($item);
            }
            $this->redirect('System/index');
        }else{
            $this->error('无此操作');
            $item = SystemModel::get();
            $this->assign('item',$item);
            return view();
        }
    }
}
