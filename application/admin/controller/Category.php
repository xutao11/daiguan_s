<?php
namespace app\admin\controller;

use app\common\model\Category as CategoryModel;
use app\admin\service\CategoryService;
use app\common\model\Article;
use app\admin\service\ToolService;
use think\Db;

class Category extends Base
{
    //列表
    public function index()
    {

        $items = CategoryModel::where(['parent_id' => 0])->order('sort asc')->select();
      // $items = Db::name('category')->where(['parent_id' => 0])->order('sort asc')->select();
       // $items = tree($items);
       // var_dump($items);die;
        $this->assign('items',$items);
        return view();
    }

    //删除
    public function del()
    {
        if(request()->isAjax()){
			$id = input('post.id/d');
            $cate = CategoryModel::get($id);
            $msg = ['code'=>0,'msg'=>'参数错误'];
            if(!empty($cate)){
                if(CategoryService::hasChildren($id)){
                    $msg = ['code'=>0,'msg'=>'此栏目有下级栏目，不可删除'];
                }else{
                    // TODO 目前只有[文章|单页|链接]分类,根据后期添加，再修改
                    if($cate['type'] == CategoryModel::ARTICLE){
                        $count = Article::where("category_id='{$id}'")->count();
                        if($count > 0){
                            $msg = ['code'=>0,'msg'=>'此栏目有文章，不可删除'];
                        }else{
                            ToolService::delFile($cate['src']);    //栏目图片
                            CategoryModel::destroy($id);
                            $msg = ['code'=>1,'msg'=>'ok'];
                        }
                    }elseif($cate['type'] == CategoryModel::PAGE){
                        $article = Article::where("category_id='{$id}'")->find();
                        if(!empty($article)){
                            ToolService::delFile($article['src']);
                            Article::destroy($article['id']);
                        }
                        ToolService::delFile($cate['src']);
                        CategoryModel::destroy($id);
                        $msg = ['code'=>1,'msg'=>'ok'];
                    }elseif($cate['type'] == CategoryModel::URL){
                        CategoryModel::destroy($id);
                        $msg = ['code'=>1,'msg'=>'ok'];
                    }else{
                        $msg = ['code'=>0,'msg'=>'fail'];
                    }
                }
            }
            return json($msg);
		}
        $this->error('无此操作');
    }

    //排序
    public function sort()
    {
        if(request()->isAjax()){
            $id = input('post.id/d');
            $sort = input('post.sort');
            $msg = ['code'=>0,'msg'=>'fail'];
            if($id <= 0 || !in_array($sort,array('up','down'))){
                return join(['code'=>0,'msg'=>'参数错误']);
            }else{
                $item = CategoryModel::get($id);
                if(empty($item)){
                    $msg = ['code'=>0,'msg'=>'无此分类'];
                }else{
                    if($sort == 'down'){
                        $prev = CategoryModel::where("parent_id='{$item['parent_id']}' and sort > {$item['sort']}")
                            ->order("sort asc")
                            ->limit(1)
                            ->find();
                        if(!empty($prev)){
                            CategoryModel::where("id='{$id}'")->update(['sort'=>$prev['sort']]);
                            CategoryModel::where("id='{$prev['id']}'")->update(['sort'=>$item['sort']]);
                        }else{
                            $msg = ['code'=>0,'msg'=>'无须调整排序'];
                        }
                    }elseif($sort == 'up'){
                        $next = CategoryModel::where("parent_id='{$item['parent_id']}' and sort < {$item['sort']}")
                            ->order("sort desc")
                            ->limit(1)
                            ->find();
                        if(!empty($next)){
                            CategoryModel::where("id='{$id}'")->update(['sort'=>$next['sort']]);
                            CategoryModel::where("id='{$next['id']}'")->update(['sort'=>$item['sort']]);
                        }else{
                            $msg = ['code'=>0,'msg'=>'无须调整排序'];
                        }
                    }
                    $msg = ['code'=>1,'msg'=>'ok'];
                }
            }
            return json($msg);
        }
        $this->error('无此操作');
    }

    //更新
    public function edit()
    {
        if(request()->isPost()){
            $item = [];
			$item['title'] = input('post.title');
            $item['type'] = input('post.type');
            $item['url'] = input('post.url');
            $item['style'] = input('post.style');
            $item['state'] = input('post.state');
            $item['description'] = input('post.description');
            $id = input('post.id');
			$img = input('post.img');

			if(!empty($img)){
				$item['src'] = $img;
			}
            CategoryModel::where(['id' => $id])->update($item);
			$this->redirect('Category/index');
		}else{
			$id = input('param.id');
			$item = CategoryModel::get($id);
			if(empty($item)){
				$this->redirect('Category/index');
			}
			$this->assign('item',$item);
			return view();
		}
    }

    //添加
    public function add()
    {
        if(request()->isPost()){
            $item = [];
			$item['title'] = input('post.title');
            $item['type'] = input('post.type');
            $item['url'] = input('post.url');
            $item['style'] = input('post.style');
            $item['state'] = input('post.state');
            $item['description'] = input('post.description');
            $item['parent_id'] = input('post.parent_id');
			$img = input('post.img');
			if(!empty($img)){
				$item['src'] = $img;
			}

            $category = new CategoryModel();
            $category->data($item)->save();
			$this->redirect('Category/index');
		}else{
			$parent_id = input('param.id')??input('param.parent_id');
            //dump($parent_id);//exit;
			$this->assign('parent_id',$parent_id);
            return view();
		}
    }
}
