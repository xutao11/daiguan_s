<?php

namespace app\admin\controller;

use \think\Controller;
use \app\admin\model\DataModel;
use \app\admin\model\AreasModel;
use think\Db;

class Data extends Controller
{
    //显示列表
    public function list1()
    {
        $date = Db::name('areas')->select();
        //客户申请表与用户表关联
        $items = Db::table('dx_data')
            ->alias('d')
            ->join('dx_user user','d.user_id = user.user_id')
            ->field('d.*,user.user_name')
            ->order('id desc')
           ->paginate(10);
           //->select();
    	/*//补充地址字段
    	foreach ($items as $k=>$v) {
            $sheng = Db::table('dx_areas')->find($v['province']);
            $shi = Db::table('dx_areas')->find($v['city']);
            $xian= Db::table('dx_areas')->find($v['county']);
            //组合
    		$items[$k]['dizhi']= $sheng['area_name'].$shi['area_name'].$xian['area_name'];

    	}*/




    	//分配是数据到模板
        $this->assign('page',$items->render());
        $this->assign("date",$date);
    	$this->assign('items',$items);
    	return view('list');
        
    }
    public function del(){
        $re=Db::table('dx_data')->where('id',$_POST['id'])->delete();
        if($re){
            echo '删除成功';
        }else{
            echo '删除失败';
        }

    }
    //评测结果回复
    public function reply(){


        $re  = Db::name('data')->where('id',$_POST['id'])->update(['reply'=>$_POST['reply']]);
       echo "$re";

    }


}


?>