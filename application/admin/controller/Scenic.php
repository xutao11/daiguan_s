<?php
namespace app\admin\controller;

use app\common\model\Scenic as ScenicModel;

class Scenic extends Base
{
    public function del()
    {
        if(request()->isAjax()){
			$id = input('post.id/d');
            $scenic = ScenicModel::get($id);
            $msg = ['code'=>0,'msg'=>'参数错误'];
            if(!empty($scenic)){
                ScenicModel::destroy($id);
                $msg = ['code'=>1,'msg'=>'ok'];
            }
            return json($msg);
		}
        $this->error('无此操作');
    }
    //编辑
    public function edit()
    {
        if(request()->isPost()){
            $item = [
                'title' => input('post.title'),
                'description' => input('post.description'),
                'lng' => input('post.lng'),
                'lat' => input('post.lat')
            ];
            $id = input('post.id/d');
            ScenicModel::where(['id' => $id])->update($item);
			$this->redirect('Scenic/index');
		}else{
            $id = input('param.id/d');
            $item = ScenicModel::get($id);
            $this->assign('item',$item);
            return view();
		}
    }
    //添加
    public function add()
    {
        if(request()->isPost()){
            $item = [
                'title' => input('post.title'),
                'description' => input('post.description'),
                'lng' => input('post.lng'),
                'lat' => input('post.lat')
            ];
            ScenicModel::create($item);
			$this->redirect('Scenic/index');
		}else{
            return view();
		}
    }
    //列表
    public function index()
    {
        $list = ScenicModel::order("id desc")
        ->paginate();
		$this->assign('items',$list);
		$this->assign('page',$list->render());
		return view();
    }
}
