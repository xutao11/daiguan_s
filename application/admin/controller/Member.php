<?php
namespace app\admin\controller;

use app\admin\model\Member as MemberModel;

class Member extends Base
{
    public function index()
    {
        $items = MemberModel::select();
        $this->assign('items',$items);
        return view();
    }
}
