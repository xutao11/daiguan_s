<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/13
 * Time: 15:09
 */


namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Validate;


//客户控制器、
class User extends Controller{

    //客户列表
    public function userlist(){
        $items = Db::table('dx_user')
            ->alias('u')
            ->join('dx_service s','u.ser_id = s.ser_id','LEFT')
            ->field('u.*,s.ser_name')
            ->paginate(10);
        $this->assign('page',$items->render());
        $this->assign('items',$items);
        return view('list');

    }
    //修改客户资料
    public function edit(){
        if(!empty($_POST)){

            $yanzheng = [
                'user_name'=>'require',
                'user_tel'=>'number|length:11',
                'user_dizhi'=>'require',


            ];
            $msg =[
                'user_name.require'=>'昵称名不能为空',
                'user_tel.number'=>'客户电话必须为数字',
                'user_tel.length'=>'客户电话必须为11数',
                'user_dizhi.require'=>'客户所在不能为空',


            ];

            $validate = new Validate($yanzheng,$msg);
            $date = [
                'user_name'=>$_POST['user_name'],
                'turename'=>$_POST['turename'],
                'user_sex'=>$_POST['user_sex'],
                'user_tel'=>$_POST['user_tel'],
                'ser_id'=>$_POST['ser_id'],
                'user_dizhi'=>$_POST['user_dizhi'],
                'user_birthday'=>$_POST['jHsDateInput'],
                'user_id'=>$_POST['user_id']
            ];

            $re = $validate->check($date);

            if(!$re){

                $this->error($validate->getError());
            }else{
                $re = Db::table('dx_user')->update($date);
                if($re){
                    $this->success('修改成功',url('admin/user/userlist'),2);
                }else{
                    $this->error('你没有修改',url('admin/user/userlist'),2);
                }
            }
        }else{
            $user_id = input('param.user_id');
            $data = Db::name('user')->where('user_id',"$user_id")->find();
            $ser = Db::name('service')->field('ser_id,ser_name')->select();
            $this->assign('ser',$ser);
            $this->assign('data',$data);
            return view('edit');
        }

    }
    //添加用户
    public function add(){
        //查询客服
        $ser = Db::name('service')->field('ser_id,ser_name')->select();
       $this->assign('ser',$ser);
        return view('tianjia');
    }
    //添加用户表单接受
    public function addform(){


        $yanzheng = [
            'user_name'=>'require',
            'user_tel'=>'number|length:11',
            'user_dizhi'=>'require',

        ];
        $msg =[
            'user_name.require'=>'客户名不能为空',
            'user_tel.number'=>'客户电话必须为数字',
            'user_tel.length'=>'客户电话必须为11数',
            'user_dizhi.require'=>'客户所在不能为空',
        ];
        $date = [
            'user_name'=>$_POST['user_name'],
            'turename'=>$_POST['turename'],
            'user_sex'=>$_POST['user_sex'],
            'user_tel'=>$_POST['user_tel'],
            'ser_id'=>$_POST['ser_id'],
            'user_dizhi'=>$_POST['user_dizhi'],
            'user_birthday'=>$_POST['jHsDateInput'],

        ];
        $validate = new Validate($yanzheng,$msg);
        $re = $validate->check($date);

        if(!$re){

            $this->error($validate->getError());
        }else{
            $re = Db::table('dx_user')->insert($date);
            if($re){
                $this->success('添加成功',url('admin/user/userlist'),2);
            }else{
                $this->error('添加失败，请稍后重试');
            }
        }
    }
    //删除客户
    public function del(){


        $del_user = Db::table('dx_user')->delete($_POST['userid']);
        $del_loan=Db::table('dx_loan')->where('user_id','=',$_POST['userid']);
        if($del_user){
            echo '1';
        }else{
            echo "0";
        }


    }

}