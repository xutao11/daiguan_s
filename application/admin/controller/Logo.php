<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017/8/21
 * Time: 16:52
 */
namespace app\admin\controller;

use think\Controller;
use app\common\model\Member;
use think\Db;

class Logo extends Controller{


    public function index(){
        $re = Db::name("logo")->where("id",'1')->find();
        $this->assign("item",$re);
        return view('show');
    }
    public function add(){
        $re = Db::name("logo")->where('id',"1")->find();
        $this->assign('item',$re);
        return view('add');

    }
    public function add_form(){


        $re = Db::name("logo")->where('id',"1")->update($_POST);
        if($re){
            $this->success('修改成功');
        }else{
            $this->error("修改失败");
        }
    }

}