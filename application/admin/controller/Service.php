<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/31
 * Time: 15:24
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;


//客服人员控制器
class Service extends Controller{
    //客服人员列表
    public function servicelist(){
        $datas =Db::name('service')->paginate(10);
        $this->assign('page',$datas->render());
        $this->assign('datas',$datas);
        return view('list');

    }
    //客服添加
    public function add(){
        if(request()->isPost()){
           $re  = Db::name('service')->data($_POST)->insert();
            if($re){
                $this->success('添加成功',Url('admin/service/servicelist'),2);
            }else{
                $this->error('添加失败');
            }

        }else{
            return view('add');
        }



    }
    //修改
    public function edit(){
        if(request()->isPost()){
            var_dump($_POST);
            Db::name("service")->update($_POST);
            $this->success('修改成功',Url('admin/service/servicelist'),2);

        }else{
            $ser_id = input('param.ser_id');
            $date = Db::name('service')->where('ser_id',$ser_id)->find();
            $this->assign('date',$date);
            return view('edit');

        }
    }
    //删除
    public function del(){
        if(isset($_POST)){
            echo $_POST['ser_id'];
            $re = Db::name('service')->where('ser_id',$_POST['ser_id'])->delete();
            if($re) {
                echo "删除成功";
            }else{
                echo '删除失败，请重试';
            }
        }else{
            $this->error('非法操作');
        }


    }
}