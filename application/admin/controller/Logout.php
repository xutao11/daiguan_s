<?php
namespace app\admin\controller;

class Logout extends Base
{
    public function index()
    {
        session(null);
        $this->redirect('Login/index');
    }
}
