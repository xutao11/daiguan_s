<?php
namespace app\admin\controller;

use think\Controller;
use app\common\model\Member;
use think\Db;


class Repayment extends Controller{

    public function addshow(){
        if(!empty($_POST)){

            //查询起始时间 拼凑首次还款日
            //获取起始时间拆分
            $re = Db::table('dx_loan')->where('loan_id',$_POST['loan_id'])->find();
            $start_date  = $re['start_date'];
            //起始时间拆分
            $ex_date = explode('-',$start_date);
            $day = str_pad( $re['repayment_date'],2,"0",STR_PAD_LEFT);
            //拼凑首次还款日
            $date_q = $ex_date['0']."-".$ex_date['1']."-".$day;
            $data = $ex_date['0']."-".$ex_date['1']."-".'01';
            $data = date("Y-m-d", strtotime("+1 months", strtotime($data)));
            //定义数据为数组
            $date =[];

            if($re['repayment_type'] == "等额本息"){
                $money = $_POST['repayment_money'];

                for($i=0;$i<$re['loan_all'];$i++){
                    $tmp_month  = date('Y-m-d',strtotime("+".$i." months",strtotime($data)));

                    $days = date('t',strtotime($tmp_month));
                    $date[$i]['loan_id'] = $re['loan_id'];
                    $date[$i]['repayment_money'] = $money;
                    $date[$i]['repayment_zhuangtai'] ="0";
                    $date[$i]['repayment_qishu'] = $i+1;
                    if($day >= $days){
                        $date[$i]['repayment_date'] = date('Y-m-d', strtotime("$tmp_month +1 month -1 day"));
                    }else{
                        $date[$i]['repayment_date'] = date("Y-m-d", strtotime("+".$i." months", strtotime($date_q)));
                    }
                }

            }else{
                foreach($_POST['repayment_money'] as $k=>$v){
                    $tmp_month  = date('Y-m-d',strtotime("+".$k." months",strtotime($data)));

                    $days = date('t',strtotime($tmp_month));
                    foreach($_POST['repayment_zhuangtai'] as $k1 =>$v1){
                        $date[$k]['repayment_money']=$v;
                        if(empty($date[$k]['repayment_money'])){
                            $this->error('每期还款金额不能为空');
                        }
                        $date[$k1]['repayment_zhuangtai']=$v1;
                        $date[$k]['repayment_qishu']=$k+1;
                        $date[$k]['loan_id']=$_POST['loan_id'];
                        //获取当月天数
                        if($day >= $days){
                            $date[$k]['repayment_date'] = date('Y-m-d', strtotime("$tmp_month +1 month -1 day"));
                        }else{
                            $date[$k]['repayment_date'] = date("Y-m-d", strtotime("+".$k." months", strtotime($date_q)));
                        }
                    }
                }
            }



           
            //组合数据写入数据库
            $arr=[];
            foreach ($date as $key=>$vul){
                $arr[$key]['loan_id']=$vul['loan_id'];
                $arr[$key]['repayment_qishu']=$vul['repayment_qishu'];
                $arr[$key]['repayment_money']=$vul['repayment_money'];
                $arr[$key]['repayment_zhuangtai']=$vul['repayment_zhuangtai'];
                $arr[$key]['moneths_date']=$vul['repayment_date'];
            }

            Db::table("dx_repayment")->where('loan_id',$_POST['loan_id'])->delete();

           $re =  Db::name('repayment')->insertAll($arr);
            $repayment_id =Db::name('repayment')->getLastInsID();
            $rer = Db::name('repayment')->where('repayment_id',$repayment_id)->find();
            $eee = Db::name('loan')->where('loan_id',$rer['loan_id'])->find();



            if($re){
                $r =<<<R
                    <script>alert('添加成功');</script>
R;

                echo $r;
                $this->assign('id',$eee['user_id']);
                return view('content/tiaozhuan');
              //  $this->success('添加分期成功',Url('admin/loan/loan_list'),2);
            }else{
                $this->error('添加分期失败');
            }

        }else{
            $user_id = input('param.user_id');
            $loan_id=input('param.loan_id');
            $usery = Db::table('dx_user')->where('user_id',$user_id)->find();
            $count = Db::name('repayment')->where('loan_id',$loan_id)->count();
            $loan = Db::name('loan')->where('loan_id',$loan_id)->find();

            if($count == $loan['loan_all']){
                $re = Db::name('repayment')->where('loan_id',$loan_id)->select();
                $this->assign('date',$re);
                $this->assign('re',$loan);
                $this->assign('user_name',$usery['user_name']);
                return view('addedit');

            }else{
                Db::name('repayment')->where('loan_id',$loan_id)->delete();
                $this->assign('re',$loan);

                $this->assign('user_name',$usery['turename']);

                return view('add');
            }

        }

    }
    public function showlist(){

        $user_id = input('param.user_id');
        $loan_id=input('param.loan_id');
        $usery = Db::table('dx_user')->where('user_id',$user_id)->find();
        $re = Db::table('dx_loan')->where('loan_id',$loan_id)->find();
        $date = Db::table('dx_repayment')->where('loan_id',$loan_id)->select();

        $this->assign('re',$re);
        $this->assign('user_name',$usery['turename']);
        $this->assign('date',$date);



        return view('list');
    }
    //edit
    public function edit()
    {
        if (!empty($_POST)) {

            if ($_POST['re_type'] == "等额本息") {
//                if(Db::name('repayment')->where('loan_id', $_POST['loan_id'])->count()){
//                    echo 'qq';
//                    $re = Db::name('repayment')->where('loan_id', $_POST['loan_id'])
//                        ->update(['repayment_money' => $_POST['repayment_money']]);
//                }else{
                   // var_dump($_POST);die;
                    $re = Db::table('dx_loan')->where('loan_id', $_POST['loan_id'])->find();
                    $start_date = $re['start_date'];
                    //起始时间拆分
                    $ex_date = explode('-', $start_date);
                    $day = str_pad($re['repayment_date'], 2, "0", STR_PAD_LEFT);
                    //拼凑首次还款日
                    $date_q = $ex_date['0'] . "-" . $ex_date['1'] . "-" . $day;
                    $data = $ex_date['0'] . "-" . $ex_date['1'] . "-" . '01';
                    $data = date("Y-m-d", strtotime("+1 months", strtotime($data)));
                    $date = [];
                    for($i=1;$i<=$re['loan_all'];$i++) {
                        $tmp_month = date('Y-m-d', strtotime("+" . $i. " months", strtotime($data)));

                        $days = date('t', strtotime($tmp_month));

                            $date[$i]['repayment_money'] = $_POST['repayment_money'];
                            if (empty($date[$i]['repayment_money'])) {
                                $this->error('每期还款金额不能为空');
                            }
                            $date[$i]['repayment_zhuangtai'] = '0';
                            $date[$i]['repayment_qishu'] = $i;
                            $date[$i]['loan_id'] = $_POST['loan_id'];
                            //获取当月天数
                            if ($day >= $days) {
                                $date[$i]['repayment_date'] = date('Y-m-d', strtotime("$tmp_month +1 month -1 day"));
                            } else {
                                $date[$i]['repayment_date'] = date("Y-m-d", strtotime("+" . $i . " months", strtotime($date_q)));
                            }


                    }
                    //组合数据写入数据库
                    $arr = [];
                    foreach ($date as $key => $vul) {
                        $arr[$key]['loan_id'] = $vul['loan_id'];
                        $arr[$key]['repayment_qishu'] = $vul['repayment_qishu'];
                        $arr[$key]['repayment_money'] = $vul['repayment_money'];
                        $arr[$key]['repayment_zhuangtai'] = $vul['repayment_zhuangtai'];
                        $arr[$key]['moneths_date'] = $vul['repayment_date'];
                    }
//                    var_dump($arr);
                    Db::table("dx_repayment")->where('loan_id', $_POST['loan_id'])->delete();
//
                    $re = Db::name('repayment')->insertAll($arr);
//                }




            } else {
                $re = Db::table('dx_loan')->where('loan_id', $_POST['loan_id'])->find();
                $start_date = $re['start_date'];
                //起始时间拆分
                $ex_date = explode('-', $start_date);
                $day = str_pad($re['repayment_date'], 2, "0", STR_PAD_LEFT);
                //拼凑首次还款日
                $date_q = $ex_date['0'] . "-" . $ex_date['1'] . "-" . $day;
                $data = $ex_date['0'] . "-" . $ex_date['1'] . "-" . '01';
                $data = date("Y-m-d", strtotime("+1 months", strtotime($data)));
                $date = [];
                foreach ($_POST['repayment_money'] as $k => $v) {
                    $tmp_month = date('Y-m-d', strtotime("+" . $k . " months", strtotime($data)));

                    $days = date('t', strtotime($tmp_month));
                    foreach ($_POST['repayment_zhuangtai'] as $k1 => $v1) {
                        $date[$k]['repayment_money'] = $v;
                        if (empty($date[$k]['repayment_money'])) {
                            $this->error('每期还款金额不能为空');
                        }
                        $date[$k1]['repayment_zhuangtai'] = $v1;
                        $date[$k]['repayment_qishu'] = $k + 1;
                        $date[$k]['loan_id'] = $_POST['loan_id'];
                        //获取当月天数
                        if ($day >= $days) {
                            $date[$k]['repayment_date'] = date('Y-m-d', strtotime("$tmp_month +1 month -1 day"));
                        } else {
                            $date[$k]['repayment_date'] = date("Y-m-d", strtotime("+" . $k . " months", strtotime($date_q)));
                        }
                    }

                }
                    //组合数据写入数据库
                    $arr = [];
                    foreach ($date as $key => $vul) {
                        $arr[$key]['loan_id'] = $vul['loan_id'];
                        $arr[$key]['repayment_qishu'] = $vul['repayment_qishu'];
                        $arr[$key]['repayment_money'] = $vul['repayment_money'];
                        $arr[$key]['repayment_zhuangtai'] = $vul['repayment_zhuangtai'];
                        $arr[$key]['moneths_date'] = $vul['repayment_date'];
                    }

                    Db::table("dx_repayment")->where('loan_id', $_POST['loan_id'])->delete();
//
                    $re = Db::name('repayment')->insertAll($arr);


                }
           
                $this->success('修改分期成功', Url('admin/loan/loan_list'), 2);

            }

    }
    //
    public function edit_form(){

        $user_id = input('param.user_id');
        $loan_id=input('param.loan_id');
        $usery = Db::table('dx_user')->where('user_id',$user_id)->find();
        $count = Db::name('repayment')->where('loan_id',$loan_id)->count();
        $loan = Db::name('loan')->where('loan_id',$loan_id)->find();
        $re = Db::name('repayment')->where('loan_id',$loan_id)->select();
          if(!$re['0']['repayment_money']){
            $re['0']['repayment_money'] = "";
        }

        $this->assign('money',$re['0']['repayment_money']);
        $this->assign('date',$re);
        $this->assign('re',$loan);
        $this->assign('user_name',$usery['turename']);
        return view('addedit');
    }

    //修改还款状态
    public function editzhuangtai(){


        $repaymebt_id = $_POST['repayment_id'];
                $r = Db::table('dx_repayment')->where('repayment_id',$repaymebt_id)->find();
        if($r['repayment_zhuangtai']==1){
            $repayment_zhuangtai = '0';
        }else{
            $repayment_zhuangtai = '1';
        }
        $re = Db::table('dx_repayment')->where('repayment_id',$repaymebt_id)->update(['repayment_zhuangtai'=>$repayment_zhuangtai]);
        if($re){
            echo $repayment_zhuangtai;
        }


    }
    //修改还款金额
    public function editmoney(){
            $repayment_id = $_POST['repayment_id'];
        $repayment_money = $_POST["money"];
        $re = Db::table('dx_repayment')->where('repayment_id',$repayment_id)->update([
            'repayment_money'=>$repayment_money,
        ]);
        if($re){
            echo '修改成功';
        }else{
            echo '修改失败';
        }


    }






}







?>