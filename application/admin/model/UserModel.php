<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/13
 * Time: 12:59
 */
 namespace app\admin\model;
 use think\model;
 class UserModel extends Model{
     //表名
     protected $table = 'dx_user';
     //根据openid查询用户信息
     static public function openid_get($openid){
        $re =  self::where('openid',$openid)->find();
         if($re){
             return $re;
         }else{
             return false;
         }
     }
     //根据手机号插叙用户信息
     static public function tel_get($tel){
         $re = self::where('user_tel',$tel)->find();
         if($re){
             return $re;
         }else{
             return false;
         }
     }
 }