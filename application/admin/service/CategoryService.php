<?php
namespace app\admin\service;

use think\Model;
use app\common\model\Category;

class CategoryService extends Model
{
    //获取顶级栏目
    static public function getTopCate()
    {
        return Category::where(['parent_id'=>0])->order(['sort'=>'asc','id'=>'asc'])->select();
    }

    //查看是否有下级栏目
    static public function hasChildren($category_id)
    {
        $count = Category::where(['parent_id'=>$category_id])->count();
        return $count?true:false;
    }

    //获取下级栏目
    static public function getChildren($category_id)
    {
        return Category::where(['parent_id'=>$category_id])->order(['sort'=>'asc','id'=>'asc'])->select();
	}
}
