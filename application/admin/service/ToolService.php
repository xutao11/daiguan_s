<?php
namespace app\admin\service;

use app\common\service\ImageService;

class ToolService
{
    //删除文件
    public static function delFile($path,$isImg = 0)
    {
        if(empty($path)){
            return null;
        }
        $root = ROOT_PATH . 'public';
        if($isImg){
            $thumb = ImageService::getThumb($path);
            $water = ImageService::get_roportion($path);
            if(!empty($thumb)){
                $thumbPath = $root . $thumb;
                $water = substr(ROOT_PATH, 0, -1).$water;
                if(file_exists($thumbPath)){
                    unlink($thumbPath);


                }

                    @unlink($water);

            }
        }
        $filePath = $root.$path;
        if(file_exists($filePath)){
            unlink($filePath);
        }
    }
}
