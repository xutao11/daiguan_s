<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    '/news/category/:category' => 'news/index',
    '/news/detail/:id' => 'news/detail',
    '/query' => 'query/index',
    '/query/second' => 'query/second',
    '/query/third' => 'query/third',
    '/area/[:parent_id]'  => 'area/index',
    '/personal/index'=>'personal/index',
  // '/shouye'=>'index/index',



    //后台
    '/loan/type' =>'loan/type',
];
