<?php
namespace app\common\model;

use think\Model;

class Link extends Model
{
    //模型事件注册
    protected static function init()
    {
        Link::event('after_insert',function($item){
            $item->where(['id' => $item->id])->update(['sort' => $item->id]);
        });
    }
}
