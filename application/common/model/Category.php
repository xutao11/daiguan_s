<?php
namespace app\common\model;

use think\Model;

class Category extends Model
{
    protected $autoWriteTimestamp = true;
    const ARTICLE = 1;
	const PAGE = 2;
	const IMAGE = 3;
	const DOWNLOAD = 4;
	const PHOTOS = 5;
	const VIDEO = 6;
	const URL = 7;
    const VOICE = 8;
	public static function getTypes()
    {
		return array(
			'1' => '文章列表',
			'2' => '单页',
			//'3' => '组图',
			//'4' => '下载',
			//'5' => '相册',
			'7' => '网址',
		);
	}

    //模型事件注册
    protected static function init()
    {
        Category::event('after_insert',function($item){
            $item->where(['id' => $item->id])->update(['sort' => $item->id]);
        });
    }
}
