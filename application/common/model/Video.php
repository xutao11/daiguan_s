<?php
namespace app\common\model;

use think\Model;

class Video extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

    //模型事件注册
    protected static function init()
    {
        Video::event('after_insert',function($item){
            $item->where(['id' => $item->id])->update(['sort' => $item->id]);
        });
    }
}
