<?php
namespace app\common\model;

use think\Model;

class Article extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    //模型事件注册
    protected static function init()
    {
        Article::event('after_insert',function($item){
            $item->where(['id' => $item->id])->update(['sort' => $item->id]);
        });
        Article::afterUpdate(function($item){
            $item->where(['id' => $item->id])->update(['update_time' => time()]);
        });
    }

    public static $tags = [
        '吃', '住', '行', '游', '购', '娱'
    ];
}
