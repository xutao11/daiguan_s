<?php
namespace app\common\model;

use think\Model;

class Slide extends Model
{
    //模型事件注册
    protected static function init()
    {
        Slide::event('after_insert',function($item){
            $item->where(['id' => $item->id])->update(['sort' => $item->id]);
        });
    }
}
