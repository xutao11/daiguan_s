<?php
/**
 * Created by PhpStorm.
 * User: 10838
 * Date: 2018/2/24
 * Time: 17:29
 */
namespace app\common\model;

use think\Model;

class UserModel extends Model{
    //表名
    protected $table = 'dx_user';
    //根据openid查询用户信息
    static public function openid_get($openid){
        $re =  self::where('openid',$openid)->find();
        if($re){
            return $re;
        }else{
            return false;
        }
    }
    //根据手机号插叙用户信息
    static public function tel_get($tel){
        $re = self::where('user_tel',$tel)->find();
        if($re){
            return $re;
        }else{
            return false;
        }
    }
}