<?php
namespace app\common\model;

use think\Model;

class System extends Model
{
    static public function getKey($key)
    {
        if(empty($key)){
            return '';
        }else{
            $item = self::find();
            return $item[$key]??'';
        }
    }
}
