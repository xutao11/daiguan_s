<?php
namespace app\common\service;

use think\Image;
use app\common\model\System;

class ImageService
{
    static public function getThumb($src,$width=0,$height=0,$type=Image::THUMB_CENTER)
    {
        if(empty($src)){
            return '';
        }
        $root = ROOT_PATH . DS .'wwwroot';
        $realPath = $root . $src;
		if(!file_exists($realPath)){
			return '';
		}
		if($width != 0 && $height != 0){
		    $w = $width;
            $h = $height;
        }else{
            $w = intval(System::getKey('thumb_w'));
            $h = intval(System::getKey('thumb_h'));
            if($w <= 0){
                $w = $width;
            }
            if($h <= 0){
                $h = $height;
            }
        }
		$info = pathinfo($src);
		$thumbName = $info['dirname'].'/'.$info['filename'].'_'.$w.'_'.$h.'.'.$info['extension'];
		$realThumbName = $root . $thumbName;
		if(!file_exists($realThumbName)){
			$image = Image::open($realPath);
			$image->thumb($w, $h,$type)->save($realThumbName);
		}
		
		return $thumbName;
    }
    static public function getcopy($src,$width="960",$height="600"){

        if(empty($src)){
            return '';
        }
        $root = ROOT_PATH . DS .'wwwroot';
        $realPath = $root . $src;
        if(!file_exists($realPath)){
            return '';
        }

        $info = pathinfo($src);
        $copyName = $info['dirname'].'/'.$info['filename'].'_'.$width.'_'.$height.'.'.$info['extension'];
        $realThumbName = $root . $copyName;
            if(!file_exists($realThumbName)){
                $image = Image::open($realPath);
                $image->crop($width, $height,320,106)->save($realThumbName);
            }

        return $copyName;


    }


    //按比例
   static public function get_roportion($src,$width='536',$height='274'){

        if(empty($src)){

            return "";

        }
        //拼凑完整路径
        $path = ROOT_PATH.'wwwroot'.$src;
        //判断图片是否存在
        /*if(!file_exists($path)){

            return "";
        }*/


        //获取文件的信息
        $info = pathinfo($src);
       $xinxi = getimagesize("$path");
        if($xinxi['0'] == $width and $xinxi['1'] == $height){
           return $src;
       }

       //图片长大于高
       if($xinxi['0'] > $xinxi['1']){
           $w = $width;
           //$h = ($xinxi['0'] * $height) / $width.'ee';
          $h =  ($width / $xinxi['0']) * $xinxi['1'];
           if($h > $height){
               $h=$height;
               $w = ($height / $xinxi['1']) * $xinxi['0'];
           }
       }else{
           $h = $height;
           $w =  ($height / $xinxi['1']) * $xinxi['0'];
           if($w > $width){
               $w=$width;
               $h = ($width / $xinxi['0']) * $xinxi['1'];
           }
       }


       $water = ROOT_PATH . DS .'wwwroot'.$info['dirname'].'/'.$info['filename'].'_'.$width.'_'.$height.'.'.$info['extension'];

       $th_path =  ROOT_PATH . DS .'wwwroot'.$info['dirname'].'/'.$info['filename'].'_'.$w.'_'.$h.'.'.$info['extension'];
       $image1 = \think\Image::open("$path");
       // 生成缩略图
       $image1->thumb($w, $h)->save($th_path);
        //合并（以水印的方式）
        $tu = \think\Image::open(ROOT_PATH.'wwwroot'.DS.'static'.DS.'image'.DS.'bg.jpg');

       $tu->water($th_path,\think\Image::WATER_CENTER)->save($water);
       unlink($th_path);
      return $info['dirname'].'/'.$info['filename'].'_'.$width.'_'.$height.'.'.$info['extension'];

    }
}
