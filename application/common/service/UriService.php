<?php
namespace app\common\service;

class UriService
{
    const ARTICLE = 1;
	const PAGE = 2;
	const IMAGE = 3;
	const DOWNLOAD = 4;
	const PHOTOS = 5;
	const VIDEO = 6;
	const URL = 7;
    const VOICE = 8;
    const WECHAT = 9;

	static public function getTypes()
    {
		return array(
			'1' => '文章列表',
			'2' => '单页',
			//'3' => '组图',
			//'4' => '下载',
			//'5' => '相册',
			'6' => '视频',
			'7' => '网址',
            '8' => '语音',
            '9' => '微信文章'
		);
	}
}
