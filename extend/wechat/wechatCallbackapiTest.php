<?php
namespace wechat;

use app\common\model\Data;
use think\Db;


require EXTEND_PATH.'wechat/REST.php';
class wechatCallbackapiTest
{

    public function valid()
    {
    
       $echoStr = $_GET["echostr"];

        //valid signature , option
        if($this->checkSignature()){
            ob_clean();
            echo $echoStr;

            exit;
        }
    }
    public function sendTextMsg($fromUsername,$toUsername,$contentStr)
    {
        $textTpl = "<xml>
                            <ToUserName><![CDATA[%s]]></ToUserName>
                            <FromUserName><![CDATA[%s]]></FromUserName>
                            <CreateTime>%s</CreateTime>
                            <MsgType><![CDATA[%s]]></MsgType>
                            <Content><![CDATA[%s]]></Content>
                            <FuncFlag>0</FuncFlag>
                            </xml>";
        $time = time();
        echo sprintf($textTpl, $fromUsername, $toUsername, $time, 'text', $contentStr);
    }
     //自动回复功能
    public function responseMsg()
    {
        //get post data, May be due to the different environments
        //获取POST数据，可能是由于不同的环境
        //$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
        $postStr = file_get_contents("php://input");

      

        //extract post data

        //提取后的数据

            if (!empty($postStr)){


                $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
                $fromUsername = $postObj->FromUserName;
                $toUsername = $postObj->ToUserName;
                $msgType = $postObj->MsgType;

                if($msgType == 'text'){
                    $keyword = trim($postObj->Content);
                    $frist_keyword = $keyword[0];
                    if(!empty($keyword)){
                        if($keyword == "1"){
                            $contentStr = "请输入T+手机号(例：T13000000000)";
                            $this->sendTextMsg($fromUsername,$toUsername,$contentStr);
                        }elseif ($keyword == "2") {
                            $contentStr = "咨询";
                            $this->sendTextMsg($fromUsername,$toUsername,$contentStr);
                        }elseif($keyword == '1000'){
                            $contentStr = $toUsername.'-'.$fromUsername;
                            $this->sendTextMsg($fromUsername,$toUsername,$contentStr);
                        }elseif ($frist_keyword == "T") {
                            //获取手机号并判断是否合法
                            $tel = substr($keyword, 1);
                            $tel_v = '/^1[34578]\d{9}$/';
                            if(strlen($tel)!=11){
                                $contentStr='手机号为11位';
                            }else{
                                preg_match($tel_v, $tel, $new_tel);
                                //手机格式输入正确将验证码写入文件
                                if($new_tel){
                                    $charset = '123456789';//随机因子
                                    $code = $this->createCode($charset,4);
                                    //发送验证码
                                    $u = $this->sendTemplateSMS($tel ,array($code,'5'),"194612");
                                    //将电话号码
                                    $date = [
                                        "openid"=>"$fromUsername",
                                        'tel'=>"$tel",
                                        'code'=>"$code",
                                    ];
                                    if(Db::name("wechat")->where('openid',"$fromUsername")->find()){
                                        Db::name("wechat")->where("openid","$fromUsername")->delete();
                                    }

                                    $re = Db::name('wechat')->data($date)->insert();
                                    $contentStr =$u;

                                }else{
                                    $contentStr = '你的手机号格式错误';
                                }
                                # code...
                            }
                            $this->sendTextMsg($fromUsername,$toUsername,$contentStr);

                        }elseif ($frist_keyword == "V") {
                            //判断是否有验证码文件存在
                            $d = Db::name('wechat')->where('openid',"$fromUsername")->find();
                            if($d){
                                //获取用户输入的验证码
                                $code = substr($keyword, 1);
                                if($d['code'] == $code){
                                    //获取用户信息
                                    $re = $this->user($fromUsername);
                                    $data=[
                                        'user_tel' =>$d['tel'],
                                        'user_name'=>$re['nickname'],
                                        'user_dizhi'=>$re['province'].$re['city'],
                                        'user_sex' => $re['sex'],
                                        'openid' => $re['openid']
                                    ];
                                    $r = Db::name("user")->where('openid',"$fromUsername")->find();

                                    if($r){
                                        $t =  Db::name('user')->where('openid',"$fromUsername")->data($data)->update();
                                        $contentStr = "你已绑定成功";
                                    }else{
                                        $t =  Db::name('user')->where('user_tel',"{$d['tel']}")->data($data)->update();
                                        $contentStr = "你已绑定成功";
                                        if($t == ''){
                                            $data['register_time']=date('Y-m-d H:i:s');
                                            $data['edit_time']=time();
                                            Db::name('user')->insert($data);
                                            $contentStr = "注册成功，请到个人中心修改密码";
                                        }
                                    }




                                    Db::name('wechat')->where('openid',"$fromUsername")->delete();

                                }else{
                                    $contentStr = "验证码错误";
                                }

                            }else{
                                $contentStr = "请输入T+手机号获取验证码";
                            }
                            $this->sendTextMsg($fromUsername,$toUsername,$contentStr);
                        }else{
                            $contentStr = "感谢关注贷管网，贷管网是基于大数据+人工智能的数据驱动金融云模式，为需要融资的客户提供更专业全面的金融服务。更多服务内容可致电18280200180。\n回复数字:\n1.绑定手机或注册账号";
                            $this->sendTextMsg($fromUsername,$toUsername,$contentStr);
                        }
                    }
                }elseif($msgType == 'event'){
                    $event = $postObj->Event;
                    if($event == 'subscribe'){
                        $contentStr = "感谢关注贷管网，贷管网是基于大数据+人工智能的数据驱动金融云模式，为需要融资的客户提供更专业全面的金融服务。更多服务内容可致电18280200180。\n回复数字:\n1.绑定手机或注册账号";
                        $this->sendTextMsg($fromUsername,$toUsername,$contentStr);
                    }

                }

            }else {
                echo "";
                exit;
            }




    }

    private function checkSignature()
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];    
                
        $token = "ddd";
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr,SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );
        
        if( $tmpStr == $signature ){
            return true;
        }else{
            return false;
        }
    }
    //获取access_token
    public function get_token(){
    
//        $appid = 'wx902deaa1c9227d1f';
//        $secret = '81d1d5e2121941e6c0b035fcf7ce5666';
        $appid = 'wx703acc3701dcc383';
        $secret = '3ee8c3cbd346de5f88c27607a05c0108';
    $get_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$secret}";
  
  $date = file_get_contents($get_url);


   $date = json_decode($date,true);

   return $date['access_token'];

    

    }
    //获取用户的基本信息
    public function user($openid){
       
        $access_token = $this->get_token();
        
        
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={$access_token}&openid={$openid}&lang=zh_CN";
        $re = file_get_contents($url);
        $re = json_decode($re,true);
        return $re;
    }


    //短信验证
   public function sendTemplateSMS($to,$datas,$tempId)
    {

            //主帐号,对应开官网发者主账号下的 ACCOUNT SID
            $accountSid= 'aaf98f89486445e601487dcf498108ad';

            //主帐号令牌,对应官网开发者主账号下的 AUTH TOKEN
            $accountToken= 'ccaa16808d704145bb20c418bf9c7377';

            //应用Id，在官网应用列表中点击应用，对应应用详情中的APP ID
            //在开发调试的时候，可以使用官网自动为您分配的测试Demo的APP ID
            $appId='8aaf07085d106c7f015d4027e1c2147c';

            //请求地址
            //沙盒环境（用于应用开发调试）：sandboxapp.cloopen.com
            //生产环境（用户应用上线使用）：app.cloopen.com
            $serverIP='app.cloopen.com';


            //请求端口，生产环境和沙盒环境一致
            $serverPort='8883';

            //REST版本号，在官网文档REST介绍中获得。
            $softVersion='2013-12-26';
         // 初始化REST SDK
         //global $accountSid,$accountToken,$appId,$serverIP,$serverPort,$softVersion;
     
         $rest = new \REST($serverIP,$serverPort,$softVersion);
       
         $rest->setAccount($accountSid,$accountToken);

         $rest->setAppId($appId);

        
         // 发送模板短信
//         echo "Sending TemplateSMS to $to <br/>";
         $result = $rest->sendTemplateSMS($to,$datas,$tempId);

         if($result == NULL ) {
             return "result error!";
            
         }
         if($result->statusCode!=0) {
            return "error code :" . $result->statusCode . "<br>"."error msg :" . $result->statusMsg;

             //echo "error code :" . $result->statusCode . "<br>";
             //echo "error msg :" . $result->statusMsg . "<br>";
             //TODO 添加错误处理逻辑
         }else{
             return "请输入验证码V+验证码(如：验证码为1234则输入V1234)";
             // 获取返回信息
            // $smsmessage = $result->TemplateSMS;
             //echo "dateCreated:".$smsmessage->dateCreated."<br/>";
             //echo "smsMessageSid:".$smsmessage->smsMessageSid."<br/>";
             //TODO 添加成功处理逻辑
         }
    }
    //生成随机验证码
    public function createCode($charset,$codelen) {
        $_len = strlen($charset)-1;
         $code='';
        for ($i=0;$i<$codelen;$i++) {
            $code .= $charset[mt_rand(0,$_len)];
        }
         return $code;
    }

}