<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:91:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/category/add.html";i:1505095441;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                <?php
use app\common\model\Category;
$parent = $parent_id?Category::get($parent_id):null;
?>

<div class="king-content-wrap">
    <div class="king-layout1-content" style="margin-top: 0;margin-left:0;">
    <ol class="breadcrumb">
	    <li><span>栏目管理</span></li>
	    <li><a href="<?php echo Url('Category/add'); ?>">栏目添加</a></li>
	</ol>
	<div class="panel panel-default m20">
        <div class="panel-body">
			<form action="<?php echo Url('Category/add'); ?>" method="post" id="form" class="box_left top30">
                <div class="form-group top20">
                    <label class="label_name">上级栏目</label>
                    <?php $parent_title = empty($parent)?'顶级栏目':$parent['title'];?>
                    <div class="add_name"><?php echo $parent_title; ?></div>
                </div>
				<div class="form-group">
					<label class="label_name">栏目名称</label>
					<input class="form-control" type="text" name="title" placeholder="填写栏目名称" />
				</div>
                <div class="form-group">
                    <label class="label_name">栏目分类</label>
                    <select class="form-control" name="type">
                        <?php foreach(Category::getTypes() as $k=>$v):?>
                        <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
                        <?php endforeach;?>
                    </select>
                </div>

				<div class="form-group">
					<label class="label_name">栏目链接</label>
					<input class="form-control" type="text" name="url" placeholder="栏目外链地址，没有则不填写" />
				</div>

				<div class="form-group">
					<label class="label_name">封面图<span style="color: red;font-size: 13px;">(图片大小:536x274)</span></label>
					<?php echo widget('Upload/image'); ?>
				</div>
				<div class="form-group">
					<label class="label_name">栏目打开方式</label>
					<select class="form-control" name="style">
						<option value="_self">当前页面</option>
						<option value="_blank">新页面</option>
					</select>
				</div>
				<div class="form-group">
					<label class="label_name">是否显示</label>
					<div class="checkbox ">
						<input type="radio" checked="checked" name="state" value="1" />是
						<input type="radio" name="state" value="0" />否
					</div>
				</div>

				<div class="form-group">
					<label class="label_name">栏目描述</label>
					<input class="form-control" type="text" name="description" placeholder="栏目描述" />
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="确认" />
				</div>
				<input type="hidden" name="parent_id" value="<?php echo isset($parent_id) ? $parent_id : 0; ?>" />
			</form>
		</div>
	</div>
</div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
