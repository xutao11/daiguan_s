<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/credit/credit.html";i:1505095441;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                

<div class="king-content-wrap">
	<div class="king-layout1-content" style="margin-top: 0; margin-left: 0px;">
		<ol class="breadcrumb">
			<li><span>信用管理</span></li>
			<li><span><?php echo $user_name; ?></span></li>

		</ol>
		<div class="panel panel-default m20" style="width:30%;">
			<form method="post" action="<?php echo Url('admin/credit/getfrom'); ?>">
				<h3>一、基本情况</h3>
				<div class="label_list top20">
					<label class="label_name">征信情况</label>
					<input class="form-control" type="text" name="credit" value="<?php echo $data['credit']; ?>"/>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">收入情况</label>
					<input class="form-control" type="text" name="income" value="<?php echo $data['income']; ?>"/>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">负债情况</label>
					<input class="form-control"  type="text" name="liabilities" value="<?php echo $data['liabilities']; ?>"/>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">理财情况</label><br>
					<input class="form-control" type="text" name="money" value="<?php echo $data['money']; ?>"/>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">其它</label>
					<input class="form-control"  type="text" name="other_basic" value="<?php echo $data['other_basic']; ?>"/>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<h3>二、推荐方案</h3>
				<div class="label_list top20">
					<label class="label_name">工作方案</label>
					<textarea class="form-control" name="work_program"><?php echo $data['work_program']; ?></textarea>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">信用卡使用方案</label>
					<textarea class="form-control" name="card_program"><?php echo $data['card_program']; ?></textarea>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">个人流水方案</label>
					<textarea class="form-control" name="water_program"><?php echo $data['water_program']; ?></textarea>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">支付宝使用方案</label><br>
					<textarea class="form-control" name="alipay_program"><?php echo $data['alipay_program']; ?></textarea>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">征信方案</label>
					<textarea class="form-control" name="credit_program"><?php echo $data['credit_program']; ?></textarea>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">理财方案</label>
					<textarea class="form-control" name="money_program"><?php echo $data['money_program']; ?></textarea>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">后期融资方案</label>
					<textarea class="form-control" name="financing_program"><?php echo $data['financing_program']; ?></textarea>
					<span style="font-size: 12px;color: red">

					</span>
				</div>
				<div class="label_list top20">
					<label class="label_name">其它</label>
					<textarea class="form-control" name="other_program"><?php echo $data['other_program']; ?></textarea>
					<span style="font-size: 12px;color: red">

					</span>
				</div>







				<div class="label_list top40">


					<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

					<input class="btn btn-success" type="button" value="保存" onclick="this.form.submit()">
				</div>


			</form>


			</div>
		</div>
	</div>
</div>



            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
