<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:90:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/login/index.html";i:1505095441;}*/ ?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="UTF-8">
	<title>大向天诚-管理后台登录页面</title>
    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/login.css" />
</head>

<body>
<div class="container">

	<form class="form-signin" role="form" action="<?php echo Url('Login/index'); ?>" method="post" style="text-align: center;">
        <img class='logos' alt='大向天诚' src="__STATIC__/manage/image/logo.jpg" />
		<input type="text" name="username" class="form-control" placeholder="用户名" required autofocus />
		<input type="password" name="password" class="form-control" placeholder="密码" required />
		<button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
	</form>
</div> <!-- /container -->

<script type="text/javascript" src="__MANAGE__/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
