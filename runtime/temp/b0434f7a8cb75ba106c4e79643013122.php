<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:89:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/link/index.html";i:1505095441;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                
<div class="king-content-wrap">
    <div class="king-layout1-content" style="margin-top: 0;margin-left: 0;">
        <ol class="breadcrumb">
            <li><span>友情链接</span></li>
            <li><span>列表</span></li>
        </ol>
        <div class="clearfix m20">
            <a class="btn btn-success" href="<?php echo Url('Link/add'); ?>">添 加</a>
        </div>
        <div class="panel panel-default m20">
            <div class="panel-body">
                <table>
                    <tr>
                        <td>封面图</td>
                        <td>标题</td>
                        <td>链接地址</td>
                        <td>排序</td>
                        <td>操作</td>
                    </tr>
                    <?php if(is_array($items) || $items instanceof \think\Collection || $items instanceof \think\Paginator): if( count($items)==0 ) : echo "" ;else: foreach($items as $key=>$item): ?>
                        <tr>
                            <td>
                                <?php if(empty($item['src']) || (($item['src'] instanceof \think\Collection || $item['src'] instanceof \think\Paginator ) && $item['src']->isEmpty())): ?>
                                &nbsp;
                                <?php else: ?>
                                <img src="<?php echo $item['src']; ?>" width="100" />
                                <?php endif; ?>
                            </td>
                            <td><?php echo $item['title']; ?>&nbsp;</td>
                            <td><?php echo $item['url']; ?>&nbsp;</td>
                            <td><?php echo $item['sort']; ?>&nbsp;</td>
                            <td>
                                <a href="<?php echo Url('Link/edit',['id' => $item['id']]); ?>">编辑</a>
                                <a class="del" data-url="<?php echo Url('Link/del'); ?>" data-id="<?php echo $item['id']; ?>" href="javascript:void(0);">删除</a>
                                <a class="sort" data-url="<?php echo Url('Link/sort'); ?>" data-id="<?php echo $item['id']; ?>" data-sort="up" href="javascript:void(0)">向上</a>
                                <a class="sort" data-url="<?php echo Url('Link/sort'); ?>" data-id="<?php echo $item['id']; ?>" data-sort="down" href="javascript:void(0)">向下</a>
                            </td>
                        </tr>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </table>
            </div>
        </div>
	</div>
</div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
