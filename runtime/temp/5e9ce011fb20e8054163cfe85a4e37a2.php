<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:93:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/repayment/list.html";i:1505095441;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                

<style>
	.re{
		color: red;
	}
</style>
<div class="king-content-wrap">
	<div class="king-layout1-content" style="margin-top: 0; margin-left: 0px;">
		<ol class="breadcrumb">
			<li><span>添加分期详情</span></li>
			<li><span><?php echo $user_name; ?></span></li>
			<li><span><?php echo $re['loan_type']; ?></span></li>
			<li><span><?php echo $re['loan_pingtai']; ?></span></li>
			<li><span><?php echo $re['repayment_type']; ?></span></li>

		</ol>
		<div class="panel panel-default m20" style="width:30%;">
			<div class="panel-body">
			<table style="width: 100%" class="table table-striped">
				<tr>
					<td>期数</td>
					<td>还款金额（元）</td>
					<td>还款状态</td>
					<td>操作</td>
				</tr>
				<?php foreach($date as $vo): ?>


				<tr>
					<td><?php echo $vo['repayment_qishu']; ?></td>
					<td><?php echo $vo['repayment_money']; ?>元</td>
					<td id ="zhuangitai<?php echo $vo['repayment_id']; ?>">
						<?php if($vo['repayment_zhuangtai'] == 1): ?>
						<span style="color: #00a0e9">已还</span>
						<?php else: ?>
						<span style="color: red">未还</span>
						<?php endif; ?>

					</td>
					<td>
						<a href="#" class="zhuangtai" repaymentid="<?php echo $vo['repayment_id']; ?>" zhuangtai="<?php echo $vo['repayment_zhuangtai']; ?>">点击改变状态 </a>
						<a href="#" class="money" repaymentid="<?php echo $vo['repayment_id']; ?>">修改金额</a>
					</td>
				</tr>
				<?php endforeach; ?>

			</table>



			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/static/layer/layer.js"></script>
<script>
	$('.zhuangtai').click(function(){
		var repaymentid = $(this).attr('repaymentid');

		$.post('<?php echo Url("admin/repayment/editzhuangtai"); ?>',{repayment_id:repaymentid},function(data){

			if(data == '1'){

				$('#zhuangitai'+repaymentid).html('<span style="color: #00a0e9">已还</span>');
			}else {

				$('#zhuangitai'+repaymentid).html('<span style="color: red">未还</span>');
			}

		});


	});
	 $('.money').click(function(){
		 var repayment_id  = $(this).attr('repaymentid');
		 layer.prompt({title: '输入金额（单位:元）', formType: 3}, function(text, index){

			 if(!isNaN(text)){
				 $.post('<?php echo Url("admin/repayment/editmoney"); ?>',{repayment_id:repayment_id,money:text},function(data){

					alert(data);
					 window.location.reload();
				 });
			 }else{
				 layer.alert("输入错误！");
			 }

		 });
	 });
</script>





            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
