<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:67:"D:\Code\daiguan1\wwwroot/../application/admin\view\widget\menu.html";i:1519610884;}*/ ?>
<?php
use App\Common\model\Category;
use app\admin\service\CategoryService;
use app\admin\service\UriService;
?>
<div class="king-layout1-sidebar" style=" height: 100%">
    <div class="" style="height: 100%; position: relative;">
        <div class="m0 p0">
            <nav style="height:100%;">
                <div class="king-vertical-nav4 auto-height">
                    <div class="sidebar-inner">
                        <div class="list-group  side-nav">
                            <?php if(!empty($menus)):?>
                                <div class="list-group ">
                                    <?php foreach($menus as $item):$children = Category::where(['parent_id'=>$item['id']])->where('type','<>',Category::URL)->order(['sort'=>'asc','id'=>'asc'])->select();if(count($children)):?>
                                            <span class="list-group-item"><?php echo $item['title']; ?></span>
                                            <?php foreach($children as $child):?>
                                            <a class="list-group-items <?php echo !empty($category_id) && $category_id==$child['id']?'actives':''; ?>" href="<?php echo UriService::getCategoryUri($child['id']);?>"><?php echo $child['title']; ?></a>
                                                    <?php $child_son = Category::where(['parent_id'=>$child['id']])->where('type','<>',Category::URL)->order(['sort'=>'asc','id'=>'asc'])->select();if(count($child_son)):foreach($child_son as $v):?>
                                                                 <a class="list-group-items <?php echo !empty($category_id) && $category_id==$v['id']?'actives':''; ?>" href="<?php echo UriService::getCategoryUri($v['id']);?>">=<?php echo $v['title']; ?></a>
                                                            <?php endforeach;endif;endforeach;else:?>
                                        <a class="<?php echo !empty($category_id) && $category_id==$item['id']?'actives':''; ?> list-group-item" href="<?php echo UriService::getCategoryUri($item['id']);?>"><?php echo $item['title']; ?></a>
                                        <?php endif;endforeach;?>
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
