<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:94:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/content/article.html";i:1505095441;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                <?php
use app\common\model\Category;
?>

<?php echo widget('Menu/index',['category_id'=>$category_id]); ?>
<div class="king-content-wrap">
    <div class="king-layout1-content" style="margin-top: 0;">
		<ol class="breadcrumb">
		    <li><span>内容管理</span></li>
		    <li><span><?php echo Category::get($category_id)['title']; ?></span></li>
		</ol>
	    <div class="clearfix m20">
		    <a class="btn btn-success pull-left" href="<?php echo Url('Article/add',['category_id'=>$category_id]); ?>">添加内容</a>
	        <form class="form-inline pull-right" action="<?php echo Url('Content/Article?category_id='.$category_id); ?>" method="get">
	            <input type="text" name="keyword" value="<?php echo isset($keyword) ? $keyword : ''; ?>" class="form-control" />
	            <input type="submit" class="btn btn-default" value="搜索" />
	        </form>
		</div>
		<div class="panel panel-default m20" style="margin-top: 20px;">
			<table class="table table-bordered" id="check_box">
				<tr class="table_title">
					<td>标题</td>
					<td>修改日期</td>
					<td>创建日期</td>
					<td>操作</td>
				</tr>
				    <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$item): ?>
					<tr class="select_box">
						<td><a href="<?php echo Url('Article/edit',array('id'=>$item['id'])); ?>"><?php echo $item['title']; ?></a></td>
						<td><?php echo $item['update_time']; ?></td>
						<td><?php echo $item['create_time']; ?></td>
						<td >
							<div>
								<a class="recommend" href="javascript:void(0);" data-id="<?php echo $item['id']; ?>" data-url="<?php echo Url('Article/recommend'); ?>" data-param="top"><?php echo $item['top']?'取消置顶':'置顶';?></a>
								<a class="recommend tuijian" href="javascript:void(0);" data-id="<?php echo $item['id']; ?>" data-url="<?php echo Url('Article/recommend'); ?>" data-param="recommend"><?php echo $item['recommend']?'<span style="color:red;">取消推荐</span>':'推荐';?></a>
								<a class="recommend" href="javascript:void(0);" data-id="<?php echo $item['id']; ?>" data-url="<?php echo Url('Article/recommend'); ?>" data-param="hot"><?php echo $item['hot']?'取消热门':'热门';?></a>
                                <br />
	                            <a href="<?php echo Url('Article/edit',array('id'=>$item['id'])); ?>">编辑</a>
	                            <a class="sort" data-url="<?php echo Url('Article/sort'); ?>" data-sort="up" data-id="<?php echo $item['id']; ?>" href="javascript:void(0)">向上</a>
	                            <a class="sort" data-url="<?php echo Url('Article/sort'); ?>" data-sort="down" data-id="<?php echo $item['id']; ?>" href="javascript:void(0)">向下</a>
	                            <a class="del" data-url="<?php echo Url('Article/del'); ?>" data-id="<?php echo $item['id']; ?>" href="javascript:void(0);">删除</a>
							</div>
						</td>
					</tr>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
				</table>
				<div class="page top30"><?php echo $page; ?></div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.tuijian').click(function(){

		var val = $(this).html();

		if(val == '推荐'){
			$(this).css('color','red');
		}else {
			$(this).css('color','#337ab7');
		}
	})
</script>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
