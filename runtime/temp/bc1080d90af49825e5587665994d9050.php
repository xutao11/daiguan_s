<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:65:"D:\Code\daiguan1\wwwroot/../application/admin\view\user\list.html";i:1519610884;s:69:"D:\Code\daiguan1\wwwroot/../application/admin\view\layout\layout.html";i:1519610883;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="四川众银联行信息科技有限公司" />
	<meta name="description" content="四川众银联行信息科技有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                
<div class="king-content-wrap" style="margin-top: 0px">
    <div class="king-layout1-content" style="margin-top: 0; margin-left: 0px">
        <ol class="breadcrumb">
        
            <li><span>客户列表</span></li>
        </ol>
        <div class="clearfix m20">
            <a class="btn btn-success" href="<?php echo Url('Admin/User/add'); ?>">添 加</a>
        </div>


        <div class="panel panel-default m20">
            <div class="panel-body">
                <div style="float: right;width:460px;margin-right: 128px;">
                    <input placeholder="请输入手机号或客户姓名" style="float: left;width: 400px" oninput="ee()" id="cx" class="form-control" type="text" name="title" />
                    <input type="button" id="chaxun" value="查询" class="btn btn-success" style="float: right">
                </div>
                <table style="text-align: center;margin-top: 49px;">
                    <tr style="background-color: rgba(25,25,25,0.19);font-size: 17px">

                        <td style="text-align: center">昵称</td>
                        <td style="text-align: center">真实姓名</td>
                        <td style="text-align: center">出生日期</td>
                        <td style="text-align: center">性别</td>
                        <td style="text-align: center">所在地</td>
                        <td style="text-align: center">手机</td>
                        <td style="text-align: center">客服人员</td>
                        <td style="text-align: center">注册时间</td>

                        <td style="text-align: center">操作</td>
                    </tr>
                    <?php if(is_array($items) || $items instanceof \think\Collection || $items instanceof \think\Paginator): if( count($items)==0 ) : echo "" ;else: foreach($items as $key=>$item): ?>
                        <tr>


                            <td><?php echo $item['user_name']; ?>
                                &nbsp;</td>
                            <td><?php echo $item['turename']; ?>
                                &nbsp;</td>
                            <td><?php echo $item['user_birthday']; ?>
                                &nbsp;</td>

                            <td>
                                <?php if(($item['user_sex']==1)): ?>
                                男
                                <?php else: ?>
                                女
                                <?php endif; ?>
                            </td>
                            <td><?php echo $item['user_dizhi']; ?></td>
                            <td>

                                <?php echo $item['user_tel']; ?>
                            &nbsp;</td>
                            <td>

                                <?php echo $item['ser_name']; ?>
                                &nbsp;</td>
                            <td>

                                <?php echo $item['register_time']; ?>

                            </td>
                            <td>

                                <a href="<?php echo Url('admin/user/edit',['user_id'=>$item['user_id']]); ?>">修改资料</a>
                                <a class="de" href="<?php echo Url('admin/loan/add',['user_id'=>$item['user_id']]); ?>">添加贷款</a>
                                <a href="<?php echo Url('admin/loan/loan_list',['user_id'=>$item['user_id']]); ?>">查看贷款</a>
                                <a href="<?php echo Url('admin/credit/showfrom',['user_id'=>$item['user_id']]); ?>">信用管理</a>
                                &nbsp;<a href="#" class="del1" userid="<?php echo $item['user_id']; ?>">删除用户</a>

                            </td>
                        </tr>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </table>
                <?php echo $page; ?>
            </div>
        </div>
    </div>
</div>
<script src="/static/layer/layer.js"></script>
<script>


        $('.del1').click(function () {
            var id = $(this).attr('userid');
            $.post("<?php echo Url('admin/user/del'); ?>",{userid:id},function (data) {
                if(data){
                    alert('用户删除成功');
                }else {
                    alert('用户删除失败');
                }
                window.location.reload();

            });
        });
    $('#chaxun').click(function(){
        var data = $('#cx').val();

        if(data == ''){
            alert('请输入手机号或姓名');
        }else {
            $.post("<?php echo Url('admin/user/chaxun'); ?>",{data:data},function(data){
                if(data == '没有找到该用户'){
                    alert(data);
                }else {
                    /*alert(data);*/
                    var e = data;


                   window.location.href = "<?php echo Url('/admin/user/f/id/"+e+"'); ?>";
                }
            });
        }
    });


</script>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
