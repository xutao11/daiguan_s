<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:88:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/loan/list.html";i:1505102773;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                
<div class="king-content-wrap" style="margin-top: 0px">
    <div class="king-layout1-content" style="margin-top: 0; margin-left: 0px">
        <ol class="breadcrumb">
        
            <li><span>客户贷款列表</span></li>
        </ol>
        <div class="panel panel-default m20">
            <div class="panel-body">
                <table class="table table-hover">
                    <tr style="background-color: rgba(25,25,25,0.19)">
                        <td style="font-size: 14px">真实姓名</td>

                        <td>性别</td>
                        <td>所在地</td>
                        <td>手机</td>
                        <td>贷款种类</td>
                        <td>贷款平台</td>
                        <td>贷款金额</td>
                        <td>还款方式</td>
                        <td>还款日</td>
                        <td>分期数</td>
                        <td>已还期数</td>
                        <td>起始时间</td>
                        <td>状态</td>
                        <td>还款银行</td>
                        <td>还款卡号</td>
                        <td>操作</td>

                    </tr>
                    <?php if(is_array($items) || $items instanceof \think\Collection || $items instanceof \think\Paginator): if( count($items)==0 ) : echo "" ;else: foreach($items as $key=>$item): ?>
                        <tr>

                            <td><?php echo $item['turename']; ?>
                            &nbsp;</td>
                            <td>
                                <?php if($item['user_sex'] == 1): ?> 男

                                <?php else: ?> 女
                                <?php endif; ?>


                                &nbsp;</td>
                            <td><?php echo $item['user_dizhi']; ?></td>
                            <td>
                                <?php echo $item['user_tel']; ?>
                            &nbsp;</td>
                            <td>
                                <?php echo $item['loan_type']; ?>
                            </td>
                            <td>
                                <?php echo $item['loan_pingtai']; ?>

                            &nbsp;</td>
                            <td>
                                <?php echo $item['loan_money']; ?>元
                                
                            &nbsp;</td>
                            <td>
                                <?php echo $item['repayment_type']; ?>

                                &nbsp;</td>
                            <td>
                                <?php if($item['repayment_date'] == ''): else: ?>
                                <?php echo $item['repayment_date']; ?>日
                                <?php endif; ?>


                                &nbsp;</td>
                            <td>
                                <?php echo $item['loan_all']; ?>个月
                            &nbsp;</td>
                            <td>
                                <?php repayment_yihuan($item['loan_id']); ?>个月
                            &nbsp;</td>
                             <td>
                                <?php echo $item['start_date']; ?>
                            &nbsp;</td>
                            <td style="color: red">
                                <?php $_59b60bb8458fd=config('data.loan_state'); if(is_array($_59b60bb8458fd) || $_59b60bb8458fd instanceof \think\Collection || $_59b60bb8458fd instanceof \think\Paginator): if( count($_59b60bb8458fd)==0 ) : echo "" ;else: foreach($_59b60bb8458fd as $k=>$vo): if($item['loan_state'] == $k): ?>
                                        <?php echo $vo; endif; endforeach; endif; else: echo "" ;endif; ?>

                            </td>
                            <td><?php echo $item['bank']; ?></td>
                            <td><?php echo $item['bank_card']; ?></td>
                            <td>
                                <a href="<?php echo Url('admin/loan/edit',['user_id'=>$item['user_id'],'loan_id'=>$item['loan_id']]); ?>">修改</a>
                                <a href="#" class="delloan" loanid="<?php echo $item['loan_id']; ?>">删除</a>
                                <a href="<?php echo Url('admin/repayment/showlist',['user_id'=>$item['user_id'],'loan_id'=>$item['loan_id']]); ?>">查看还款详情</a>
                            </td>


                        </tr>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </table>
                <?php echo $page; ?>
            </div>

        </div>
    </div>
</div>

<script>
    $('.delloan').click(function () {
        var loan_id = $(this).attr('loanid');

        $.post('<?php echo Url("admin/loan/del"); ?>',{loan_id:loan_id},function (data) {
            alert(data);
            window.location.reload();
        });
    });
</script>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
