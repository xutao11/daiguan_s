<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:90:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/service/add.html";i:1502430628;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1502430620;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷官网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                

<div class="king-content-wrap">
    <div class="king-layout1-content" style="margin-top: 0; margin-left: 0px">
        <ol class="breadcrumb">
            <li><span>添加客服</span></li>


        </ol>
        <div class="panel panel-default m20">
            <div class="panel-body">
                <table style="width: 50%">
                <form action="" method="post" id="form" style="width: 30%" onsubmit="return mySubmit()">

                        <tr>

                           <td class="label_name" style="width: 6%">姓名</td>
                            <td style="width: 30%">
                            <input id="ser_name" class="form-control" type="text" name="ser_name" />

                            </td>

                        </tr>

                        <tr>

                            <td class="label_name">
                                性别
                            </td>
                            <td>
                                <select name="ser_sex" style="width: 100px;height: 37px">
                                    <option value="1">男</option>
                                    <option value="0">女</option>
                                </select>
                            </td>

                        </tr>
                        <tr>

                            <td class="label_name">手机</td>
                            <td>
                                <input id="ser_tel" class="form-control" type="text" name="ser_tel" />
                            </td>
                        </tr>
                    <tr>

                        <td class="label_name">qq</td>
                        <td>
                            <input class="form-control" type="text" name="ser_qq" />
                        </td>
                    </tr>
                    <tr>

                        <td class="label_name">微信</td>
                        <td>
                            <input class="form-control" type="text" name="ser_wechat" />
                        </td>
                    </tr>
                    <tr>

                        <td class="label_name">邮箱</td>
                        <td>
                            <input class="form-control" type="text" name="ser_email" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3"><input type="submit" class="btn btn-success" value="保存" /></td>
                    </tr>


                </form>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function mySubmit(flag){
        return flag;
    }
    $("#form").submit(function(){
        var ser_name = $('#ser_name').val();
        var ser_tel = $('#ser_tel').val();

        if(ser_name == ''){
            alert('客户姓名不能为空！');
            return false;
        }else if(ser_tel.length != '11'){
            alert('手机号格式错误');
            return false;

        }
    });

</script>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
