<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/loan/add.html";i:1505095441;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                

<div class="king-content-wrap">
	<div class="king-layout1-content" style="margin-top: 0; margin-left: 0px;">
		<ol class="breadcrumb">
			<li><span>添加贷款</span></li>
			<li><span style="color: red"><?php echo $user_name; ?></span></li>

		</ol>
		<div class="panel panel-default m20" style="width:30%;">
			<form method="post" action="<?php echo Url('admin/loan/type'); ?>">
				<div class="label_list top20">
					<label class="label_name">贷款状态</label>
					<select id="loan_state" name="loan_state">
						<?php $_59b60130b0b3c=config('data.loan_state'); if(is_array($_59b60130b0b3c) || $_59b60130b0b3c instanceof \think\Collection || $_59b60130b0b3c instanceof \think\Paginator): if( count($_59b60130b0b3c)==0 ) : echo "" ;else: foreach($_59b60130b0b3c as $k=>$vo): ?>
						<option value="<?php echo $k; ?>"><?php echo $vo; ?></option>
						<?php endforeach; endif; else: echo "" ;endif; ?>
					</select>
					<span style="font-size: 12px;color: red">

                        </span>
				</div>

				<div class="label_list top20">
					<label class="label_name">贷款类型选择</label>
					<select id="type_id" name="type_id">
						<?php $_59b60130b0acb=config('data.loan_type'); if(is_array($_59b60130b0acb) || $_59b60130b0acb instanceof \think\Collection || $_59b60130b0acb instanceof \think\Paginator): if( count($_59b60130b0acb)==0 ) : echo "" ;else: foreach($_59b60130b0acb as $k=>$vo): ?>
						<option value="<?php echo $vo; ?>"><?php echo $vo; ?></option>
						<?php endforeach; endif; else: echo "" ;endif; ?>
					</select>
					<span style="font-size: 12px;color: red">

                        </span>
					<br/>
				</div>

				<div class="label_list top20">
					<label class="label_name">贷款平台</label>
					<input class="form-control" type="text" name="loan_pingtai"/>
					<span style="font-size: 12px;color: red">
							必填,如（银行，金融机构.......）
                        </span>
				</div>
				<div class="label_list top20">
					<label class="label_name">贷款金额</label>
					<input class="form-control"  type="text" name="loan_money"/>
					<span style="font-size: 12px;color: red">
						元（贷款金额单位：元）
                        </span>
					<br/>
				</div>
				<div class="label_list top20 bank_card">
					<label class="label_name">还款方式:</label><br>
					<select name="repayment_type" style="margin-right: 18px;width: 55%;height:35px;">
						<?php $_59b60130b0a4e=config('data.repayment_type'); if(is_array($_59b60130b0a4e) || $_59b60130b0a4e instanceof \think\Collection || $_59b60130b0a4e instanceof \think\Paginator): if( count($_59b60130b0a4e)==0 ) : echo "" ;else: foreach($_59b60130b0a4e as $k=>$vo): ?>
						<option value="<?php echo $vo; ?>"><?php echo $vo; ?></option>
						<?php endforeach; endif; else: echo "" ;endif; ?>
					</select>
					<span style="font-size: 12px;color: red">
						还款方式
                        </span>
					<br/><br/>
				</div>
				<div class="label_list top20 bank_card">
					<label class="label_name">分期总数</label>
					<input class="form-control"  type="text" name="loan_all"/>
					<span style="font-size: 12px;color: red">
						分期数单位为：月（最多36个月）
                        </span>
				</div>

				<br/><br/>
				<div id="repayment_date" style="display: none" class="label_list top20">
					<label class="label_name">每月还款日</label>
					<select name="repayment_date" style="margin-right: 18px;width: 55%;height:35px;">
						<option value="">请选择还款日</option>
						<?php $__FOR_START_310720467__=1;$__FOR_END_310720467__=32;for($i=$__FOR_START_310720467__;$i < $__FOR_END_310720467__;$i+=1){ ?>
						<option value="<?php echo $i; ?>">每月<?php echo $i; ?>日</option>
						<?php } ?>
					</select>
					<span style="font-size: 12px;color: red">
						每月还款日（1-30号）
                        </span>
					<br/><br/>
				</div>

				<div id="start_date" style="display: none" class="label_list top20 bank_card">
					<label class="label_name">起始时间</label>
					<input id="date" class="form-control"  type="text" name="start_date"/>
					<span style="font-size: 12px;color: red">
						必填
                        </span>
				</div>
				<br/><br/>
				<div id="bank" style="display: none" class="label_list top20 bank_card">
					<label class="label_name">还款银行</label>
					<input class="form-control"  type="text" name="bank"/>
					<span style="font-size: 12px;color: red">
						必填
                        </span>
					<br/><br/>
				</div>

				<div id="bank_card" style="display: none" class="label_list top20 bank_card">
					<label class="label_name">还款卡号</label>
					<input class="form-control"  type="text" name="bank_card"/>
					<span style="font-size: 12px;color: red">
						必填
                        </span>

				</div>
				<div class="label_list top40">

					<input type="hidden" value="<?php echo $user_id; ?>" name="user_id">
					<input type="hidden" value="<?php echo $user_name; ?>" name="user_name">

					<input class="btn btn-success" type="button" value="下一步" onclick="this.form.submit()">
				</div>


			</form>


			</div>
		</div>
	</div>
</div>

<script>
	$('#date').jHsDate();
	$('#loan_state').change(function () {
		var data = $(this).val();
		if(data == '10'){
			$('#repayment_date').css('display','inline');
			$('#start_date').css('display','inline');
			$('#bank').css('display','inline');
			$('#bank_card').css('display','inline');
		}else {
			$('#repayment_date').css('display','none');
			$('#start_date').css('display','none');
			$('#bank').css('display','none');
			$('#bank_card').css('display','none');
		}
	});


	$('#type_id').change(function(){
		var data = $(this).val();

		if(data == '信用卡'){
			$('.bank_card').css('display','none');
		}else {
			$('.bank_card').css('display','inline');
		}

	});
</script>



            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
