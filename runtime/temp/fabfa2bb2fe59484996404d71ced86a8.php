<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:88:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/user/edit.html";i:1505095442;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                

<div class="king-content-wrap">
    <div class="king-layout1-content" style="margin-top: 0; margin-left: 0px">
        <ol class="breadcrumb">
            <li><span>修改资料</span></li>


        </ol>
        <div class="panel panel-default m20">
            <div class="panel-body">
                <table style="width: 50%">
                <form action="" method="post" id="form" style="width: 30%">

                        <tr>

                           <td class="label_name" style="width: 6%">客户昵称</td>
                            <td style="width: 30%">
                            <input class="form-control" type="text" name="user_name" value="<?php echo $data['user_name']; ?>"/>
                            </td>

                        </tr>
                        <tr>

                            <td class="label_name" style="width: 6%">真实姓名</td>
                            <td style="width: 30%">
                                <input class="form-control" type="text" name="turename" value="<?php echo $data['turename']; ?>"/>
                            </td>

                        </tr>
                        <tr>

                            <td class="label_name">
                                性别
                            </td>
                            <td>

                                <select name="user_sex" style="width: 100px;height: 37px">
                                    <option value="1" <?php if($data['user_sex']==1): ?> selected <?php endif; ?>>男</option>
                                    <option value="0" <?php if($data['user_sex']==0): ?> selected <?php endif; ?>>女</option>
                                </select>
                            </td>

                        </tr>
                    <tr>

                        <td class="label_name">出生日期</td>
                        <td>
                            <input id="user_birthday" class="form-control" type="text" name="user_birthday" value="<?php echo $data['user_birthday']; ?>"/>
                        </td>
                    </tr>
                        <tr>

                            <td class="label_name">手机</td>
                            <td>
                                <input class="form-control" type="text" name="user_tel" value="<?php echo $data['user_tel']; ?>"/>
                            </td>
                        </tr>
                        <tr>

                            <td class="label_name">所在地</td>
                            <td>
                                <input class="form-control" type="text" name="user_dizhi" value="<?php echo $data['user_dizhi']; ?>"/>
                            </td>

                        </tr>
                        <tr>
                            <td class="label_name">
                                客服人员
                            </td>
                            <td>
                                <select name="ser_id" style="width: 100px;height: 37px">
                                    <option value="">请选择</option>
                                    <?php if(is_array($ser) || $ser instanceof \think\Collection || $ser instanceof \think\Paginator): if( count($ser)==0 ) : echo "" ;else: foreach($ser as $key=>$v): ?>
                                    <option value="<?php echo $v['ser_id']; ?>" <?php if($data['ser_id'] == $v['ser_id']): ?> selected <?php endif; ?>><?php echo $v['ser_name']; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </td>
                        </tr>
                    <tr>
                        <td colspan="3">
                            <input type="hidden" name="user_id" value="<?php echo $data['user_id']; ?>">
                            <input onclick="form.submit()" type="button" class="btn btn-success" value="保存" />
                        </td>
                    </tr>


                </form>
                </table>
            </div>
        </div>
    </div>
</div>
<script>

    $('#user_birthday').jHsDate();


</script>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
