<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:87:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/logo/add.html";i:1503308990;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1502430620;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷官网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                


<div class="king-content-wrap" style="margin-left: -229px;width: 100%;">
							<div class="king-layout1-content" style="margin-top: 0;">
								<div class="panel panel-default m20">
									<div class="panel-body">
										<form action="<?php echo Url('admin/logo/add_form'); ?>" method="post" id="form" onsubmit="return mySubmit();">
											<div class="label_list top20">
												<label class="label_name">标题</label>
												<input class="form-control" type="text" name="title" value="<?php echo $item['title']; ?>"/>
											</div>
					<div class="label_list top20">
						<label class="label_name">简介<span style="color: red;font-size: 13px;">（最多25个字）</span></label>
						<input id="jianjie" class="form-control"  type="text" name="description" value="<?php echo $item['description']; ?>"/>
					</div>
					<div class="label_list top20">
						<label class="label_name">内容</label>
						<textarea name="content" id="ueditor" class="word_editor"><?php echo $item['content']; ?></textarea>
					</div>

					<div class="label_list top40">
						<input type="submit" class="btn btn-success" value="保存" />
					</div>

				</form>
			</div>
		</div>
	</div>
</div>
<script>

	function mySubmit(flag){
		return flag;
	}
	$("#form").submit(function(){
		var tel = $('#jianjie').val();

		if(tel.length >30){
			alert('简介不要超过25个字');
			return false;
		}
	});


</script>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
