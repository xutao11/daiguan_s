<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:96:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/repayment/addedit.html";i:1505106884;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                

<div class="king-content-wrap">
	<div class="king-layout1-content" style="margin-top: 0; margin-left: 0px;">
		<ol class="breadcrumb">
			<li><span>修改分期详情</span></li>
			<li><span style="color: red"><?php echo $user_name; ?></span></li>
			<li><span><?php echo $re['loan_type']; ?></span></li>
			<li><span><?php echo $re['loan_pingtai']; ?></span></li>
			<li><span><?php echo $re['repayment_type']; ?></span></li>

		</ol>
		<div class="panel panel-default m20" style="width:30%;">
			<div class="panel-body">
				<form action="<?php echo Url('/admin/repayment/edit'); ?>" method="post">
				<table  style="width: 100%" class="table table-hover">
					<tr>
						<td>期数</td>
						<td>还款金额（元）</td>
						<td>还款状态</td>
					</tr>
					<?php echo $re['repayment_type']; switch($re['repayment_type']): case "等额本息": ?>

					<tr>
						<td>共<?php echo $re['loan_all']; ?>期</td>
						<td>

							<input name="repayment_money" type="text" class="form-control" value="<?php echo $money; ?>">
						</td>
						<td>
							默认
						</td>
					</tr>

					<?php break; default: foreach($date as $vo): ?>
					<tr>
						<td>第<?php echo $vo['repayment_qishu']; ?>期</td>
						<td>
							<input name="repayment_money[]" type="text" value="<?php echo $vo['repayment_money']; ?>" class="form-control">
						</td>

						<td>
							<select name="repayment_zhuangtai[]">
								<option value="0" <?php if($vo['repayment_zhuangtai'] == '0'): ?> selected = 'selected' <?php endif; ?>>未还</option>
								<option value="1" <?php if($vo['repayment_zhuangtai'] == '1'): ?> selected = 'selected'<?php endif; ?>>已还</option>

							</select>
						</td>

					</tr>
					<?php endforeach; endswitch; ?>


				</table>
					<input type="hidden" value="<?php echo $re['repayment_type']; ?>" name="re_type">
					<input type="hidden" value="<?php echo $re['loan_id']; ?>" name="loan_id">
					<input class="btn btn-success" type="submit" value="确认">
				</form>



			</div>
		</div>
	</div>
</div>




            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
