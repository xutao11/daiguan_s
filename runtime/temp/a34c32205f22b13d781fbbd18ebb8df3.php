<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:88:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/logo/show.html";i:1505095441;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                


<div class="center">

    <div class="container  top30">
        <div class="center_name"style="padding: 15px 125px 15px 15px;
    border-top: 1px solid #eeeeee;
    border-bottom: 1px solid #eeeeee;
    text-align: left;
    margin-top: 30px;
    position: relative;">

            <h3><?php echo $item['title']; ?></h3>
            <p><?php echo $item['description']; ?></p>
            <a href="<?php echo Url('admin/logo/add'); ?>" class="more" style="float: right;margin-top: -58px;width: 101px;
    border: 1px solid #3f82f7;
    height: 37px;
    font-size: 16px;
    text-align: center;
    line-height: 37px;
    border-radius: 9px;">修改</a>

        </div>
        <div class="center_text top30" style="color: rgb(102, 102, 102);
    font-family: "Microsoft YaHei", Simsun, Arial, Helvetica, sans-serif;
        background-color: rgb(255, 255, 255);"><?php echo $item['content']; ?></div>
    </div>
</div>
<script>
    $('.more').mouseover(function () {
        $(this).css('color','#fff');
        $(this).css('backgroundColor',"#3f82f7");
    });
    $('.more').mouseout(function () {
        $(this).css('color','#3f82f7');
        $(this).css('backgroundColor',"#fff");
    });
</script>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
