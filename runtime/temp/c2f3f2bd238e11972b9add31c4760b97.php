<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:91:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/service/list.html";i:1505095442;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                
<div class="king-content-wrap" style="margin-top: 0px">
    <div class="king-layout1-content" style="margin-top: 0; margin-left: 0px">
        <ol class="breadcrumb">
        
            <li><span>客服列表</span></li>
        </ol>
        <div class="clearfix m20">
            <a class="btn btn-success" href="<?php echo Url('admin/service/add'); ?>">添 加</a>
        </div>
        <div class="panel panel-default m20">
            <div class="panel-body">
                <table class="table table-hover">
                    <tr style="background-color: rgba(25,25,25,0.19)">

                        <td style="font-size: 14px">姓名</td>
                        <td>性别</td>
                        <td>手机</td>
                        <td>qq</td>
                        <td>微信</td>
                        <td>邮箱</td>

                        <td>操作</td>

                    </tr>
                    <?php if(is_array($datas) || $datas instanceof \think\Collection || $datas instanceof \think\Paginator): if( count($datas)==0 ) : echo "" ;else: foreach($datas as $key=>$item): ?>
                        <tr>

                            <td><?php echo $item['ser_name']; ?>
                            &nbsp;</td>
                            <td>
                                <?php if($item['ser_sex'] == 1): ?> 男

                                <?php else: ?> 女
                                <?php endif; ?>


                                &nbsp;</td>
                            <td><?php echo $item['ser_tel']; ?></td>
                            <td>
                                <?php echo $item['ser_qq']; ?>
                            &nbsp;</td>
                            <td>
                                <?php echo $item['ser_wechat']; ?>
                            </td>
                            <td>
                                <?php echo $item['ser_email']; ?>

                            &nbsp;</td>

                            <td>
                                <a href="<?php echo Url('admin/service/edit',['ser_id'=>$item['ser_id']]); ?>">修改</a>
                                <a href="#" class="delloan" serid="<?php echo $item['ser_id']; ?>">删除</a>
                            </td>


                        </tr>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </table>
                <?php echo $page; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $('.delloan').click(function () {
        var ser_id = $(this).attr('serid');

        $.post('<?php echo Url("admin/service/del"); ?>',{ser_id:ser_id},function (data) {
            alert(data);
            window.location.reload();
        });
    });
</script>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
