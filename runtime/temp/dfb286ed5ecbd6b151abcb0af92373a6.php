<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:90:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/article/add.html";i:1505881321;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                <?php
use app\common\model\Category;
?>

<?php echo widget('Menu/index',['category_id'=>$category_id]); ?>
<div class="king-content-wrap">
							<div class="king-layout1-content" style="margin-top: 0;">
								<ol class="breadcrumb">
									<li><span>内容管理</span></li>
									<li><span><?php echo Category::get($category_id)['title']; ?></span></li>
								</ol>
								<div class="panel panel-default m20">
									<div class="panel-body">
										<form action="<?php echo Url('Article/add'); ?>" method="post" id="form" onsubmit="return mySubmit();">
											<div class="label_list top20">
												<label class="label_name">标题<span style="color: red;font-size: 13px;">（还可输入<span id="show1">10</span>个字）</span></label>
												<input oninput="ee()" id="title" class="form-control" type="text" name="title" />
											</div>
											<div class="label_list top20">
												<label class="label_name">封面图<span style="color: red;font-size: 13px;">(图片大小:536x274)</span></label>
												<div class="label_img ">
													<?php echo widget('Upload/image'); ?>
												</div>
											</div>
					<div class="label_list top20">
						<label class="label_name">简介<span style="color: red;font-size: 13px;">（还可输入<span id="show">40</span>个字）</span></label>
						<input oninput="ss()" id="jianjie" class="form-control"  type="text" name="description" />
					</div>
					<div class="label_list top20">
						<label class="label_name">链接</label>
						<input class="form-control"  type="text" name="url" />
					</div>
                    <div class="label_list top20">
                        <label class="label_name">是否跳转</label>
                        <div>
                            <input type="radio" name="isskip" value="1" />是
                            <input type="radio" name="isskip" value="0" />否

                        </div>
                    </div>
					<div class="label_list top20">
						<label class="label_name">内容</label>
						<textarea name="content" id="ueditor" class="word_editor"></textarea>
					</div>
					<hr />
					<div class="label_list top20">
						<label class="label_name">seo_title</label>
						<input class="form-control"  type="text" name="seo_title" />
					</div>
					<div class="label_list top20">
						<label class="label_name">seo_keyword</label>
						<input class="form-control"  type="text" name="seo_keyword" />
					</div>
					<div class="label_list top20">
						<label class="label_name">seo_description</label>
						<input class="form-control"  type="text" name="seo_description" />
					</div>

					<div class="label_list top40">
						<input type="submit" class="bos_button btn btn-success" value="保存" />
					</div>
					<input type="hidden" name="category_id" value="<?php echo $category_id; ?>" />
				</form>
			</div>
		</div>
	</div>
</div>
<script>

	function mySubmit(flag){
		return flag;
	}
	$("#form").submit(function(){
		var tel = $('#jianjie').val();
		var title = $('#title').val();
		if(tel.title >10){
			alert('标题不要超过10个字');
			return false;
		}

		if(tel.length >40){
			alert('简介不要超过40个字');
		return false;
	}
	});
	window.onload=function release(){
		var text=document.getElementById('jianjie');

		if(text.value.length>40)
		{
			alert('超过字数')
		}

	}
	function ss(){
		var text = document.getElementById('jianjie');
		var show = document.getElementById('show');


		if(show.innerHTML < 1){
			var date = $('#jianjie').val();

			var value = date.substring(0,40);

			$('#jianjie').val(value);
		}
		show.innerHTML =40 - text.value.length;


	}
	function ee(){
		var text = document.getElementById('title');
		var show = document.getElementById('show1');


		if(show.innerHTML < 1){
			var date = $('#title').val();

			var value = date.substring(0,10);

			$('#title').val(value);
		}
		show.innerHTML =10 - text.value.length;


	}



</script>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
