<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:93:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/category/index.html";i:1505095441;s:92:"/home/daiguanwangqdiayixgau0avn6wfarn3g/wwwroot/../application/admin/view/layout/layout.html";i:1505095441;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="成都东帝投资有限公司" />
	<meta name="description" content="成都东帝投资有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                <?php
use app\admin\service\CategoryService;
?>

<div class="king-content-wrap">
	<div class="king-layout1-content" style="margin-top: 0;margin-left: 0;">
		<ol class="breadcrumb">
			<li><span>栏目管理</span></li>
			<li><span>栏目列表</span></li>
		</ol>
		<div class="clearfix m20">
			<a class="btn btn-success" href="<?php echo Url('Category/add'); ?>">添 加</a>
		</div>
		<div class="panel panel-default m20">
			<div class="panel-body">
				<?php if(empty($items) || (($items instanceof \think\Collection || $items instanceof \think\Paginator ) && $items->isEmpty())): ?>
				无记录
				<?php else: if(is_array($items) || $items instanceof \think\Collection || $items instanceof \think\Paginator): $i = 0; $__LIST__ = $items;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
				<ul class="list-group">
					<li class="list-group-item">
						<span><?php echo $item['title']; ?></span>
						<span style="float:right;">
								<a href="<?php echo Url('Category/add?parent_id='.$item->id); ?>">添加子栏目</a>
								<a href="<?php echo Url('Category/edit?id='.$item->id); ?>">编辑栏目</a>
								<a class="del" data-id="<?php echo $item->id; ?>" data-url="<?php echo Url('Category/del'); ?>" href="javascript:void(0);">删除栏目</a>
								<a class="sort" data-id="<?php echo $item->id; ?>" data-url="<?php echo Url('Category/sort'); ?>" data-sort="up" href="javascript:void(0)">向上</a>
								<a class="sort" data-id="<?php echo $item->id; ?>" data-url="<?php echo Url('Category/sort'); ?>" data-sort="down" href="javascript:void(0)">向下</a>
							</span>
						<?php if(CategoryService::hasChildren($item['id'])):$children = CategoryService::getChildren($item['id']);?>
						<ul>
							<?php foreach($children as $child): ?>
								<li class="list-group-item">
									<a href="<?php echo Url('Category/edit?id='.$child->id); ?>" class="gui_controls_left"><?php echo $child['title']; ?></a>
									<span style="float:right;">
										<a href="<?php echo Url('Category/add?parent_id='.$child->id); ?>">添加子栏目</a>
											<a href="<?php echo Url('Category/edit?id='.$child->id); ?>">编辑栏目</a>
											<a class="del" data-id="<?php echo $child->id; ?>" data-url="<?php echo Url('Category/del'); ?>" href="javascript:void(0);">删除栏目</a>
											<a class="sort" data-id="<?php echo $child->id; ?>" data-url="<?php echo Url('Category/sort'); ?>" data-sort="up" href="javascript:void(0)">向上</a>
											<a class="sort" data-id="<?php echo $child->id; ?>" data-url="<?php echo Url('Category/sort'); ?>" data-sort="down" href="javascript:void(0)">向下</a>
										</span>
								</li>
							<?php if(CategoryService::hasChildren($child['id'])):$child_child = CategoryService::getChildren($child['id']);foreach($child_child as $v): ?>
										<li class="list-group-item">
											<a href="<?php echo Url('Category/edit?id='.$v->id); ?>" class="gui_controls_left">=<?php echo $v['title']; ?></a>
											<span style="float:right;">
														<a href="<?php echo Url('Category/edit?id='.$v->id); ?>">编辑栏目</a>
														<a class="del" data-id="<?php echo $v->id; ?>" data-url="<?php echo Url('Category/del'); ?>" href="javascript:void(0);">删除栏目</a>
														<a class="sort" data-id="<?php echo $v->id; ?>" data-url="<?php echo Url('Category/sort'); ?>" data-sort="up" href="javascript:void(0)">向上</a>
														<a class="sort" data-id="<?php echo $v->id; ?>" data-url="<?php echo Url('Category/sort'); ?>" data-sort="down" href="javascript:void(0)">向下</a>
													</span>
										</li>
									<?php endforeach; endif; endforeach;?>
						</ul>
						<?php endif;?>
					</li>
				</ul>
				<?php endforeach; endif; else: echo "" ;endif; endif; ?>
			</div>
		</div>
	</div>
</div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
