<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:65:"D:\Code\daiguan1\wwwroot/../application/admin\view\data\list.html";i:1519610883;s:69:"D:\Code\daiguan1\wwwroot/../application/admin\view\layout\layout.html";i:1519610883;}*/ ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>贷管网-后台管理系统</title>
	<meta name="keywords" content="四川众银联行信息科技有限公司" />
	<meta name="description" content="四川众银联行信息科技有限公司" />

    <link rel="stylesheet" type="text/css" href="__COMMON__/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/js/webuploader/webuploader.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/style.css" /><link rel="stylesheet" type="text/css" href="__MANAGE__/css/bk.css" /><script type="text/javascript" src="__COMMON__/jquery-3.2.0.min.js"></script><script type="text/javascript" src="__COMMON__/bootstrap.min.js"></script><script type="text/javascript" src="__COMMON__/jHsDate.js"></script><link rel="stylesheet" type="text/css" href="__COMMON__/jHsDate.css" />
</head>
<body>

<div class="king-layout1-header">
    <div class="" style="position: relative;">
        <nav class="navbar-fixed-top">
            <div class="navbar king-horizontal-nav1" style="background-color: #f2f2f2; height: 60px; margin-bottom:0">
                <div class="navbar-container ">
                    <div class="navbar-header pull-left">
                        <a class="navbar-brand" href="/" style="border-right:none;">
                            <img src="__STATIC__/manage/image/logo.png" style="height: 60px;">
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right pr10">
                        <li><a href="javascript:void;"><?php echo session('auth')['uname'];; ?></a></li>
                        <li><a href="<?php echo Url('Logout/index'); ?>" class="other_left">退出</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="king-layout1-main">
    <?php echo widget('Menu/left'); ?>
    <div class="king-content-wrap">
        <div class="king-layout1-content">
            <div class="container-fluid ">
                
<div class="king-content-wrap">
    <div class="king-layout1-content" style="margin-top: 0;margin-left: 0;">
        <ol class="breadcrumb">
        
            <li><span>客户申请列表</span></li>
        </ol>
        <div class="clearfix m20">

        </div>
        <div class="panel panel-default m20">
            <div class="panel-body">
                <table>
                    <tr style="background-color: rgba(25,25,25,0.19)">
                        <td style="width: 5%">所属用户</td>
                        <td>姓名</td>
                        <td>手机</td>
                        <td style="width: 49px;">性别</td>
                        <td>所在地</td>
                        <td>职业</td>
                        <td>月收入</td>
                        <td>征信情况</td>
                        <td>房产</td>
                        <td>车产</td>
                        <td>保险</td>
                        <td>信用卡</td>
                        <td>申请时间</td>
                        <td>评测结果</td>
                        <td style="width: 10%">操作</td>
                    </tr>
                    <?php if(is_array($items) || $items instanceof \think\Collection || $items instanceof \think\Paginator): if( count($items)==0 ) : echo "" ;else: foreach($items as $key=>$item): ?>
                        <tr>
                            <td><?php echo $item['user_name']; ?></td>
                            <td><?php echo $item['name']; ?>&nbsp;</td>
                            <td><?php echo $item['phone']; ?></td>
                            <td>
                                <?php switch($item['sex']): case "m": ?>先生<?php break; case "f": ?>女士<?php break; endswitch; ?>  
                            &nbsp;</td>
                            <td><?php $re = area($date,$item['county']); echo $re['2'].$re['1'].$re['0']; ?></td>
                            <td>

                                <?php $_5aa5d48327c0d=config('data.work'); if(is_array($_5aa5d48327c0d) || $_5aa5d48327c0d instanceof \think\Collection || $_5aa5d48327c0d instanceof \think\Paginator): if( count($_5aa5d48327c0d)==0 ) : echo "" ;else: foreach($_5aa5d48327c0d as $k=>$work): if($item['work'] == $k): ?>
                                <?php echo $work; endif; endforeach; endif; else: echo "" ;endif; ?>
                            &nbsp;</td>
                            <td>

                                <?php $_5aa5d48327b74=config('data.wages'); if(is_array($_5aa5d48327b74) || $_5aa5d48327b74 instanceof \think\Collection || $_5aa5d48327b74 instanceof \think\Paginator): if( count($_5aa5d48327b74)==0 ) : echo "" ;else: foreach($_5aa5d48327b74 as $k=>$wages): if($item['wages'] == $k): ?>
                                <?php echo $wages; endif; endforeach; endif; else: echo "" ;endif; ?>
                                &nbsp;</td>
                            <td>

                                <?php $_5aa5d48327aca=config('data.credit'); if(is_array($_5aa5d48327aca) || $_5aa5d48327aca instanceof \think\Collection || $_5aa5d48327aca instanceof \think\Paginator): if( count($_5aa5d48327aca)==0 ) : echo "" ;else: foreach($_5aa5d48327aca as $k=>$credit): if($item['credit'] == $k): ?>
                                <?php echo $credit; endif; endforeach; endif; else: echo "" ;endif; ?>
                            </td>
                            <td>
                                <?php $_5aa5d48327a00=config('data.house'); if(is_array($_5aa5d48327a00) || $_5aa5d48327a00 instanceof \think\Collection || $_5aa5d48327a00 instanceof \think\Paginator): if( count($_5aa5d48327a00)==0 ) : echo "" ;else: foreach($_5aa5d48327a00 as $k=>$house): if($item['house'] == $k and $k == '1'): ?>
                                <?php echo $house; elseif($item['house'] == $k and $k == '2'): ?>
                                <?php echo $house; ?>,价值<?php echo $item['house_value']; ?>万元
                                <?php elseif($item['house'] == $k and $k == '3'): ?>
                                <?php echo $house; ?>,价值<?php echo $item['house_value']; ?>万元，月供<?php echo $item['house_loan']; ?>元，已还<?php echo $item['house_months']; ?>期


                                <?php endif; endforeach; endif; else: echo "" ;endif; ?>

                            &nbsp;</td>
                            <td>
                                <?php $_5aa5d4832792e=config('data.car'); if(is_array($_5aa5d4832792e) || $_5aa5d4832792e instanceof \think\Collection || $_5aa5d4832792e instanceof \think\Paginator): if( count($_5aa5d4832792e)==0 ) : echo "" ;else: foreach($_5aa5d4832792e as $k=>$car): if($item['car'] == $k and $k == '1'): ?>
                                <?php echo $car; elseif($item['car'] == $k and $k == '2'): ?>
                                <?php echo $car; ?>,价值<?php echo $item['car_value']; ?>万元
                                <?php elseif($item['car'] == $k and $k == '3'): ?>
                                <?php echo $car; ?>,价值<?php echo $item['car_value']; ?>万元，月供<?php echo $item['car_loan']; ?>元，已还<?php echo $item['car_months']; ?>期
                                <?php endif; endforeach; endif; else: echo "" ;endif; ?>
                                
                            &nbsp;</td>
                            <td>
                                <?php $_5aa5d4832780e=config('data.life'); if(is_array($_5aa5d4832780e) || $_5aa5d4832780e instanceof \think\Collection || $_5aa5d4832780e instanceof \think\Paginator): if( count($_5aa5d4832780e)==0 ) : echo "" ;else: foreach($_5aa5d4832780e as $k=>$life): if($item['life'] == $k and $k == '1'): ?>
                                        <?php echo $life; elseif($item['life'] == $k and $k == '2'): ?>
                                        <?php echo $life; ?>,保险险种为
                                            <?php $_5aa5d483278a8=config('data.life_type'); if(is_array($_5aa5d483278a8) || $_5aa5d483278a8 instanceof \think\Collection || $_5aa5d483278a8 instanceof \think\Paginator): if( count($_5aa5d483278a8)==0 ) : echo "" ;else: foreach($_5aa5d483278a8 as $ke=>$life_type): if($item['life_type'] == $ke): ?>
                                                    <?php echo $life_type; ?><br/>
                                            <?php endif; endforeach; endif; else: echo "" ;endif; ?>
                                        保险公司<?php echo $item['life_company']; ?>,年缴费金额<?php echo $item['life_money']; ?>,实缴期限<?php echo $item['life_months']; ?>个月
                                    <?php endif; endforeach; endif; else: echo "" ;endif; ?>

                                &nbsp;</td>
                            <td>
                                <?php $_5aa5d48327749=config('data.card'); if(is_array($_5aa5d48327749) || $_5aa5d48327749 instanceof \think\Collection || $_5aa5d48327749 instanceof \think\Paginator): if( count($_5aa5d48327749)==0 ) : echo "" ;else: foreach($_5aa5d48327749 as $k=>$card): if($item['card'] == $k and $k == '1'): ?>
                                <?php echo $card; elseif($item['card'] == $k and $k == '2'): ?>
                                <?php echo $card; ?>,共<?php echo $item['card_number']; ?>张，单张额度最高<?php echo $item['card_max']; ?>元
                                <?php endif; endforeach; endif; else: echo "" ;endif; ?>
                            &nbsp;</td>
                            <td>
                                <?php echo $item['application_date']; ?>
                            </td>
                            <td style="color: red">
                                <?php echo $item['reply']; ?>
                            </td>
                            <td>
                                <a class="del2" data-url="<?php echo Url('Slide/del'); ?>" data-id="<?php echo $item['id']; ?>" href="javascript:void(0);">删除</a>
                                <a href="#" class="huifu" dataid="<?php echo $item['id']; ?>">回复</a>
                            </td>
                        </tr>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </table>
                <?php echo $page; ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/static/layer/layer.js"></script>
<script>
    $('.del2').click(function () {
        var id = $(this).attr('data-id');
        $.post('<?php echo Url("admin/data/del"); ?>',{id:id},function (data) {
            alert(data);
            window.location.reload();
        });
    });
    $('.zhuangtai').click(function(){
        var repaymentid = $(this).attr('repaymentid');
        var  repaymentzhuangtai=$(this).attr('zhuangtai');
        $.post('<?php echo Url("admin/repayment/editzhuangtai"); ?>',{repayment_id:repaymentid,repayment_zhuangtai:repaymentzhuangtai},function(date){

            window.location.reload();
        });


    });
    $('.huifu').click(function(){
        var dataid  = $(this).attr('dataid');
        layer.prompt({title: '请回复', formType: 2}, function(text, index){


                $.post('<?php echo Url("admin/data/reply"); ?>',{id:dataid,reply:text},function(data){
                    if(data){
                        layer.alert('回复添加成功');
                    }else{
                        layer.alert('回复添加失败');
                    }


                    window.location.reload();
                });


        });
    });
</script>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="__MANAGE__/js/webuploader/webuploader.min.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.config.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/ueditor.all.js"></script><script type="text/javascript" src="__MANAGE__/js/ueditor/plugins/135editor.js"></script><script type="text/javascript" src="__MANAGE__/js/script.js"></script>
</body>
</html>
